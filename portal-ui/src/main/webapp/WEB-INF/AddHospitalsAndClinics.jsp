<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8" %>
<!DOCTYPE html>
<html class="no-js" lang="en">
<head>
    <%@include file="pages/css_import.jsp" %>
    <title>Portal - Add Hospitals</title>
</head>
<body>
<div id="page-wrapper">
    <!-- Preloader -->
    <%@include file="pages/preloader.jsp" %>
    <!-- END Preloader -->

    <div id="page-container" class="sidebar-partial sidebar-visible-lg sidebar-no-animations">
        <!-- Main Sidebar -->
        <%@include file="pages/side-nav.jsp" %>
        <!-- END Main Sidebar -->

        <!-- Main Container -->
        <div id="main-container">
            <%@include file="pages/header.jsp" %>
            <!-- END Header -->
            <!-- Page content -->
            <div id="page-content">
                <!-- Forms General Header -->
                <div class="content-header">
                    <div class="header-section">
                        <h1>
                            <i class="gi gi-notes_2"></i>Add New Hospital<br><small>adding new hospital </small>
                        </h1>
                    </div>
                </div>
                <ul class="breadcrumb breadcrumb-top">
                    <li>Managers</li>
                    <li><a href="hospital-and-clinics">Hospital managers</a></li>
                    <li><a href="">Add new hospital</a></li>
                </ul>
                <!-- END Forms General Header -->

                <div class="row">
                    <div class="col-md-6">
                        <!-- Basic Form Elements Block -->
                        <div class="block">
                            <!-- Basic Form Elements Title -->
                            <div class="block-title">
                                <div class="block-options pull-right">
                                </div>
                                <h2><strong>Add Hospital</strong> Form</h2>
                            </div>
                            <!-- END Form Elements Title -->

                            <!-- Basic Form Elements Content -->
                            <form action="page_forms_general.html" method="post" enctype="multipart/form-data"
                                  class="form-horizontal form-bordered" onsubmit="return false;">
                                <div class="form-group">
                                    <label class="col-md-3 control-label" for="example-text-input">Hospital Code</label>
                                    <div class="col-md-9">
                                        <input type="number" id="example-text-input" name="example-text-input"
                                               class="form-control" placeholder="Code">
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label class="col-md-3 control-label" for="example-text-input">Hospital Name</label>
                                    <div class="col-md-9">
                                        <input type="text" id="example-text-input" name="example-text-input"
                                               class="form-control" placeholder="Name">
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label class="col-md-3 control-label" for="example-email-input">Contact</label>
                                    <div class="col-md-9">
                                        <input type="number" id="example-email-input" name="example-email-input"
                                               class="form-control" placeholder="Phone">
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label class="col-md-3 control-label" for="example-textarea-input">Address</label>
                                    <div class="col-md-9">
                                        <textarea id="example-textarea-input" name="example-textarea-input" rows="9"
                                                  class="form-control" placeholder="Address"></textarea>
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label class="col-md-3 control-label" for="example-select">Province</label>
                                    <div class="col-md-9">
                                        <select id="example-select" name="example-select" class="form-control" size="1">
                                            <option value="0">Please select</option>
                                            <option value="1">Option #1</option>
                                            <option value="2">Option #2</option>
                                            <option value="3">Option #3</option>
                                        </select>
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label class="col-md-3 control-label" for="example-select">City</label>
                                    <div class="col-md-9">
                                        <select id="example-select" name="example-select" class="form-control" size="1">
                                            <option value="0">Please select</option>
                                            <option value="1">Option #1</option>
                                            <option value="2">Option #2</option>
                                            <option value="3">Option #3</option>
                                        </select>
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label class="col-md-3 control-label" for="example-select">Districs</label>
                                    <div class="col-md-9">
                                        <select id="example-select" name="example-select" class="form-control" size="1">
                                            <option value="0">Please select</option>
                                            <option value="1">Option #1</option>
                                            <option value="2">Option #2</option>
                                            <option value="3">Option #3</option>
                                        </select>
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label class="col-md-3 control-label">Inpatient</label>
                                    <div class="col-md-9">
                                        <label class="radio-inline" for="inpatient-yes">
                                            <input type="radio" id="inpatient-yes" name="inpatient-yes" value="yes"> Yes
                                        </label>
                                        <label class="radio-inline" for="inpatient-no">
                                            <input type="radio" id="inpatient-no" name=inpatient-no" value="no"> No
                                        </label>
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label class="col-md-3 control-label">Outpatient</label>
                                    <div class="col-md-9">
                                        <label class="radio-inline" for="outpatient-yes">
                                            <input type="radio" id="outpatient-yes" name="outpatient-yes" value="yes">
                                            Yes
                                        </label>
                                        <label class="radio-inline" for="outpatient-no">
                                            <input type="radio" id="outpatient-no" name="outpatient-no" value="no"> No
                                        </label>
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label class="col-md-3 control-label">Ponek</label>
                                    <div class="col-md-9">
                                        <label class="radio-inline" for=ponek-yes">
                                            <input type="radio" id="ponek-yes" name="ponek-yes" value="yes"> Yes
                                        </label>
                                        <label class="radio-inline" for="ponek-no">
                                            <input type="radio" id="ponek-no" name="ponek-no" value="no"> No
                                        </label>
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label class="col-md-3 control-label" for="example-text-input">Beds
                                        Available</label>
                                    <div class="col-md-9">
                                        <input type="number" id="example-text-input" name="example-text-input"
                                               class="form-control" placeholder="Beds">
                                    </div>
                                </div>
                                <div class="form-group form-actions">
                                    <div class="col-md-9 col-md-offset-3">
                                        <button type="submit" class="btn btn-sm btn-primary"><i
                                                class="fa fa-angle-right"></i> Submit
                                        </button>
                                        <button type="reset" class="btn btn-sm btn-warning"><i class="fa fa-repeat"></i>
                                            Reset
                                        </button>
                                    </div>
                                </div>
                            </form>
                            <!-- END Basic Form Elements Content -->
                        </div>
                        <!-- END Basic Form Elements Block -->
                    </div>
                    <div class="col-md-6">
                        <div class="block full">
                            <!-- Horizontal Form Title -->
                            <div class="block-title">
                                <div class="block-options pull-right">
                                </div>
                                <h2><strong>Geolocation</strong> Map</h2>
                            </div>
                            <form action="page_forms_general.html" method="post" enctype="multipart/form-data"
                                  class="form-horizontal form-bordered" onsubmit="return false;">
                                <div class="form-group">
                                    <label class="col-md-3 control-label">Geo Location</label>
                                    <div class="col-md-9 form-inline">
                                        <div>
                                            <input type="text" id="example-if-email" name="example-if-email"
                                                   class="form-control" placeholder="Latitude">
                                        </div>
                                        <div>
                                            <input type="text" id="example-if-password" name="example-if-password"
                                                   class="form-control" placeholder="Longitude">
                                        </div>
                                    </div>
                                </div>
                                <div class="form-group">
                                    <div id="gmap-geolocation" class="gmap"></div>
                                </div>

                            </form>
                        </div>
                    </div>
                </div>
            </div>
            <!-- END Page Content -->
            <!-- Footer -->
            <%@include file="pages/footer.jsp" %>
            <!-- END Footer -->
        </div>
        <!-- END Main Container -->
    </div>
    <!-- END Page Container -->
</div>
<!-- END Page Wrapper -->

<!-- Scroll to top link, initialized in js/app.js - scrollToTop() -->
<a href="#" id="to-top"><i class="fa fa-angle-double-up"></i></a>
<%@include file="pages/js_import.jsp" %>

<!-- Google Maps API Key (you will have to obtain a Google Maps API key to use Google Maps) -->
<!-- For more info please have a look at https://developers.google.com/maps/documentation/javascript/get-api-key#key -->
<script src="https://maps.googleapis.com/maps/api/js?key=AIzaSyBqVV6BlpyEaqr7bag9LNAHwpZw8LxjhGg"></script>
<script src="main/js/helpers/gmaps.min.js"></script>

<!-- Load and execute javascript code used only in this page -->
<script src="main/js/AddHospitalsAndClinics.js"></script>
</body>
</html>