<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8" %>
<!DOCTYPE html>
<html class="no-js" lang="en">
<head>
    <%@include file="pages/css_import.jsp" %>
    <title>Portal - Dashboard</title>
</head>
<body>
<div id="page-wrapper">
    <!-- Preloader -->
    <%@include file="pages/preloader.jsp" %>
    <!-- END Preloader -->

    <div id="page-container" class="sidebar-partial sidebar-visible-lg sidebar-no-animations">
        <!-- Main Sidebar -->
        <%@include file="pages/side-nav.jsp" %>
        <!-- END Main Sidebar -->

        <!-- Main Container -->
        <div id="main-container">
            <%@include file="pages/header.jsp" %>
            <!-- END Header -->

            <!-- Page content -->
            <div id="page-content">
                <!-- Maps Header -->
                <!-- For a map header add the class 'content-header-media' and init a map in a div as in the following example -->
                <div class="content-header content-header-media">
                    <div class="header-section">
                        <h1>
                            <i class="gi gi-pin"></i>Maps<br><small>Easy Google Maps Integration!</small>
                        </h1>
                    </div>
                    <!-- Header Map -->
                    <!-- Gmaps.js (initialized in js/pages/compMaps.js), for more examples you can check out http://hpneo.github.io/gmaps/examples.html -->
                    <div id="gmap-top" class="content-header-media-map"></div>
                </div>
                <ul class="breadcrumb breadcrumb-top">
                    <li>Components</li>
                    <li><a href="">Maps</a></li>
                </ul>
                <!-- END Maps Header -->

                <!-- Google Maps Content -->

                <!-- Geolocation Block -->
                <div class="block full">
                    <!-- Geolocation Title -->
                    <div class="block-title">
                        <h4><strong>Geolocation</strong> Map</h4>
                    </div>
                    <!-- END Geolocation Title -->

                    <!-- Geolocation Content -->
                    <!-- Gmaps.js (initialized in js/pages/compMaps.js), for more examples you can check out http://hpneo.github.io/gmaps/examples.html -->
                    <div id="gmap-geolocation" class="gmap"></div>
                    <!-- END Geolocation Content -->
                </div>
                <!-- END Geolocation Block -->
                <!-- END Google Maps Content -->
            </div>
            <!-- END Page Content -->

            <!-- Footer -->
            <%@include file="pages/footer.jsp" %>
            <!-- END Footer -->
        </div>
        <!-- END Main Container -->
    </div>
    <!-- END Page Container -->
</div>
<!-- END Page Wrapper -->

<!-- Scroll to top link, initialized in js/app.js - scrollToTop() -->
<a href="#" id="to-top"><i class="fa fa-angle-double-up"></i></a>
<%@include file="pages/js_import.jsp" %>

<!-- Google Maps API Key (you will have to obtain a Google Maps API key to use Google Maps) -->
<!-- For more info please have a look at https://developers.google.com/maps/documentation/javascript/get-api-key#key -->
<script src="https://maps.googleapis.com/maps/api/js?key=AIzaSyBqVV6BlpyEaqr7bag9LNAHwpZw8LxjhGg"></script>
<script src="assets/js/helpers/gmaps.min.js"></script>
<script src="main/js/index.js"></script>

</body>
</html>