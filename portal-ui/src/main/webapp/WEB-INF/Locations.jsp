<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8" %>
<!DOCTYPE html>
<html class="no-js" lang="en">
<head>
    <%@include file="pages/css_import.jsp" %>
    <title>Portal - Locations Manager</title>
</head>
<body>
<div id="page-wrapper">
    <!-- Preloader -->
    <%@include file="pages/preloader.jsp" %>
    <!-- END Preloader -->

    <div id="page-container" class="sidebar-partial sidebar-visible-lg sidebar-no-animations">
        <!-- Main Sidebar -->
        <%@include file="pages/side-nav.jsp" %>
        <!-- END Main Sidebar -->

        <!-- Main Container -->
        <div id="main-container">
            <%@include file="pages/header.jsp" %>
            <!-- END Header -->

            <!-- Page content -->
            <div id="page-content">
                <!-- Page Header -->
                <div class="content-header">
                    <div class="header-section">
                        <h1>
                            <i class="gi gi-brush"></i>Locations Manager<br>
                        </h1>
                    </div>
                </div>
                <ul class="breadcrumb breadcrumb-top">
                    <li>Page</li>
                    <li><a href="">Locations</a></li>
                </ul>
                <!-- END Page Header -->

                <!-- Datatables Content -->
                <div class="block full">
                    <div class="block-title">
                        <h2>Locations</h2>
                    </div>


                    <div class="pull-left">
                        <a href="#addLocationModal" class="btn btn-primary" data-toggle="modal"><i
                                class="hi hi-plus fa-fw"></i> Add new</a><br>
                    </div>

                    <br/>
                    <br/>

                    <div class="table-responsive">

                        <table id="locationsDT" class="table table-vcenter table-condensed table-bordered"
                               style="width:100%">
                            <thead>
                            <tr>
                                <th class="text-center tableheader_no">#</th>
                                <th class="text-center tableheader_districs1">Kelurahan</th>
                                <th class="text-center tableheader_districs2">Kecamatan</th>
                                <th class="text-center tableheader_city">Kota</th>
                                <th class="text-center tableheader_province">Provinsi</th>
                                <th class="text-center tableheader_action">Actions</th>
                            </tr>
                            </thead>
                            <tbody>
                            </tbody>
                            <tfoot>
                            <tr>
                                <th class="text-center">#</th>
                                <th class="text-center">Kelurahan</th>
                                <th class="text-center">Kecamatan</th>
                                <th class="text-center">Kota</th>
                                <th class="text-center">Provinsi</th>
                                <th class="text-center">Actions</th>
                            </tr>
                            </tfoot>

                        </table>
                    </div>
                </div>
                <!-- END Datatables Content -->

                <!-- ADD location Modal -->
                <div id="addLocationModal" class="modal fade" tabindex="-1" role="dialog" aria-hidden="true">
                    <div class="modal-dialog">
                        <form id="form-new-location" method="post" class="form-horizontal form-bordered"
                              onsubmit="return false;">
                            <div class="modal-content">
                                <div class="modal-header">
                                    <button type="button" class="close" data-dismiss="modal" aria-hidden="true">
                                        &times;
                                    </button>
                                    <h3 class="modal-title">Add new location</h3>
                                </div>
                                <div class="modal-body">
                                    <!-- Modal content -->
                                    <div class="form-group">
                                        <label class="col-md-4 control-label" for="addInpKelurahan">Kelurahan *</label>
                                        <div class="col-md-6">
                                            <input style="text-transform:uppercase" type="text" id="addInpKelurahan"
                                                   name="addInpKelurahan" class="form-control districs1-typeahead"
                                                   autocomplete="off" required>
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <label class="col-md-4 control-label" for="addInpKecamatan">Kecamatan *</label>
                                        <div class="col-md-6">
                                            <input style="text-transform:uppercase" type="text" id="addInpKecamatan"
                                                   name="addInpKecamatan" class="form-control districs2-typeahead"
                                                   autocomplete="off" required>
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <label class="col-md-4 control-label" for="addInpKota">Kota *</label>
                                        <div class="col-md-6">
                                            <input style="text-transform:uppercase" type="text" id="addInpKota"
                                                   name="addInpKota" class="form-control city-typeahead"
                                                   autocomplete="off" required>
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <label class="col-md-4 control-label" for="addInpProvinsi">Provinsi *</label>
                                        <div class="col-md-6">
                                            <input style="text-transform:uppercase" type="text" id="addInpProvinsi"
                                                   name="addInpProvinsi" class="form-control province-typeahead"
                                                   autocomplete="off" required>
                                        </div>
                                    </div>
                                    <!-- END of Modal content -->
                                </div>
                                <div class="modal-footer form-group form-actions">
                                    <button type="button" class="btn btn-sm btn-default" data-dismiss="modal">Close
                                    </button>
                                    <button type="submit" class="btn btn-sm btn-primary">Save</button>
                                </div>
                            </div>
                        </form>
                    </div>
                </div>
                <!-- END ADD location Modal -->
                <!-- EDIT location Modal -->
                <div id="updateLocationModal" class="modal fade" tabindex="-1" role="dialog" aria-hidden="true">
                    <div class="modal-dialog">
                        <div class="modal-content">
                            <div class="modal-header">
                                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;
                                </button>
                                <h3 class="modal-title">Update location</h3>
                            </div>
                            <div class="modal-body">
                                <!-- Modal content -->
                                Modal Content..

                                <!-- END of Modal content -->
                            </div>
                            <div class="modal-footer">
                                <button type="button" class="btn btn-sm btn-default" data-dismiss="modal">Close</button>
                                <button type="button" class="btn btn-sm btn-primary">Update</button>
                            </div>
                        </div>
                    </div>
                </div>
                <!-- END EDIT location Modal -->

            </div>
            <!-- END Page Content -->

            <!-- Footer -->
            <%@include file="pages/footer.jsp" %>
            <!-- END Footer -->
        </div>
        <!-- END Main Container -->
    </div>
    <!-- END Page Container -->
</div>
<!-- END Page Wrapper -->

<!-- Scroll to top link, initialized in js/app.js - scrollToTop() -->
<a href="#" id="to-top"><i class="fa fa-angle-double-up"></i></a>
<%@include file="pages/js_import.jsp" %>
<script src="main/js/Locations.js"></script>
</body>
</html>