<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8" %>
<!DOCTYPE html>
<html class="no-js" lang="en">
<head>
    <%@include file="pages/css_import.jsp" %>
    <title>Portal - Hospitals Manager</title>
</head>
<body>
<div id="page-wrapper">
    <!-- Preloader -->
    <%@include file="pages/preloader.jsp" %>
    <!-- END Preloader -->

    <div id="page-container" class="sidebar-partial sidebar-visible-lg sidebar-no-animations">
        <!-- Main Sidebar -->
        <%@include file="pages/side-nav.jsp" %>
        <!-- END Main Sidebar -->

        <!-- Main Container -->
        <div id="main-container">
            <%@include file="pages/header.jsp" %>
            <!-- END Header -->

            <!-- Page content -->
            <div id="page-content">
                <!-- Page Header -->
                <div class="content-header">
                    <div class="header-section">
                        <h1>
                            <i class="gi gi-brush"></i>Hospitals Manager<br>
                        </h1>
                    </div>
                </div>
                <ul class="breadcrumb breadcrumb-top">
                    <li>Page</li>
                    <li><a href="">Hospitals and Clinics</a></li>
                </ul>
                <!-- END Page Header -->

                <!-- Datatables Content -->
                <div class="block full">
                    <div class="block-title">
                        <h2>Hospitals and Clinics</h2>
                    </div>

                    <div class="pull-left">
                        <a href="add-new-hospital" class="btn btn-primary"><i class="hi hi-plus fa-fw"></i> Add
                            new</a><br>
                    </div>
                    <br/>
                    <br/>

                    <div class="table-responsive">
                        <table id="hospitalsDT" class="table table-vcenter table-condensed table-bordered table-hover"
                               style="width:100%">
                            <thead>
                            <tr>
                                <th class="text-center tableheader_no">#</th>
                                <th class="text-center tableheader_id">ID</th>
                                <th class="text-center tableheader_name">Name</th>
                                <th class="text-center tableheader_contact">Contact</th>
                                <th class="text-center tableheader_address">Address</th>
                                <th class="text-center tableheader_totaldr">Total Dr Obsgyn</th>
                                <th class="text-center tableheader_bedsavailable">Beds Available</th>
                                <th class="text-center tableheader_action">Action</th>
                            </tr>
                            </thead>
                            <tbody>

                            </tbody>
                            <tfoot>
                            <th class="text-center"></th>
                            <th class="text-center">ID</th>
                            <th class="text-center">Name</th>
                            <th class="text-center">Contact</th>
                            <th class="text-center">Address</th>
                            <th class="text-center">Total Dr Obsgyn</th>
                            <th class="text-center">Beds Available</th>
                            <th class="text-center"></th>
                            </tfoot>
                        </table>
                    </div>
                </div>
                <!-- END Datatables Content -->
            </div>
            <!-- END Page Content -->

            <!-- Footer -->
            <%@include file="pages/footer.jsp" %>
            <!-- END Footer -->
        </div>
        <!-- END Main Container -->
    </div>
    <!-- END Page Container -->
</div>
<!-- END Page Wrapper -->

<!-- Scroll to top link, initialized in js/app.js - scrollToTop() -->
<a href="#" id="to-top"><i class="fa fa-angle-double-up"></i></a>
<%@include file="pages/js_import.jsp" %>
<script src="main/js/HospitalsAndClinics.js"></script>
</body>
</html>