<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8" %>
<!-- Main Sidebar -->
<div id="sidebar">
    <!-- Wrapper for scrolling functionality -->
    <div id="sidebar-scroll">
        <!-- Sidebar Content -->
        <div class="sidebar-content">
            <!-- Brand -->
            <a href="index" class="sidebar-brand">
                <i class="gi gi-flash"></i><span class="sidebar-nav-mini-hide"><strong>IO-</strong>PORTAL</span>
            </a>
            <!-- END Brand -->

            <!-- User Info -->
            <div class="sidebar-section sidebar-user clearfix sidebar-nav-mini-hide">
                <div class="sidebar-user-avatar">
                    <a href="page_ready_user_profile.html">
                        <img src="assets/img/placeholders/avatars/avatar2.jpg" alt="avatar">
                    </a>
                </div>
                <div class="sidebar-user-name">John Doe</div>
                <div class="sidebar-user-links">
                    <a href="page_ready_user_profile.html" data-toggle="tooltip" data-placement="bottom"
                       title="Profile"><i class="gi gi-user"></i></a>
                    <a href="page_ready_inbox.html" data-toggle="tooltip" data-placement="bottom" title="Messages"><i
                            class="gi gi-envelope"></i></a>
                    <!-- Opens the user settings modal that can be found at the bottom of each page (page_footer.html in PHP version) -->
                    <a href="javascript:void(0)" class="enable-tooltip" data-placement="bottom" title="Settings"
                       onclick="$('#modal-user-settings').modal('show');"><i class="gi gi-cogwheel"></i></a>
                    <a href="login" data-toggle="tooltip" data-placement="bottom" title="Logout"><i
                            class="gi gi-exit"></i></a>
                </div>
            </div>
            <!-- END User Info -->

            <!-- Theme Colors -->
            <!-- Change Color Theme functionality can be found in js/app.js - templateOptions() -->
            <ul class="sidebar-section sidebar-themes clearfix sidebar-nav-mini-hide">

            </ul>
            <!-- END Theme Colors -->

            <!-- Sidebar Navigation -->
            <ul class="sidebar-nav">

                <li class="sidebar-header">
                    <span class="sidebar-header-options clearfix"><a href="javascript:void(0)" data-toggle="tooltip"
                                                                     title="Applications Management"><i
                            class="gi gi-lightbulb"></i></a></span>
                    <span class="sidebar-header-title">Managers</span>
                </li>
                <li>
                    <a href="#" class="sidebar-nav-menu"><i
                            class="fa fa-angle-left sidebar-nav-indicator sidebar-nav-mini-hide"></i><i
                            class="gi gi-building sidebar-nav-icon"></i><span
                            class="sidebar-nav-mini-hide">Manager</span></a>
                    <ul>
                        <a href="#" class="sidebar-nav-submenu"><i
                                class="fa fa-angle-left sidebar-nav-indicator sidebar-nav-mini-hide"></i><i
                                class="gi gi-hospital sidebar-nav-icon"></i><span class="sidebar-nav-mini-hide">Hospitals Manager</span></a>
                        <ul>
                            <li>
                                <a href="hospital-and-clinics">Hospitals and Clinics</a>
                            </li>
                            <li>
                                <a href="hospital-and-clinics-bulk-import">Bulk import</a>
                            </li>
                        </ul>
                        <a href="#" class="sidebar-nav-submenu"><i
                                class="fa fa-angle-left sidebar-nav-indicator sidebar-nav-mini-hide"></i><i
                                class="gi gi-google_maps sidebar-nav-icon"></i><span class="sidebar-nav-mini-hide">Locations Manager</span></a>
                        <ul>
                            <li>
                                <a href="locations">Locations</a>
                            </li>
                            <li>
                                <a href="locations-bulk-import">Bulk import</a>
                            </li>
                        </ul>
                    </ul>
                </li>

                <li class="sidebar-header">
                    <span class="sidebar-header-options clearfix"><a href="javascript:void(0)" data-toggle="tooltip"
                                                                     title="Application Settings"><i
                            class="gi gi-settings"></i></a></span>
                    <span class="sidebar-header-title">Configurations</span>
                </li>
                <li>
                    <a href="#" class="sidebar-nav-menu"><i
                            class="fa fa-angle-left sidebar-nav-indicator sidebar-nav-mini-hide"></i><i
                            class="gi gi-settings sidebar-nav-icon"></i><span class="sidebar-nav-mini-hide">Configurations</span></a>
                    <ul>
                        <li>
                            <a href="#">General Settings</a>
                        </li>
                        <li>
                            <a href="#">External settings</a>
                        </li>
                    </ul>
                </li>
            </ul>
            <!-- END Sidebar Navigation -->
        </div>
        <!-- END Sidebar Content -->
    </div>
    <!-- END Wrapper for scrolling functionality -->
</div>
<!-- END Main Sidebar -->