$('#form-login').submit(function (event) {
    event.preventDefault();
    const username = $('#form-login input[name="login-username"]').val();
    const password = $('#form-login input[name="login-password"]').val();

    const loginUrl = CONSTANTS.PORTAL_BACK_END + '/api/admin/login';

    const loginData = {
        username: username,
        password: password
    };
    $.ajax({
        method: 'POST',
        url: loginUrl,
        dataType: 'json',
        contentType: 'application/json; charset=utf-8',
        data: JSON.stringify(loginData),
        success: function (response) {

            window.location.replace('index');
        },
        error: function (error) {
            // clear fields
            $('#form-login input[name="login-password"]').val('');
            // show error message
            errorNotification(error.responseJSON.reason);

        }
    });

});