/**
 * DO NOT ALTER OR REMOVE COPYRIGHT NOTICES OR THIS HEADER.
 *
 * Copyright (c) 2020 IO-Teknologi Indonesia, and individual contributors
 * as indicated by the @author tags. All Rights Reserved
 *
 * The contents of this file are subject to the terms of the
 * Common Development and Distribution License (the License).
 *
 * Everyone is permitted to copy and distribute verbatim copies
 * of this license document, but changing it is not allowed.
 *
 */

$(document).ready(function () {
    validateSession();
    generateLocationDT();

    generateDistrics1();
    generateDistrics2();
    generateCity();
    generateProvince();

});

function generateLocationDT() {
    App.datatables();

    /* Add placeholder attribute to the search input */
    $('.dataTables_filter input').attr('placeholder', 'Search');

    var locationUrl = CONSTANTS.PORTAL_BACK_END + '/api/locations?query_filter=ALL&key=ALL';
    locationsDT = $('#locationsDT').dataTable({
        stateSave: true,
        dom: '<"myfilter"fB><"mylength"l>tip',
        pageLength: 10,
        lengthMenu: [[10, 20, 30, -1], [10, 20, 30, 'All']],
        responsive: true,
        autowidth: true,
        ajax: {
            type: 'GET',
            url: locationUrl,
            dataSrc: ''
        },
        columnDefs: [{
            orderable: false,
            targets: 'tableheader_no',
            data: '',
            defaultContent: ''
        }, {
            targets: 'tableheader_districs1',
            data: 'districs1',
            defaultContent: ''
        }, {
            targets: 'tableheader_districs2',
            data: 'districs2',
            defaultContent: ''
        }, {
            targets: 'tableheader_city',
            data: 'city',
            defaultContent: ''
        }, {
            targets: 'tableheader_province',
            data: 'province',
            defaultContent: ''
        }, {
            orderable: false,
            className: 'text-center',
            targets: 'tableheader_action',
            data: '',
            defaultContent: '<td><div class="btn-group"><a href="#updateLocationModal" data-toggle="modal" title="Edit" class="btn btn-default"><i class="fa fa-pencil"></i></a><a href="#" data-toggle="modal" title="Delete" class="btn btn-danger"><i class="fa fa-times"></i></a></div></td>'

        }],
        initComplete: function initComplete() {
            var urlParams = new URLSearchParams(window.location.search);
            if (urlParams.has('username')) {
                UserWithoutManagerDT.rows().every(function (e) {
                    if (((this.data() || {}).user || {}).userName == urlParams.get('username')) {
                        this.select();
                    }
                });
            }
        }
    });
}

function generateDistrics1() {
    const districs1ListUrl = CONSTANTS.PORTAL_BACK_END + '/api/locations/key?query_filter=districs1';

    $.ajax({
        method: 'GET',
        url: districs1ListUrl,
        dataType: 'json',
        contentType: 'application/json; charset=utf-8',
        success: function (response) {
            $('.districs1-typeahead').typeahead({source: response});
        },
        error: function (error) {
        }
    });
}

function generateDistrics2() {
    const districs2ListUrl = CONSTANTS.PORTAL_BACK_END + '/api/locations/key?query_filter=districs2';

    $.ajax({
        method: 'GET',
        url: districs2ListUrl,
        dataType: 'json',
        contentType: 'application/json; charset=utf-8',
        success: function (response) {
            $('.districs2-typeahead').typeahead({source: response});

        },
        error: function (error) {
        }
    });
}

function generateCity() {
    const cityListUrl = CONSTANTS.PORTAL_BACK_END + '/api/locations/key?query_filter=city';

    $.ajax({
        method: 'GET',
        url: cityListUrl,
        dataType: 'json',
        contentType: 'application/json; charset=utf-8',
        success: function (response) {
            $('.city-typeahead').typeahead({source: response});

        },
        error: function (error) {
        }
    });
}

function generateProvince() {
    const provinceListUrl = CONSTANTS.PORTAL_BACK_END + '/api/locations/key?query_filter=province';

    $.ajax({
        method: 'GET',
        url: provinceListUrl,
        dataType: 'json',
        contentType: 'application/json; charset=utf-8',
        success: function (response) {
            $('.province-typeahead').typeahead({source: response});

        },
        error: function (error) {
        }
    });
}

$('#form-new-location').submit(function (event) {
    event.preventDefault();

    var districs1 = $('#form-new-location input[name="addInpKelurahan"]').val();
    var districs2 = $('#form-new-location input[name="addInpKecamatan"]').val();
    var city = $('#form-new-location input[name="addInpKota"]').val();
    var province = $('#form-new-location input[name="addInpProvinsi"]').val();

    const addLocationUrl = CONSTANTS.PORTAL_BACK_END + '/api/locations';
    const data = {
        districs1: districs1,
        districs2: districs2,
        city: city,
        province: province
    };

    $.ajax({
        method: 'POST',
        url: addLocationUrl,
        dataType: 'json',
        contentType: 'application/json; charset=utf-8',
        data: JSON.stringify(data),
        success: function (response) {
            successNotification('Successfull add new location');
            setTimeout(function () {
                location.reload();
            }, 2000);
        },
        error: function (error) {
        }
    });

});
