/**
 * DO NOT ALTER OR REMOVE COPYRIGHT NOTICES OR THIS HEADER.
 *
 * Copyright (c) 2020 IO-Teknologi Indonesia, and individual contributors
 * as indicated by the @author tags. All Rights Reserved
 *
 * The contents of this file are subject to the terms of the
 * Common Development and Distribution License (the License).
 *
 * Everyone is permitted to copy and distribute verbatim copies
 * of this license document, but changing it is not allowed.
 *
 */

$(document).ready(function () {

    var url = window.location;
    $('#sidebar-nav a[href="' + url + '"]').addClass('active');


    validateSession();
    generateHospitalDT();


});

function generateHospitalDT() {
    App.datatables();

    /* Add placeholder attribute to the search input */
    $('.dataTables_filter input').attr('placeholder', 'Search');

    var hospitalUrl = CONSTANTS.PORTAL_BACK_END + '/api/hospitals';

    hospitalDT = $('#hospitalsDT').dataTable({
        stateSave: true,
        dom: '<"myfilter"fB><"mylength"l>tip',
        pageLength: 10,
        lengthMenu: [[10, 20, 30, -1], [10, 20, 30, 'All']],
        responsive: true,
        autowidth: true,
        ajax: {
            type: 'GET',
            url: hospitalUrl,
            dataSrc: ''
        },
        columnDefs: [{
            orderable: false,
            targets: 'tableheader_no',
            data: '',
            className: 'table-hover',
            defaultContent: ''
        }, {
            targets: 'tableheader_id',
            data: 'id',
            className: 'table-hover',
            defaultContent: ''
        }, {
            targets: 'tableheader_name',
            data: 'name',
            className: 'bolded table-hover',
            defaultContent: ''
        }, {
            targets: 'tableheader_contact',
            data: 'contact',
            className: 'table-hover',
            defaultContent: ''
        }, {
            targets: 'tableheader_address',
            data: 'address',
            className: 'table-hover',
            defaultContent: ''
        }, {
            targets: 'tableheader_totaldr',
            data: 'totalDr',
            className: 'text-center table-hover',
            defaultContent: ''

        }, {
            targets: 'tableheader_bedsavailable',
            data: 'bedsAvailable',
            className: 'text-center',
            defaultContent: ''
        }, {
            orderable: false,
            className: 'text-center',
            targets: 'tableheader_action',
            data: '',
            defaultContent: '<td><div class="btn-group"><a href="#updateLocationModal" title="Details" class="btn btn-default"><i class="gi gi-eye_open"></i></a><a href="#" data-toggle="modal" title="Delete" class="btn btn-danger"><i class="fa fa-times"></i></a></div></td>'

        }],
        initComplete: function initComplete() {
            var urlParams = new URLSearchParams(window.location.search);
            if (urlParams.has('id')) {
                hospitalDT.rows().every(function (e) {
                    if (((this.data() || {}).user || {}).id == urlParams.get('id')) {
                        this.select();
                    }
                });
            }
        }
    });
}
