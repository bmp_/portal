/**
 * DO NOT ALTER OR REMOVE COPYRIGHT NOTICES OR THIS HEADER.
 *
 * Copyright (c) 2020 IO-Teknologi Indonesia, and individual contributors
 * as indicated by the @author tags. All Rights Reserved
 *
 * The contents of this file are subject to the terms of the
 * Common Development and Distribution License (the License).
 *
 * Everyone is permitted to copy and distribute verbatim copies
 * of this license document, but changing it is not allowed.
 *
 */

$(document).ready(function () {

    var url = window.location;
    $('#sidebar-nav a[href="' + url + '"]').addClass('active');
    validateSession();

    generateMaps();


});

function generateMaps() {
    $('.gmap').css('height', '350px');

    var gmapGeolocation = new GMaps({
        div: '#gmap-geolocation',
        lat: 0,
        lng: 0,
        scrollwheel: false
    });

    GMaps.geolocate({
        success: function (position) {
            gmapGeolocation.setCenter(position.coords.latitude, position.coords.longitude);
            gmapGeolocation.addMarker({
                lat: position.coords.latitude,
                lng: position.coords.longitude,
                animation: google.maps.Animation.DROP,
                title: 'GeoLocation',
                infoWindow: {
                    content: '<div class="text-success"><i class="fa fa-map-marker"></i> <strong>Your location!</strong></div>'
                }
            });
        },
        error: function (error) {
            alert('Geolocation failed: ' + error.message);
        },
        not_supported: function () {
            alert("Your browser does not support geolocation");
        },
        always: function () {
            // Message when geolocation succeed
        }
    });
}