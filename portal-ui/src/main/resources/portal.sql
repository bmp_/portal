-- MySQL dump 10.13  Distrib 8.0.23, for osx10.16 (x86_64)
--
-- Host: localhost    Database: portal
-- ------------------------------------------------------
-- Server version	8.0.19

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!50503 SET NAMES utf8mb4 */;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

--
-- Table structure for table `configuration`
--

DROP TABLE IF EXISTS `configuration`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `configuration` (
  `id` int NOT NULL AUTO_INCREMENT,
  `config_key` varchar(50) DEFAULT NULL,
  `config_value` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=19 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `configuration`
--

LOCK TABLES `configuration` WRITE;
/*!40000 ALTER TABLE `configuration` DISABLE KEYS */;
INSERT INTO `configuration` VALUES (1,'PORTAL_SERVICE_URL','http://localhost:8080/portal'),(2,'ENCRYPTION_KEY','@#1D%7887F&108!8780FEF*88&%!@d97d0!'),(3,'MAIL_SERVER_FROM_ADDRESS','system@portal.com'),(4,'MAIL_SERVER_PREFIX','[Portal]'),(5,'MAIL_SECURED_SMTP','true'),(6,'MAIL_HOST_NAME','smtp.gmail.com'),(7,'MAIL_SMTP_PORT','587'),(8,'MAIL_PASSWORD','qrz9TJe46TtCkNeq5s8Ygw=='),(9,'MAIL_USERNAME','bachtiarpermadi01@gmail.com'),(10,'MAIL_SMTP_TLS','true'),(11,'MAIL_SMTP_SSL','false'),(12,'MAIL_SMTP_AUTH','true'),(13,'OTP_ENABLED','true'),(14,'OTP_VALIDITY','3600'),(15,'OTP_LENGTH','4'),(17,'OTP_MAIL_SUBJECT','OTP Validation'),(18,'OTP_MAIL_CONTENT','Your OTP code :\n\n&otpToken&\n\nExpired in &otpValidity& minute/s. Do not share this code with others. \n\n\n\nKind regards,\nsupport@io-technology.id');
/*!40000 ALTER TABLE `configuration` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `doctors`
--

DROP TABLE IF EXISTS `doctors`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `doctors` (
  `id` varchar(50) NOT NULL,
  `hospitalid` varchar(15) CHARACTER SET latin1 COLLATE latin1_swedish_ci DEFAULT NULL,
  `name` varchar(100) DEFAULT NULL,
  `contact` varchar(50) DEFAULT NULL,
  `createdt` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`),
  KEY `doctors_FK` (`hospitalid`),
  CONSTRAINT `doctors_FK` FOREIGN KEY (`hospitalid`) REFERENCES `hospitals` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `doctors`
--

LOCK TABLES `doctors` WRITE;
/*!40000 ALTER TABLE `doctors` DISABLE KEYS */;
INSERT INTO `doctors` VALUES ('b1e75db2-725d-5423-93c9-ecda2bc3e33c','2071015','Bachtiar Madya Permadi','085747079410','2020-11-07 02:42:16'),('e36c32d4-144d-5e1c-be4f-68cbdf5b9a38','3402042','Jesse','081414141414','2020-11-19 19:40:27'),('ef08937c-0f91-5041-ab87-baafaa9e2773','2071015','Jamal Ahnan Fuadi','081414079410','2020-11-19 13:50:23');
/*!40000 ALTER TABLE `doctors` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `doctors_schedule`
--

DROP TABLE IF EXISTS `doctors_schedule`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `doctors_schedule` (
  `id` int NOT NULL AUTO_INCREMENT,
  `doctor_id` varchar(50) DEFAULT NULL,
  `day` varchar(10) DEFAULT NULL,
  `time_start` time DEFAULT NULL,
  `time_end` time DEFAULT NULL,
  `createdt` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`),
  KEY `doctors_schedule_FK` (`doctor_id`),
  CONSTRAINT `doctors_schedule_FK` FOREIGN KEY (`doctor_id`) REFERENCES `doctors` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=8 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `doctors_schedule`
--

LOCK TABLES `doctors_schedule` WRITE;
/*!40000 ALTER TABLE `doctors_schedule` DISABLE KEYS */;
INSERT INTO `doctors_schedule` VALUES (1,'b1e75db2-725d-5423-93c9-ecda2bc3e33c','senin','09:00:00','14:00:00','2020-11-07 02:56:19'),(2,'b1e75db2-725d-5423-93c9-ecda2bc3e33c','selasa','09:00:00','14:00:00','2020-11-07 02:56:19'),(3,'b1e75db2-725d-5423-93c9-ecda2bc3e33c','rabu','09:00:00','14:00:00','2020-11-07 02:56:19'),(4,'ef08937c-0f91-5041-ab87-baafaa9e2773','kamis','09:00:00','14:00:00','2020-11-07 02:56:19'),(5,'e36c32d4-144d-5e1c-be4f-68cbdf5b9a38','senin','09:00:00','14:00:00','2020-11-19 19:42:28'),(6,'e36c32d4-144d-5e1c-be4f-68cbdf5b9a38','selasa','09:00:00','14:00:00','2020-11-19 19:42:28'),(7,'e36c32d4-144d-5e1c-be4f-68cbdf5b9a38','rabu','09:00:00','14:00:00','2020-11-19 19:42:28');
/*!40000 ALTER TABLE `doctors_schedule` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `emergency`
--

DROP TABLE IF EXISTS `emergency`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `emergency` (
  `id` int NOT NULL AUTO_INCREMENT,
  `userid` varchar(50) NOT NULL,
  `lat` varchar(20) DEFAULT NULL,
  `lng` varchar(20) NOT NULL,
  `status` varchar(50) CHARACTER SET latin1 COLLATE latin1_swedish_ci NOT NULL DEFAULT 'in_progress',
  `createdt` timestamp NULL DEFAULT CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=17 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `emergency`
--

LOCK TABLES `emergency` WRITE;
/*!40000 ALTER TABLE `emergency` DISABLE KEYS */;
INSERT INTO `emergency` VALUES (11,'79ded0b6-5228-5f4a-ad16-00e1e2125f82','-','-','in_progress','2021-03-09 14:22:39'),(12,'2a6b83f8-e75c-5106-925d-a00b7dab8d2a','1','2','in_progress','2021-03-09 19:21:45'),(13,'2a6b83f8-e75c-5106-925d-a00b7dab8d2a','1','2','in_progress','2021-03-09 19:21:46'),(14,'2a6b83f8-e75c-5106-925d-a00b7dab8d2a','1','2','in_progress','2021-03-09 19:21:47'),(15,'2a6b83f8-e75c-5106-925d-a00b7dab8d2a','-','-','in_progress','2021-03-09 19:30:09'),(16,'2a6b83f8-e75c-5106-925d-a00b7dab8d2a','-','-','in_progress','2021-03-09 19:31:55');
/*!40000 ALTER TABLE `emergency` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `hospitals`
--

DROP TABLE IF EXISTS `hospitals`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `hospitals` (
  `id` varchar(15) NOT NULL,
  `name` varchar(100) DEFAULT NULL,
  `address` text,
  `contact` varchar(50) DEFAULT NULL,
  `lat` varchar(50) DEFAULT NULL,
  `lng` varchar(50) DEFAULT NULL,
  `districts` varchar(50) CHARACTER SET latin1 COLLATE latin1_swedish_ci DEFAULT NULL,
  `city` varchar(50) DEFAULT NULL,
  `province` varchar(50) DEFAULT NULL,
  `inpatient` varchar(50) DEFAULT NULL,
  `outpatient` varchar(50) DEFAULT NULL,
  `ponek` varchar(50) DEFAULT NULL,
  `doctorobsgynstanby` int DEFAULT NULL,
  `doctorobsgynnonstanby` int DEFAULT NULL,
  `totaldoctor` int DEFAULT NULL,
  `bedsavailable` int DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `hospitals`
--

LOCK TABLES `hospitals` WRITE;
/*!40000 ALTER TABLE `hospitals` DISABLE KEYS */;
INSERT INTO `hospitals` VALUES ('2071015','RS Santa Elisabeth','Jl. Ganjuran Sumbermulyo Bambanglipuro, Kaligondang, Sumbermulyo, Kec. Bantul, Kab. Bantul, Daerah Istimewa Yogyakarta 55764, Indonesia','0274367502',' ',' ','bantul','yogyakarta','yogyakarta',' ',' ',' ',1,0,1,20),('3402016','RS Umum Daerah Panembahan Senopati','Jl. Dr. Wahidin Sudiro Husodo, Area Sawah, Trirenggo, Kec. Bantul, Kab. Bantul, Daerah Istimewa Yogyakarta 55714, Indonesia','0274 367381',' ',' ','bantul','yogyakarta','yogyakarta',' ',' ',' ',NULL,NULL,NULL,NULL),('3402042','RS Ibu Anak Ummi Khasanah','Jl. Pemuda, Gandekan, Babadan, Bantul, Kec. Bantul, Kab. Bantul, Daerah Istimewa Yogyakarta 55711, Indonesia','0274367638',' ',' ','bantul','yogyakarta','yogyakarta',' ',' ',' ',NULL,NULL,NULL,NULL),('3402053','RS  Nur Hidayah','Jl. Imogiri Tim, No. 11 Km, Rw 5, Bembem, Trimulyo, Kec. Jetis, Kab. Bantul, Daerah Istimewa Yogyakarta 55781, Indonesia','085100472941',' ',' ','bantul','yogyakarta','yogyakarta',' ',' ',' ',NULL,NULL,NULL,NULL),('3402064','RS Umum Rachma Husada ','Jl. Parangtritis Km. 16, Garselo Patalan, Jetis, Patalan, Bantul, Ketandan, Patalan, Kec. Jetis, Kab. Bantul, Daerah Istimewa Yogyakarta 55781, Indonesia','0274 6460091',' ',' ','bantul','yogyakarta','yogyakarta',' ',' ',' ',NULL,NULL,NULL,NULL),('3402076','RS Khusus Bedah Bantul','Jl. Ringroad Selatan, Panggungharjo, Sewon, Glugo, Kec. Sewon, Kab. Bantul, Daerah Istimewa Yogyakarta 55188, Indonesia','0274 376115',' ',' ','bantul','yogyakarta','yogyakarta',' ',' ',' ',NULL,NULL,NULL,NULL),('3402080','RS  Ibu  Anak Kahyangan','Jl. Tino Sidin No.390 Kadipiro, Kadipiro, Ngestiharjo, Kec. Kasihan, Kab. Bantul, Daerah Istimewa Yogyakarta 55184, Indonesia','0274 618953',' ',' ','bantul','yogyakarta','yogyakarta',' ',' ',' ',NULL,NULL,NULL,NULL),('3402081','Rumah Sakit Bedah Adelia','Jl. Monumen Perjuangan TNI AU No. 1A, Krobikan, Tamanan, Kec. Banguntapan, Kab. Bantul, Daerah Istimewa Yogyakarta 55191, Indonesia','0274 4396680',' ',' ','bantul','yogyakarta','yogyakarta',' ',' ',' ',NULL,NULL,NULL,NULL),('3402082','RS Ibu Anak Adinda','Jl. Soragan No. 14, Soragan, Ngestiharjo, Kec. Kasihan, Kab. Bantul, Daerah Istimewa Yogyakarta 55184, Indonesia','0274 5305605',' ',' ','bantul','yogyakarta','yogyakarta',' ',' ',' ',NULL,NULL,NULL,NULL),('3402086','RS Rajawali Citra ','Jl. Pleret, Banjardadap, Potorono, Kec. Banguntapan, Kab. Bantul, Daerah Istimewa Yogyakarta 55196, Indonesia','085100482003',' ',' ','bantul','yogyakarta','yogyakarta',' ',' ',' ',NULL,NULL,20,15),('3471074','RSPAU Dr.  Suhardi Harjolukito','Jl. Janti Yogyakarta, Lanud Adisutjipto, Jl. Ringroad Timur, Karang Janbe, Banguntapan, Kec. Banguntapan, Kab. Bantul, Daerah Istimewa Yogyakarta 55198, Indonesia','0274 444702',' ',' ','bantul','yogyakarta','yogyakarta',' ',' ',' ',NULL,NULL,NULL,NULL),('3471121','RS Paru Respira','Jl. Panembahan Senopati No. 4, Dagaran, Palbapang, Kec. Bantul, Kab. Bantul, Daerah Istimewa Yogyakarta 55713, Indonesia','0274367326',' ',' ','bantul','yogyakarta','yogyakarta',' ',' ',' ',NULL,NULL,NULL,NULL);
/*!40000 ALTER TABLE `hospitals` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `locations`
--

DROP TABLE IF EXISTS `locations`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `locations` (
  `id` int NOT NULL AUTO_INCREMENT,
  `districs1` varchar(100) DEFAULT NULL,
  `districs2` varchar(100) DEFAULT NULL,
  `city` varchar(100) DEFAULT NULL,
  `province` varchar(100) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=19 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `locations`
--

LOCK TABLES `locations` WRITE;
/*!40000 ALTER TABLE `locations` DISABLE KEYS */;
INSERT INTO `locations` VALUES (1,'kasihan','bantul','yogyakarta','DI. YOGYAKARTA'),(2,'bantul','bantul','yogyakarta','DI. YOGYAKARTA'),(3,'imogiri','bantul','yogyakarta','DI. YOGYAKARTA'),(4,'temon','kulon progo','yogyakarta','DI. YOGYAKARTA'),(5,'wates','kulon progo','yogyakarta','DI. YOGYAKARTA'),(6,'sentolo','kulon progo','yogyakarta','DI. YOGYAKARTA'),(7,'purwokerto barat','banyumas','purwokerto','JAWA TENGAH'),(8,'sokaraja','banyumas','purwokerto','JAWA TENGAH'),(9,'cingcin','soreang','bandung','JAWA BARAT'),(10,'pakemitan','cinambo','bandung','JAWA BARAT'),(11,'pasteur','sukajadi','bandung','JAWA BARAT'),(14,'turangga','lengkong','bandung','JAWA BARAT'),(18,'banguntapan','bantul','yogyakarta','DI. YOGYAKARTA');
/*!40000 ALTER TABLE `locations` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `managers`
--

DROP TABLE IF EXISTS `managers`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `managers` (
  `userid` varchar(50) NOT NULL,
  `fullname` varchar(100) NOT NULL,
  `email` varchar(50) NOT NULL,
  `phone` varchar(50) NOT NULL,
  `avatar` varchar(250) DEFAULT NULL,
  `createdt` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `isactive` tinyint DEFAULT '0',
  PRIMARY KEY (`userid`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `managers`
--

LOCK TABLES `managers` WRITE;
/*!40000 ALTER TABLE `managers` DISABLE KEYS */;
/*!40000 ALTER TABLE `managers` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `otplog`
--

DROP TABLE IF EXISTS `otplog`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `otplog` (
  `id` int NOT NULL AUTO_INCREMENT,
  `userid` varchar(50) NOT NULL,
  `token` varchar(30) NOT NULL,
  `createdt` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `info` varchar(100) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=85 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `otplog`
--

LOCK TABLES `otplog` WRITE;
/*!40000 ALTER TABLE `otplog` DISABLE KEYS */;
INSERT INTO `otplog` VALUES (83,'b41c04ac-f20a-51ae-a660-6aced46a4327','1711','2021-03-07 16:57:26','register');
/*!40000 ALTER TABLE `otplog` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `systemadministrators`
--

DROP TABLE IF EXISTS `systemadministrators`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `systemadministrators` (
  `userid` varchar(50) NOT NULL,
  `username` varchar(30) DEFAULT NULL,
  `password` varchar(50) DEFAULT NULL,
  `givenname` varchar(50) NOT NULL,
  `role` int NOT NULL,
  `isactive` tinyint NOT NULL DEFAULT '0',
  `createdt` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  PRIMARY KEY (`userid`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `systemadministrators`
--

LOCK TABLES `systemadministrators` WRITE;
/*!40000 ALTER TABLE `systemadministrators` DISABLE KEYS */;
INSERT INTO `systemadministrators` VALUES ('ea6d3157-39de-5a31-bead-a31d0e5b95d8','ykadmin','AquMy/6jndK+9GlqHb9HVw==','Yogyakarta Administrator',2,1,'2020-10-27 05:39:12'),('fe362530-d485-56df-af94-874ccff43572','sysadmin','AquMy/6jndK+9GlqHb9HVw==','Super Administrator',1,1,'2020-10-27 05:39:11');
/*!40000 ALTER TABLE `systemadministrators` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `user_addresses`
--

DROP TABLE IF EXISTS `user_addresses`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `user_addresses` (
  `userid` varchar(50) NOT NULL,
  `address` text,
  `zone` varchar(30) DEFAULT NULL,
  `city` varchar(50) DEFAULT NULL,
  `lat` varchar(20) DEFAULT NULL,
  `lng` varchar(20) DEFAULT NULL,
  PRIMARY KEY (`userid`),
  CONSTRAINT `users_address_FK` FOREIGN KEY (`userid`) REFERENCES `users` (`userid`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `user_addresses`
--

LOCK TABLES `user_addresses` WRITE;
/*!40000 ALTER TABLE `user_addresses` DISABLE KEYS */;
INSERT INTO `user_addresses` VALUES ('2a6b83f8-e75c-5106-925d-a00b7dab8d2a','Jl Sarwodadi Raya, RT 02 RW 08, Purwokerto Kidul, Purwokerto Selatan, Banyumas, Jawa Tengah 53147','Purwokerto Kidul','Purwokerto','-7.432653','109.256262'),('79ded0b6-5228-5f4a-ad16-00e1e2125f82','Bandung, Pasir Biru, Kec. Cibiru, Kota Bandung, Jawa Barat 40615','Pasir Biru','Bandung','-6.925432345708487','107.72325310664195'),('8a03edf2-8ba4-59c8-abb6-b49037b69ce7',NULL,NULL,NULL,NULL,NULL);
/*!40000 ALTER TABLE `user_addresses` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `user_authentications`
--

DROP TABLE IF EXISTS `user_authentications`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `user_authentications` (
  `userid` varchar(50) NOT NULL,
  `password` varchar(50) DEFAULT NULL,
  PRIMARY KEY (`userid`),
  CONSTRAINT `users_authentication_FK` FOREIGN KEY (`userid`) REFERENCES `users` (`userid`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `user_authentications`
--

LOCK TABLES `user_authentications` WRITE;
/*!40000 ALTER TABLE `user_authentications` DISABLE KEYS */;
INSERT INTO `user_authentications` VALUES ('2a6b83f8-e75c-5106-925d-a00b7dab8d2a','M/dzVgtP2Ks69iKhmQD5jQ=='),('79ded0b6-5228-5f4a-ad16-00e1e2125f82','AquMy/6jndK+9GlqHb9HVw=='),('8a03edf2-8ba4-59c8-abb6-b49037b69ce7','M/dzVgtP2Ks69iKhmQD5jQ==');
/*!40000 ALTER TABLE `user_authentications` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `user_gestations`
--

DROP TABLE IF EXISTS `user_gestations`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `user_gestations` (
  `id` int NOT NULL AUTO_INCREMENT,
  `userid` varchar(100) NOT NULL,
  `babygender` varchar(15) DEFAULT NULL,
  `babyname` varchar(100) DEFAULT NULL,
  `insemination` date DEFAULT NULL,
  `duedate` date DEFAULT NULL,
  `firstpregnancy` varchar(5) CHARACTER SET latin1 COLLATE latin1_swedish_ci DEFAULT 'false',
  `misbirth` varchar(5) CHARACTER SET latin1 COLLATE latin1_swedish_ci DEFAULT 'false',
  `beenborn` varchar(5) CHARACTER SET latin1 COLLATE latin1_swedish_ci DEFAULT 'false',
  `createdt` timestamp NULL DEFAULT CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=6 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `user_gestations`
--

LOCK TABLES `user_gestations` WRITE;
/*!40000 ALTER TABLE `user_gestations` DISABLE KEYS */;
INSERT INTO `user_gestations` VALUES (1,'79ded0b6-5228-5f4a-ad16-00e1e2125f82','w','Puspa','2021-01-01','2021-09-11','true','false','false','2021-02-10 17:28:21'),(2,'8a03edf2-8ba4-59c8-abb6-b49037b69ce7','Perempuan','Bunga','2021-02-01','2021-11-08','true','false','false','2021-02-10 17:51:47'),(5,'2a6b83f8-e75c-5106-925d-a00b7dab8d2a','w','Hanna','2021-01-01','2021-09-11','true','false','false','2021-03-08 18:12:09');
/*!40000 ALTER TABLE `user_gestations` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `user_schedules`
--

DROP TABLE IF EXISTS `user_schedules`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `user_schedules` (
  `id` int NOT NULL AUTO_INCREMENT,
  `userid` varchar(50) NOT NULL,
  `schedulename` varchar(50) DEFAULT NULL,
  `hospitalid` varchar(15) NOT NULL,
  `doctorid` varchar(50) NOT NULL,
  `scheduledate` varchar(10) DEFAULT NULL,
  `scheduletime` varchar(10) DEFAULT NULL,
  `motherweight` double DEFAULT NULL,
  `bloodpressure` varchar(25) DEFAULT NULL,
  `heartrate` varchar(25) DEFAULT NULL,
  `notes` text,
  `status` varchar(25) DEFAULT NULL,
  `createdt` timestamp NULL DEFAULT CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`),
  KEY `userschedules_FK` (`userid`),
  KEY `userschedules_FK_1` (`hospitalid`),
  KEY `userschedules_FK_2` (`doctorid`),
  CONSTRAINT `user_schedules_FK` FOREIGN KEY (`hospitalid`) REFERENCES `hospitals` (`id`),
  CONSTRAINT `userschedules_FK` FOREIGN KEY (`userid`) REFERENCES `users` (`userid`),
  CONSTRAINT `userschedules_FK_2` FOREIGN KEY (`doctorid`) REFERENCES `doctors` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=14 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `user_schedules`
--

LOCK TABLES `user_schedules` WRITE;
/*!40000 ALTER TABLE `user_schedules` DISABLE KEYS */;
INSERT INTO `user_schedules` VALUES (11,'8a03edf2-8ba4-59c8-abb6-b49037b69ce7','periksa 1','2071015','b1e75db2-725d-5423-93c9-ecda2bc3e33c','07-02-2021','13:00:00',75,'120/80','0','','in progress','2021-02-10 17:55:12'),(13,'2a6b83f8-e75c-5106-925d-a00b7dab8d2a','periksa ke-1','2071015','b1e75db2-725d-5423-93c9-ecda2bc3e33c','07-02-2021','13:00:00',90,'120/80','110','Mual mual','in progress','2021-03-09 14:20:59');
/*!40000 ALTER TABLE `user_schedules` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `users`
--

DROP TABLE IF EXISTS `users`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `users` (
  `userid` varchar(50) NOT NULL,
  `givenname` varchar(20) NOT NULL,
  `fullname` varchar(100) NOT NULL,
  `email` varchar(50) NOT NULL,
  `phone` varchar(50) NOT NULL,
  `gender` char(1) DEFAULT NULL,
  `relation` varchar(20) DEFAULT NULL,
  `age` tinyint DEFAULT NULL,
  `avatar` varchar(250) DEFAULT NULL,
  `managers` varchar(50) NOT NULL,
  `isactive` varchar(5) CHARACTER SET latin1 COLLATE latin1_swedish_ci DEFAULT 'false',
  `createdt` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  PRIMARY KEY (`userid`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `users`
--

LOCK TABLES `users` WRITE;
/*!40000 ALTER TABLE `users` DISABLE KEYS */;
INSERT INTO `users` VALUES ('2a6b83f8-e75c-5106-925d-a00b7dab8d2a','Susan W','Susan Wu','susan@mail.com','987654321','w','bunda',25,NULL,'','1','2021-03-06 18:54:07'),('79ded0b6-5228-5f4a-ad16-00e1e2125f82','lilytan','Lily Tan','lilyt@mail.com','085747079410',NULL,'bunda',25,NULL,'','true','2021-02-10 17:26:50'),('8a03edf2-8ba4-59c8-abb6-b49037b69ce7','Anggie','Anggie Sukma','anggie@mail.com','081414079410',NULL,NULL,NULL,NULL,'','true','2021-02-10 17:43:35');
/*!40000 ALTER TABLE `users` ENABLE KEYS */;
UNLOCK TABLES;
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

-- Dump completed on 2021-03-10  2:41:19
