/**
 * DO NOT ALTER OR REMOVE COPYRIGHT NOTICES OR THIS HEADER.
 * <p>
 * Copyright (c) 2020 IO-Teknologi Indonesia, and individual contributors
 * as indicated by the @author tags. All Rights Reserved
 * <p>
 * The contents of this file are subject to the terms of the
 * Common Development and Distribution License (the License).
 * <p>
 * Everyone is permitted to copy and distribute verbatim copies
 * of this license document, but changing it is not allowed.
 */
package id.io.portal.filter;

import id.io.portal.util.constant.ConstantHelper;
import id.io.portal.util.rest.model.ServiceResponse;

import javax.annotation.security.PermitAll;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;
import javax.ws.rs.container.ContainerRequestContext;
import javax.ws.rs.container.ContainerRequestFilter;
import javax.ws.rs.container.ResourceInfo;
import javax.ws.rs.core.Context;
import javax.ws.rs.core.Response;
import javax.ws.rs.core.Response.ResponseBuilder;
import javax.ws.rs.ext.Provider;
import java.io.IOException;
import java.lang.reflect.Method;

@Provider
public class ServiceFilter implements ContainerRequestFilter {

    private static final ResponseBuilder ACCESS_DENIED =
            Response.status(Response.Status.UNAUTHORIZED).entity(new ServiceResponse(Response.Status.UNAUTHORIZED));
    @Context
    private ResourceInfo resourceInfo;
    @Context
    private HttpServletRequest httpServletRequest;

    @Override
    public void filter(ContainerRequestContext requestContext) throws IOException {

        if (hasSecurityAnnotations(resourceInfo) && !hasSession(httpServletRequest)) {
            requestContext.abortWith(ACCESS_DENIED.build());
        }
    }

    private boolean hasSession(HttpServletRequest request) {
        final HttpSession session = request.getSession(false);
        return null != session && null != session.getAttribute(ConstantHelper.SESSION_KEY);
    }


    private boolean hasSecurityAnnotations(ResourceInfo resourceInfo) {
        Method method = resourceInfo.getResourceMethod();
        Class<?> clazz = resourceInfo.getResourceClass();
        return method.isAnnotationPresent(PermitAll.class) || clazz.isAnnotationPresent(PermitAll.class);

    }

}
