/**
 * DO NOT ALTER OR REMOVE COPYRIGHT NOTICES OR THIS HEADER.
 * <p>
 * Copyright (c) 2020 IO-Teknologi Indonesia, and individual contributors
 * as indicated by the @author tags. All Rights Reserved
 * <p>
 * The contents of this file are subject to the terms of the
 * Common Development and Distribution License (the License).
 * <p>
 * Everyone is permitted to copy and distribute verbatim copies
 * of this license document, but changing it is not allowed.
 */
package id.io.portal.resource;

import id.io.portal.service.ConfigurationService;

import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.Consumes;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;

/**
 * @author bmp
 */
@Path("/config")
@Produces(MediaType.APPLICATION_JSON)
@Consumes(MediaType.APPLICATION_JSON)
public class ConfigResource extends BaseResource {

    private ConfigurationService configService;

    public ConfigResource() {
        log = getLogger(ConfigResource.this.getClass());
        configService = new ConfigurationService();

    }

    @GET
    public Response getConfiguration() {
        final String methodName = "getConfiguration";
        log.debug(methodName, "GET /api/config");
        start(methodName);
        completed(methodName);
        return Response.ok(configService.getListConfiguration().toString()).build();
    }

    @GET
    @Path("/mobile")
    public Response getMobileConfiguration() {
        final String methodName = "getMobileConfiguration";
        log.debug(methodName, "GET /api/config/mobile");
        start(methodName);
        Response response = buildSuccessResponse();
        response = buildSuccessResponse(configService.getMobileConfiguration());
        completed(methodName);

        return response;
    }

}
