package id.io.portal.resource.validator;

import id.io.portal.model.LocationRequest;

public class LocationValidator extends BaseValidator {

    public LocationValidator() {
    }

    public boolean validateUpdate(LocationRequest request) {
        return notNull(request) && validate(request.getId()) && validate(request.getPostalCode())
                && validate(request.getVillage()) && validate(request.getDistrict()) && validate(request.getRegency())
                && validate(request.getProvince());
    }
    public boolean validateAdd(LocationRequest request) {
        return notNull(request)  && validate(request.getPostalCode())
                && validate(request.getVillage()) && validate(request.getDistrict()) && validate(request.getRegency())
                && validate(request.getProvince());
    }
}
