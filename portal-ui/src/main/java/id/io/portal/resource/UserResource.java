/**
 * DO NOT ALTER OR REMOVE COPYRIGHT NOTICES OR THIS HEADER.
 * <p>
 * Copyright (c) 2020 IO-Teknologi Indonesia, and individual contributors
 * as indicated by the @author tags. All Rights Reserved
 * <p>
 * The contents of this file are subject to the terms of the
 * Common Development and Distribution License (the License).
 * <p>
 * Everyone is permitted to copy and distribute verbatim copies
 * of this license document, but changing it is not allowed.
 */
package id.io.portal.resource;

import id.io.portal.model.User;
import id.io.portal.model.UserLocationResponse;
import id.io.portal.model.UserRequest;
import id.io.portal.model.UserResponse;
import id.io.portal.resource.util.constant.MessageConstant;
import id.io.portal.resource.validator.UserValidator;
import id.io.portal.service.*;

import javax.ws.rs.*;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import java.util.List;

@Path("users")
@Produces(MediaType.APPLICATION_JSON)
public class UserResource extends BaseResource {

    private UserService userService;
    private UserSchedulesService schedulesService;
    private UserLocationService locationService;
    private MobileService mobileService;
    private UsersGestationService gestationService;

    //validator
    private UserValidator validator;

    public UserResource() {
        log = getLogger(this.getClass());
        userService = new UserService();
        schedulesService = new UserSchedulesService();
        locationService = new UserLocationService();
        mobileService = new MobileService();
        gestationService = new UsersGestationService();

        validator = new UserValidator();
    }

    @GET
    public Response getUsers() {
        final String methodName = "getUsers";
        log.debug(methodName, "GET api/users");
        start(methodName);

        Response response = buildBadRequestResponse();
        List<UserResponse> userList = userService.getUsers();
        response = buildSuccessResponse(userList);
        completed(methodName);
        return response;
    }

    @GET
    @Path("/{userid}")
    public Response getUser(@PathParam("userid") String userid) {
        final String methodName = "getUser";
        log.debug(methodName, "GET api/users/" + userid);
        Response response = buildBadRequestResponse();
        start(methodName);
        UserResponse userResponse = userService.getUser(userid);
        if (!userResponse.getUserId().isEmpty()) {
            response = buildSuccessResponse(userResponse);
        }
        completed(methodName);
        return response;
    }

    @PUT
    @Consumes(MediaType.APPLICATION_JSON)
    public Response updateUserInfo(UserRequest request) {
        final String methodName = "updateUserInfo";
        log.debug(methodName, "PUT api/users");
        start(methodName);
        logRequest(methodName, request);
        Response response = buildBadRequestResponse();
        if (validator.validate(request)) {
            User user = new User();
            user.setUserid(request.getUserId());
            user.setCardid(request.getCardId());
            user.setGivenname(request.getGivenName());
            user.setFullname(request.getFullName());
            user.setEmail(request.getEmail());
            user.setPhone(request.getPhone());
            user.setRelation(request.getRelation());
            user.setAge(request.getAge());
            if (userService.updateUser(user)) {
                response = buildSuccessResponse();
            } else {
                response = buildNotAcceptableResponse(MessageConstant.USER_UPDATE_FAILED);
            }
        }
        completed(methodName);
        return response;
    }

    @DELETE
    @Path("/{userid}")
    public Response deleteUser(@PathParam("userid") String userid) {
        final String methodName = "deleteUser";
        log.debug(methodName, "DELETE api/users/" + userid);
        Response response = buildBadRequestResponse();
        start(methodName);
        if (userService.validateUserId(userid)) {
            if (locationService.removeUser(userid) && schedulesService.removeUser(userid)
                    && gestationService.removeUser(userid) && mobileService.removeUser(userid)
                    && userService.removeUser(userid)) {
                response = buildSuccessResponse();
            } else {
                response = buildNotAcceptableResponse(MessageConstant.USER_DELETE_FAILED);
            }
        } else {
            response = buildNotFoundResponse(MessageConstant.USER_NOT_FOUND);
        }
        completed(methodName);
        return response;
    }
}
