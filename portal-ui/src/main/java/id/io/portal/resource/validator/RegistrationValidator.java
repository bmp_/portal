/**
 * DO NOT ALTER OR REMOVE COPYRIGHT NOTICES OR THIS HEADER.
 * <p>
 * Copyright (c) 2020 IO-Teknologi Indonesia, and individual contributors
 * as indicated by the @author tags. All Rights Reserved
 * <p>
 * The contents of this file are subject to the terms of the
 * Common Development and Distribution License (the License).
 * <p>
 * Everyone is permitted to copy and distribute verbatim copies
 * of this license document, but changing it is not allowed.
 */
package id.io.portal.resource.validator;

import id.io.portal.model.RegistrationRequest;

public class RegistrationValidator extends BaseValidator {

    public RegistrationValidator() {
    }

    public boolean validate(RegistrationRequest request) {
        return notNull(request) && validate(request.getEmail()) && validate(request.getPassword())
                && validate(request.getFullName()) && validate(request.getGivenName()) && validate(request.getPhone());
    }
}
