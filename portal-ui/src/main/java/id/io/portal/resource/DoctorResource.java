/**
 * DO NOT ALTER OR REMOVE COPYRIGHT NOTICES OR THIS HEADER.
 * <p>
 * Copyright (c) 2020 IO-Teknologi Indonesia, and individual contributors
 * as indicated by the @author tags. All Rights Reserved
 * <p>
 * The contents of this file are subject to the terms of the
 * Common Development and Distribution License (the License).
 * <p>
 * Everyone is permitted to copy and distribute verbatim copies
 * of this license document, but changing it is not allowed.
 */
package id.io.portal.resource;

import id.io.portal.model.DoctorResponse;
import id.io.portal.service.DoctorService;

import javax.ws.rs.*;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import java.util.ArrayList;
import java.util.List;

@Path("doctors")
@Produces(MediaType.APPLICATION_JSON)
@Consumes(MediaType.APPLICATION_JSON)
public class DoctorResource extends BaseResource {

    private DoctorService doctorService;

    public DoctorResource() {
        log = getLogger(this.getClass());
        doctorService = new DoctorService();
    }

    @GET
    @Path("/queryFilter")
    public Response getDoctorInHospital(@QueryParam("hospital-id") String hospitalId) {
        final String methodName = "getDoctorInHospital";
        log.debug(methodName, "GET /queryFilter?hospital-id=" + hospitalId);
        start(methodName);
        Response response = buildBadRequestResponse();
        List<String> result = new ArrayList<>();
        List<DoctorResponse> responseList = doctorService.getDoctorsHospital(hospitalId);
        for (DoctorResponse x : responseList) {
            result.add(x.getName());
        }
        response = buildSuccessResponse(result);
        completed(methodName);
        return response;
    }

    @GET
    @Path("search")
    public Response getDoctorByName(@QueryParam("name") String name) {
        final String methodName = "getDoctorInHospital";
        log.debug(methodName, "GET /search/" + name);
        start(methodName);
        Response response = buildBadRequestResponse();
        DoctorResponse result = doctorService.getDoctorByName(name);
        response = buildSuccessResponse(result);
        completed(methodName);
        return response;
    }

}
