/**
 * DO NOT ALTER OR REMOVE COPYRIGHT NOTICES OR THIS HEADER.
 * <p>
 * Copyright (c) 2020 IO-Teknologi Indonesia, and individual contributors
 * as indicated by the @author tags. All Rights Reserved
 * <p>
 * The contents of this file are subject to the terms of the
 * Common Development and Distribution License (the License).
 * <p>
 * Everyone is permitted to copy and distribute verbatim copies
 * of this license document, but changing it is not allowed.
 */
package id.io.portal.resource;

import id.io.portal.util.constant.ConstantHelper;
import org.apache.http.HttpStatus;
import org.json.JSONObject;

import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;

@Path("ping")
public class PingTestResource extends BaseResource {

    public PingTestResource() {
        log = getLogger(PingTestResource.this.getClass());
    }

    @GET
    @Produces(MediaType.APPLICATION_JSON)
    public Response pingTest() {
        final String methodName = "pingTest";
        log.debug(methodName, "GET /ping");
        start(methodName);

        JSONObject objResponse = new JSONObject();
        objResponse.put(ConstantHelper.HTTP_CODE, HttpStatus.SC_OK);
        objResponse.put(ConstantHelper.HTTP_MESSAGE, "Server Test Successfull!");

        completed(methodName);
        return Response.status((!objResponse.has(ConstantHelper.HTTP_CODE))
                ? HttpStatus.SC_OK : objResponse.getInt(ConstantHelper.HTTP_CODE)).entity(objResponse.toString()).build();


    }


}
