/**
 * DO NOT ALTER OR REMOVE COPYRIGHT NOTICES OR THIS HEADER.
 * <p>
 * Copyright (c) 2020 IO-Teknologi Indonesia, and individual contributors
 * as indicated by the @author tags. All Rights Reserved
 * <p>
 * The contents of this file are subject to the terms of the
 * Common Development and Distribution License (the License).
 * <p>
 * Everyone is permitted to copy and distribute verbatim copies
 * of this license document, but changing it is not allowed.
 */
package id.io.portal.resource;

import id.io.portal.helper.JsonHelper;
import id.io.portal.manager.PropertyManager;
import id.io.portal.util.log.AppLogger;
import id.io.portal.util.rest.model.ServiceResponse;

import javax.servlet.http.HttpServletRequest;
import javax.ws.rs.core.Context;
import javax.ws.rs.core.Response;
import javax.ws.rs.core.Response.Status;
import java.util.concurrent.Executor;
import java.util.concurrent.Executors;

public class BaseResource {

    @Context
    protected HttpServletRequest request;

    protected AppLogger log;

    public BaseResource() {
    }

    protected AppLogger getLogger(Class<?> clazz) {
        return new AppLogger(clazz);
    }

    // Response Builder
    protected Response buildResponse(Status status) {
        return Response.status(status).entity(new ServiceResponse(status)).build();
    }

    protected Response buildResponse(Status status, String description) {
        return buildResponse(status, new ServiceResponse(status, description));
    }

    protected Response buildResponse(Status status, Object entity) {
        return Response.status(status).entity(entity).build();
    }

    // SUCCESS  response
    protected Response buildSuccessResponse() {
        return buildResponse(Status.OK);
    }

    protected Response buildSuccessResponse(String message) {
        return buildResponse(Status.OK, message);
    }

    protected Response buildSuccessResponse(Object obj) {
        return buildResponse(Status.OK, obj);
    }

    // BAD_REQUEST  response
    protected Response buildBadRequestResponse() {
        return buildResponse(Status.BAD_REQUEST);
    }

    protected Response buildBadRequestResponse(String message) {
        return buildResponse(Status.BAD_REQUEST, message);
    }

    // UNAUTHORIZED  response
    protected Response buildUnauthorizedResponse() {
        return buildResponse(Status.UNAUTHORIZED);
    }

    // ERROR  response
    protected Response buildNotFoundResponse(String message) {
        return buildResponse(Status.NOT_FOUND, message);
    }

    protected Response buildFoundResponse(String message) {
        return buildResponse(Status.FOUND, message);
    }

    protected Response buildNotAcceptableResponse(String message) {
        return buildResponse(Status.NOT_ACCEPTABLE, message);
    }

    // No Content  response
    protected Response buildNoContentResponse(String message) {
        return buildResponse(Status.NO_CONTENT, message);
    }


    protected void start(String methodName) {
        log.info(methodName, "start");
    }

    protected void completed(String methodName) {
        log.info(methodName, "completed");
    }

    protected void logRequest(String methodName, Object request)
    {
        log.debug(methodName, "Request: " + toJson(request));
    }
    protected void logResponse(String methodName, Object response)
    {
        log.debug(methodName, "Response: " + toJson(response));
    }

    protected String toJson(Object obj)
    {
        return JsonHelper.toJson(obj);
    }


    public String generateOTPContent(String template, String otp, String validity) {
        String result = template.replaceAll("&otpToken&", otp).replaceAll("&otpValidity&", validity);
        return result;
    }
}
