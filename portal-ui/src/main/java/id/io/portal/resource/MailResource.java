/**
 * DO NOT ALTER OR REMOVE COPYRIGHT NOTICES OR THIS HEADER.
 * <p>
 * Copyright (c) 2020 IO-Teknologi Indonesia, and individual contributors
 * as indicated by the @author tags. All Rights Reserved
 * <p>
 * The contents of this file are subject to the terms of the
 * Common Development and Distribution License (the License).
 * <p>
 * Everyone is permitted to copy and distribute verbatim copies
 * of this license document, but changing it is not allowed.
 */
package id.io.portal.resource;

import id.io.portal.helper.ConvertionHelper;
import id.io.portal.helper.MailHelper;
import id.io.portal.helper.OTPGeneratorHelper;
import id.io.portal.service.ConfigurationService;
import id.io.portal.util.constant.ConstantHelper;
import id.io.portal.util.database.ConfigurationDatabaseHelper;
import id.io.portal.util.database.OtpLogDatabaseHelper;
import org.apache.http.HttpStatus;
import org.json.JSONObject;

import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import java.util.HashMap;

/**
 * @author bmp
 */
@Path("send")
public class MailResource extends BaseResource {

    private ConfigurationDatabaseHelper configDatabaseHelper;
    private OtpLogDatabaseHelper otpLogDatabaseHelper;
    private ConfigurationService configService;

    public MailResource() {
        log = getLogger(MailResource.this.getClass());
        configDatabaseHelper = new ConfigurationDatabaseHelper();
        otpLogDatabaseHelper = new OtpLogDatabaseHelper();

        configService = new ConfigurationService();
    }

    @POST
    @Path("/mail")
    @Produces(MediaType.APPLICATION_JSON)
    public Response dumnySendMail() {
        final String methodName = "dumnySendMail";
        log.debug(methodName, "POST /mail");
        start(methodName);

        HashMap<String, String> mailConfig = configService.getMailConfiguration();
        HashMap<String, String> otpConfig = configService.getOtpConfiguration();

        String recipient = "bachtiar.madya.p@gmail.com";
        String token = OTPGeneratorHelper.generateOtp(Integer.parseInt(otpConfig.get(ConstantHelper.OTP_LENGTH)));
        String subject = mailConfig.get(ConstantHelper.OTP_MAIL_SUBJECT);
        String content = mailConfig.get(ConstantHelper.OTP_MAIL_CONTENT);
        content = content.replace("{otpToken}", token)
                .replace("{otpValidity}", ConvertionHelper.convertSecondToMinute(otpConfig.get(ConstantHelper.OTP_VALIDITY)));

        otpLogDatabaseHelper.saveOtpLog(recipient, token, subject);
        MailHelper mailHelper = new MailHelper();
        mailHelper.sendMail(recipient, content, subject);

        completed(methodName);
        return Response.ok().build();
    }

    @POST
    @Path("/otp/check/{token}")
    @Produces(MediaType.APPLICATION_JSON)
    public Response dumnyValidateOtp(@PathParam("token") String token) {
        final String methodName = "dumnyValidateOtp";
        log.debug(methodName, "POST /otp/check/" + token);
        start(methodName);


        JSONObject response = new JSONObject();

        String recipient = "bachtiar.madya.p@gmail.com";
        int validity = Integer.parseInt(ConvertionHelper.convertSecondToMinute(configDatabaseHelper.getConfigurationValue(ConstantHelper.OTP_VALIDITY)));

        if (otpLogDatabaseHelper.checkOtp(token)) {
            if (otpLogDatabaseHelper.validateOtp(recipient, token, validity)) {

                response.put(ConstantHelper.HTTP_CODE, HttpStatus.SC_OK);
                response.put(ConstantHelper.HTTP_MESSAGE, "OTP valid and active");

            } else {

                response.put(ConstantHelper.HTTP_CODE, HttpStatus.SC_FORBIDDEN);
                response.put(ConstantHelper.HTTP_MESSAGE, "Expired");

            }
        } else {
            response.put(ConstantHelper.HTTP_CODE, HttpStatus.SC_UNAUTHORIZED);
            response.put(ConstantHelper.HTTP_MESSAGE, "Invalid token");
        }

        completed(methodName);
        return Response.status((!response.has(ConstantHelper.HTTP_CODE))
                ? HttpStatus.SC_OK : response.getInt(ConstantHelper.HTTP_CODE)).entity(response.toString()).build();
    }

}
