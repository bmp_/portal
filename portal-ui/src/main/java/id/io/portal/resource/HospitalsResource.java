/**
 * DO NOT ALTER OR REMOVE COPYRIGHT NOTICES OR THIS HEADER.
 * <p>
 * Copyright (c) 2020 IO-Teknologi Indonesia, and individual contributors
 * as indicated by the @author tags. All Rights Reserved
 * <p>
 * The contents of this file are subject to the terms of the
 * Common Development and Distribution License (the License).
 * <p>
 * Everyone is permitted to copy and distribute verbatim copies
 * of this license document, but changing it is not allowed.
 */
package id.io.portal.resource;

import id.io.portal.model.HospitalResponse;
import id.io.portal.service.HospitalService;
import id.io.portal.util.constant.ConstantHelper;
import org.apache.http.HttpStatus;
import org.json.JSONException;
import org.json.JSONObject;

import javax.ws.rs.*;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import java.util.ArrayList;
import java.util.List;

@Path("hospitals")
@Produces(MediaType.APPLICATION_JSON)
@Consumes(MediaType.APPLICATION_JSON)
public class HospitalsResource extends BaseResource {

    private HospitalService hospitalService;

    public HospitalsResource() {
        log = getLogger(HospitalsResource.this.getClass());
        hospitalService = new HospitalService();
    }

    @GET
    public Response getHospitals() {
        final String methodName = "getHospitals";
        log.debug(methodName, "GET /hospitals");
        start(methodName);

        JSONObject response = new JSONObject();
        try {

            response = hospitalService.getHospitals();
            completed(methodName);
            return Response.ok().entity(response.get(ConstantHelper.HTTP_RESPONSE).toString()).build();
        } catch (JSONException ex) {
            JSONObject errResponse = new JSONObject();
            errResponse.put(ConstantHelper.HTTP_CODE, HttpStatus.SC_BAD_REQUEST);
            errResponse.put(ConstantHelper.HTTP_REASON, "error_get_hospitals");
            errResponse.put(ConstantHelper.HTTP_MESSAGE, "Error get hospitals");

            response.put(ConstantHelper.HTTP_RESPONSE, errResponse);
            log.error(methodName, response.getString(ConstantHelper.HTTP_REASON));
        }
        return Response.status(HttpStatus.SC_BAD_REQUEST).entity(response.getJSONObject(ConstantHelper.HTTP_RESPONSE).toString()).build();

    }

    @GET
    @Path("/queryFilter")
    public Response getHospitalByDistrict(@QueryParam("in") String district) {
        final String methodName = "getHospitalByDistrict";
        log.debug(methodName, "GET api/hospitals/queryFilter?in=" + district);
        start(methodName);
        Response response = buildBadRequestResponse();
        List<String> result = new ArrayList<>();
        List<HospitalResponse> hospitalList = hospitalService.getHospitalByDistrict(district);
        for (HospitalResponse x : hospitalList) {
            result.add(x.getName());
        }
        response = buildSuccessResponse(result);
        completed(methodName);
        return response;
    }

    @GET
    @Path("search")
    public Response getHospitalByName(@QueryParam("name") String name) {
        final String methodName = "getHospitalByCity";
        log.debug(methodName, "GET api/hospitals/search/" + name);
        start(methodName);
        Response response = buildBadRequestResponse();
        HospitalResponse result = hospitalService.getHospitalByName(name);
        response = buildSuccessResponse(result);
        completed(methodName);
        return response;
    }
/*

    @GET
    @Path("/{id}")
    public Response getHospitals(@PathParam("id") String id) {

        final String methodName = "getHospitals";
        log.debug(methodName, "GET api/hospitals/" + id);
        start(methodName);

        JSONObject response = new JSONObject();
        try {

            response = hospitalService.getHospitals(id);
            completed(methodName);
            return Response.ok().entity(response.get(ConstantHelper.HTTP_RESPONSE).toString()).build();
        } catch (JSONException ex) {
            JSONObject errResponse = new JSONObject();
            errResponse.put(ConstantHelper.HTTP_CODE, HttpStatus.SC_BAD_REQUEST);
            errResponse.put(ConstantHelper.HTTP_REASON, "error_get_hospitals");
            errResponse.put(ConstantHelper.HTTP_MESSAGE, "Error get hospitals");

            response.put(ConstantHelper.HTTP_RESPONSE, errResponse);
            log.error(methodName, response.getString(ConstantHelper.HTTP_REASON));
        }
        return Response.status(HttpStatus.SC_BAD_REQUEST).entity(response.getJSONObject(ConstantHelper.HTTP_RESPONSE).toString()).build();

    }
*/

}
