/**
 * DO NOT ALTER OR REMOVE COPYRIGHT NOTICES OR THIS HEADER.
 * <p>
 * Copyright (c) 2020 IO-Teknologi Indonesia, and individual contributors
 * as indicated by the @author tags. All Rights Reserved
 * <p>
 * The contents of this file are subject to the terms of the
 * Common Development and Distribution License (the License).
 * <p>
 * Everyone is permitted to copy and distribute verbatim copies
 * of this license document, but changing it is not allowed.
 */
package id.io.portal.resource;

import id.io.portal.model.Principal;
import id.io.portal.service.SystemAdministratorService;
import id.io.portal.util.constant.ConstantHelper;
import org.apache.http.HttpStatus;
import org.json.JSONObject;

import javax.annotation.security.PermitAll;
import javax.servlet.http.HttpSession;
import javax.ws.rs.*;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;

@Path("admin")
@Produces(MediaType.APPLICATION_JSON)
public class SystemAdministratorResource extends BaseResource {

    private SystemAdministratorService systemAdministratorService;

    public SystemAdministratorResource() {
        log = getLogger(SystemAdministratorResource.this.getClass());
        systemAdministratorService = new SystemAdministratorService();

    }

    @POST
    @Path("/login")
    @Consumes(MediaType.APPLICATION_JSON)
    public Response login(String jsonRequest) {
        final String methodName = "login";
        log.debug(methodName,"GET api/admin/login");
        start(methodName);

        JSONObject json = new JSONObject(jsonRequest);

        JSONObject response = systemAdministratorService.authenticate(json);
        if (response.getInt(ConstantHelper.HTTP_CODE) == HttpStatus.SC_OK) {
            HttpSession session = request.getSession(true);

            Principal principal = new Principal(json.getString("username"));
            session.setAttribute(ConstantHelper.SESSION_KEY, principal);

            response.remove(ConstantHelper.HTTP_CODE);
            completed(methodName);
            return Response.status(HttpStatus.SC_OK).entity(response.get(ConstantHelper.HTTP_RESPONSE)).build();
        } else {
            log.error(methodName, response.getString(ConstantHelper.HTTP_REASON));
            return Response.status((!response.has(ConstantHelper.HTTP_CODE)) ? HttpStatus.SC_BAD_GATEWAY : response.getInt(ConstantHelper.HTTP_CODE)).entity(response.toString()).build();
        }
    }

    @POST
    @Path("/logout")
    @PermitAll
    public Response logout() {
        clearSession();
        return buildSuccessResponse();
    }

    @GET
    @Path("/session")
    @PermitAll
    public Response session() {
        return buildSuccessResponse();
    }

    private void clearSession() {
        HttpSession session = request.getSession(false);
        if (session != null) {
            session.invalidate();
        }
    }

}
