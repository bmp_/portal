/**
 * DO NOT ALTER OR REMOVE COPYRIGHT NOTICES OR THIS HEADER.
 * <p>
 * Copyright (c) 2020 IO-Teknologi Indonesia, and individual contributors
 * as indicated by the @author tags. All Rights Reserved
 * <p>
 * The contents of this file are subject to the terms of the
 * Common Development and Distribution License (the License).
 * <p>
 * Everyone is permitted to copy and distribute verbatim copies
 * of this license document, but changing it is not allowed.
 */
package id.io.portal.resource;

import id.io.portal.helper.ConvertionHelper;
import id.io.portal.helper.OTPGeneratorHelper;
import id.io.portal.helper.UUIDGeneratorHelper;
import id.io.portal.manager.EncryptionManager;
import id.io.portal.model.*;
import id.io.portal.resource.util.constant.MessageConstant;
import id.io.portal.resource.validator.AuthenticationValidator;
import id.io.portal.resource.validator.RegistrationValidator;
import id.io.portal.service.*;
import id.io.portal.util.constant.ConstantHelper;

import javax.ws.rs.Consumes;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import java.util.HashMap;
import java.util.UUID;

@Path("mobile")
@Produces(MediaType.APPLICATION_JSON)
public class MobileResource extends BaseResource {

    private MobileService authenticationService;
    private UserService userService;
    private UserLocationService locationService;
    private OtpService otpService;
    private ConfigurationService configurationService;

    //validator
    private AuthenticationValidator authenticationValidatorvalidator;
    private RegistrationValidator registrationValidator;

    public MobileResource() {
        log = getLogger(this.getClass());
        authenticationService = new MobileService();
        userService = new UserService();
        authenticationValidatorvalidator = new AuthenticationValidator();
        registrationValidator = new RegistrationValidator();
        locationService = new UserLocationService();
        otpService = new OtpService();
        configurationService = new ConfigurationService();
    }

    @POST
    @Path("/login")
    @Consumes(MediaType.APPLICATION_JSON)
    public Response login(AuthenticationRequest request) {
        final String methodName = "login";
        log.debug(methodName, "POST /mobile/login");

        start(methodName);
        logRequest(methodName, request);
        Response response = buildBadRequestResponse();

        if (authenticationValidatorvalidator.validate(request)) {
            if (userService.validateEmail(request.getEmail())) {

                UserResponse user = userService.getUser(userService.getUserId(request.getEmail()));
                Authentication authentication = new Authentication(user.getUserId(), request.getPassword());
                if (authenticationService.authenticate(authentication)) {
                    UserResponse userResponse = new UserResponse();
                    userResponse.setUserId(user.getUserId());
                    userResponse.setGivenName(user.getGivenName());
                    userResponse.setFullName(user.getFullName());
                    userResponse.setEmail(user.getEmail());
                    userResponse.setActive(user.isActive());
                    userResponse.setCreateDt(user.getCreateDt());
                    response = buildSuccessResponse(userResponse);
                } else {
                    response = buildUnauthorizedResponse();
                }
            } else {
                response = buildNotFoundResponse(MessageConstant.USER_NOT_FOUND);
            }
        }
        completed(methodName);
        return response;
    }

    @POST
    @Path("/register")
    @Consumes(MediaType.APPLICATION_JSON)
    public Response register(RegistrationRequest request) {
        final String methodName = "register";
        log.debug(methodName, "POST /mobile/register");
        start(methodName);
        logRequest(methodName, request);
        Response response = buildBadRequestResponse();

        if (registrationValidator.validate(request)) {
            if (!userService.validateEmail(request.getEmail())) {

                UUID userId = UUIDGeneratorHelper.generateUUID(request.getEmail());
                User user = new User();
                user.setUserid(userId.toString());
                user.setGivenname(request.getGivenName());
                user.setFullname(request.getFullName());
                user.setEmail(request.getEmail());
                user.setPhone(request.getPhone());
                user.setManagers("");

                Authentication authentication = new Authentication(userId.toString(),
                        EncryptionManager.getInstance().encrypt(request.getPassword()));

                UserLocation userLocation = new UserLocation();
                userLocation.setUserid(userId.toString());

                if (userService.registerUser(user) && authenticationService.register(authentication)
                        && locationService.registerUser(userLocation)) {
                    HashMap<String, String> otpConfiguration = configurationService.getOtpConfiguration();
                    if (Boolean.parseBoolean(otpConfiguration.get(ConstantHelper.OTP_ENABLED))) {
                        String subject = otpConfiguration.get(ConstantHelper.OTP_MAIL_SUBJECT);
                        String template = otpConfiguration.get(ConstantHelper.OTP_MAIL_CONTENT);
                        String otp = OTPGeneratorHelper.generateOtp(
                                Integer.parseInt(otpConfiguration.get(ConstantHelper.OTP_LENGTH)));
                        String validity = ConvertionHelper.convertSecondToMinute(
                                otpConfiguration.get(ConstantHelper.OTP_VALIDITY));
                        String content = generateOTPContent(template, otp, validity);

                        otpService.sendOtp(user, subject, content, otp, validity, methodName);
                    }
                    UserResponse userResponse = userService.getUser(userService.getUserId(request.getEmail()));
                    UserResponse userResp = new UserResponse();
                    userResp.setUserId(userResponse.getUserId());
                    userResp.setGivenName(userResponse.getGivenName());
                    userResp.setFullName(userResponse.getFullName());
                    userResp.setEmail(userResponse.getEmail());
                    userResp.setActive(userResponse.isActive());
                    userResp.setCreateDt(userResponse.getCreateDt());
                    response = buildSuccessResponse(userResp);

                } else {
                    response = buildNotAcceptableResponse(MessageConstant.USER_CREATE_FAILED);
                }
            } else {
                response = buildFoundResponse(MessageConstant.EMAIL_ALREADY_USED);
            }
        }
        completed(methodName);
        return response;
    }

}
