/**
 * DO NOT ALTER OR REMOVE COPYRIGHT NOTICES OR THIS HEADER.
 * <p>
 * Copyright (c) 2020 IO-Teknologi Indonesia, and individual contributors
 * as indicated by the @author tags. All Rights Reserved
 * <p>
 * The contents of this file are subject to the terms of the
 * Common Development and Distribution License (the License).
 * <p>
 * Everyone is permitted to copy and distribute verbatim copies
 * of this license document, but changing it is not allowed.
 */
package id.io.portal.resource;

import id.io.portal.model.UserLocation;
import id.io.portal.model.UserLocationRequest;
import id.io.portal.model.UserLocationResponse;
import id.io.portal.resource.util.constant.MessageConstant;
import id.io.portal.resource.validator.UserValidator;
import id.io.portal.service.UserLocationService;

import javax.ws.rs.*;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;

@Path("users/location")
@Produces(MediaType.APPLICATION_JSON)
public class UserLocationResource extends BaseResource {

    private UserLocationService locationService;
    //validator
    private UserValidator validator;

    public UserLocationResource() {
        log = getLogger(this.getClass());
        locationService = new UserLocationService();
        validator = new UserValidator();
    }

    @GET
    @Path("/{userid}")
    public Response getUserLocation(@PathParam("userid") String userid) {
        final String methodName = "getUserLocation";
        log.debug(methodName, "GET api/users/location/" + userid);
        start(methodName);
        Response response = buildBadRequestResponse();
        UserLocationResponse result = locationService.getUserLocation(userid);
        response = buildSuccessResponse(result);
        completed(methodName);
        return response;
    }

    @PUT
    @Consumes(MediaType.APPLICATION_JSON)
    public Response updateUserLocation(UserLocationRequest request) {
        final String methodName = "updateUserLocation";
        log.debug(methodName, "PUT api/users/location/");
        start(methodName);
        logRequest(methodName, request);
        Response response = buildBadRequestResponse();
        if (validator.validate(request)) {
            UserLocation user = new UserLocation();
            user.setUserid(request.getUserId());
            user.setProvince(request.getProvince());
            user.setRegency(request.getRegency());
            user.setDistrict(request.getDistrict());
            user.setPostalcode(request.getPostalCode());
            user.setVillage(request.getVillage());
            user.setAddress(request.getAddress());
            user.setLat(request.getLatitude());
            user.setLng(request.getLongitude());
            if (locationService.updateUserLocation(user)) {
                response = buildSuccessResponse();
            } else {
                response = buildNotAcceptableResponse(MessageConstant.LOCATION_UPDATE_FAILED);
            }
        }
        completed(methodName);
        return response;
    }
}
