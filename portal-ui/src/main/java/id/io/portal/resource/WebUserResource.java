package id.io.portal.resource;

import id.io.portal.model.WebUserRequest;
import id.io.portal.model.WebUserResponse;
import id.io.portal.resource.validator.WebUserValidator;
import id.io.portal.service.WebUserService;

import javax.ws.rs.*;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import java.util.List;

@Path("web-users")
@Produces(MediaType.APPLICATION_JSON)
@Consumes(MediaType.APPLICATION_JSON)
public class WebUserResource extends BaseResource {

    private WebUserService userService;

    private WebUserValidator validator;

    public WebUserResource() {
        log = getLogger(this.getClass());

        userService = new WebUserService();
        validator = new WebUserValidator();
    }

    @GET
    public Response getUsers() {
        final String methodName = "getUsers";
        log.debug(methodName, "GET api/web-users");
        start(methodName);

        Response response = buildBadRequestResponse();
        List<WebUserResponse> userList = userService.getUsers();
        response = buildSuccessResponse(userList);
        completed(methodName);
        return response;
    }

    @POST
    public Response addUser(WebUserRequest request) {
        final String methodName = "addUser";
        log.debug(methodName, "POST api/web-users");
        start(methodName);
        Response response = buildBadRequestResponse();
        if (validator.validate(request) && userService.addUser(request)) {
            response = buildSuccessResponse();
        }

        completed(methodName);
        return response;
    }


}
