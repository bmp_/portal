/**
 * DO NOT ALTER OR REMOVE COPYRIGHT NOTICES OR THIS HEADER.
 * <p>
 * Copyright (c) 2020 IO-Teknologi Indonesia, and individual contributors
 * as indicated by the @author tags. All Rights Reserved
 * <p>
 * The contents of this file are subject to the terms of the
 * Common Development and Distribution License (the License).
 * <p>
 * Everyone is permitted to copy and distribute verbatim copies
 * of this license document, but changing it is not allowed.
 */
package id.io.portal.resource.validator;

import id.io.portal.model.UserGestationRequest;
import id.io.portal.model.UserLocationRequest;
import id.io.portal.model.UserRequest;
import id.io.portal.model.UserScheduleRequest;

public class UserValidator extends BaseValidator {

    public UserValidator() {
    }

    public boolean validate(UserRequest request) {
        return notNull(request) && validate(request.getUserId()) && validate(request.getCardId())
                && validate(request.getGivenName()) && validate(request.getFullName())
                && validate(request.getEmail()) && validate(request.getPhone())
                && validate(request.getRelation()) && validate(request.getAge());
    }

    public boolean validate(UserLocationRequest request) {
        return notNull(request) && validate(request.getUserId())  && validate(request.getVillage())
                && validate(request.getPostalCode()) && validate(request.getDistrict()) && validate(request.getRegency())
                && validate(request.getProvince()) && validate(request.getAddress())
                && validate(request.getLatitude())
                && validate(request.getLongitude());
    }

    public boolean validate(UserGestationRequest request) {
        return notNull(request) && validate(request.getUserId()) && validate(request.getName())
                && validate(request.getGender()) && validate(request.getInseminationDt()) && validate(request.getHplDt())
                && notNull(request.isFirstGestation()) && notNull(request.isBeenBorn())
                && notNull(request.isMisBirth());
    }

    public boolean validate(UserScheduleRequest request) {
        return notNull(request) && validate(request.getUserId()) && validate(request.getScheduleName())
                && validate(request.getScheduleDate()) && validate(request.getScheduleTime())
                && validate(request.getHospital()) && validate(request.getDoctor());
    }
}
