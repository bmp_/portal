/**
 * DO NOT ALTER OR REMOVE COPYRIGHT NOTICES OR THIS HEADER.
 * <p>
 * Copyright (c) 2020 IO-Teknologi Indonesia, and individual contributors
 * as indicated by the @author tags. All Rights Reserved
 * <p>
 * The contents of this file are subject to the terms of the
 * Common Development and Distribution License (the License).
 * <p>
 * Everyone is permitted to copy and distribute verbatim copies
 * of this license document, but changing it is not allowed.
 */
package id.io.portal.resource;

import id.io.portal.model.UserGestationRequest;
import id.io.portal.model.UserGestationResponse;
import id.io.portal.resource.util.constant.MessageConstant;
import id.io.portal.resource.validator.UserValidator;
import id.io.portal.service.UsersGestationService;

import javax.ws.rs.*;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import java.util.List;

@Path("users/gestation")
@Produces(MediaType.APPLICATION_JSON)
public class UserGestationResource extends BaseResource {

    private UsersGestationService usersGestationService;

    //validator
    private UserValidator validator;

    public UserGestationResource() {
        log = getLogger(UserGestationResource.this.getClass());
        usersGestationService = new UsersGestationService();

        validator = new UserValidator();
    }

    @GET
    @Path("/{userid}")
    @Consumes(MediaType.APPLICATION_JSON)
    public Response getGestations(@PathParam("userid") String userid) {
        final String methodName = "getGestations";
        log.debug(methodName, "GET /api/users/gestation/" + userid);
        start(methodName);
        Response response = buildBadRequestResponse();
        if (usersGestationService.validateUser(userid)) {
            List<UserGestationResponse> responseList = usersGestationService.getGestations(userid);
            response = buildSuccessResponse(responseList);
        } else {
            response = buildNoContentResponse(MessageConstant.NO_CONTENT_FOUND);
        }
        completed(methodName);
        return response;
    }

    @GET
    @Path("/{userid}/{id}")
    @Consumes(MediaType.APPLICATION_JSON)
    public Response getGestation(@PathParam("userid") String userid, @PathParam("id") String id) {
        final String methodName = "getGestation";
        log.debug(methodName, "GET /api/users/gestation/" + userid + "/" + id);
        start(methodName);
        Response response = buildBadRequestResponse();
        if (usersGestationService.validateUser(userid)) {
            UserGestationResponse result = usersGestationService.getGestation(id, userid);
            response = buildSuccessResponse(result);
        } else {
            response = buildNoContentResponse(MessageConstant.NO_CONTENT_FOUND);
        }
        completed(methodName);
        return response;
    }

    @GET
    @Path("/track/{userid}")
    @Consumes(MediaType.APPLICATION_JSON)
    public Response getTrackingGestation(@PathParam("userid") String userid) {
        final String methodName = "getTrackingGestation";
        log.debug(methodName, "GET /api/users/gestation/track/" + userid);
        start(methodName);
        Response response = buildBadRequestResponse();
        if (usersGestationService.validateUser(userid)) {
            UserGestationResponse result = usersGestationService.getTrackingGestation(userid);
            response = buildSuccessResponse(result);
        } else {
            response = buildNoContentResponse(MessageConstant.NO_CONTENT_FOUND);
        }
        completed(methodName);
        return response;
    }

    @POST
    @Consumes(MediaType.APPLICATION_JSON)
    public Response addGestation(UserGestationRequest request) {
        final String methodName = "addGestation";
        log.debug(methodName, "POST /api/users/gestation");
        start(methodName);
        Response response = buildBadRequestResponse();
        logRequest(methodName, request);
        if (validator.validate(request)) {
            if (usersGestationService.addGestation(request)) {
                response = buildSuccessResponse();
            } else {
                response = buildNotAcceptableResponse(MessageConstant.GESTATION_ADD_FAILED);
            }
        } else {
            response = buildNotAcceptableResponse(MessageConstant.INVALID_REQUEST);
        }
        completed(methodName);
        return response;
    }

    @PUT
    @Consumes(MediaType.APPLICATION_JSON)
    public Response updateGestation(UserGestationRequest request) {
        final String methodName = "updateGestation";
        log.debug(methodName, "PUT /api/users/gestation/");
        start(methodName);
        logRequest(methodName, request);
        Response response = buildBadRequestResponse();
        if (validator.validate(request)) {
            if (usersGestationService.updateGestation(request)) {
                response = buildSuccessResponse();
            } else {
                response = buildNotAcceptableResponse(MessageConstant.GESTATION_UPDATE_FAILED);
            }
        } else {
            response = buildNotAcceptableResponse(MessageConstant.INVALID_REQUEST);
        }
        return response;
    }

    @DELETE
    @Path("/{id}")
    @Consumes(MediaType.APPLICATION_JSON)
    public Response deleteGestation(@PathParam("id") String id) {
        final String methodName = "deleteGestation";
        log.debug(methodName, "DELETE /api/users/gestation/" + id);
        start(methodName);
        Response response = buildBadRequestResponse();
        if (usersGestationService.deleteGestation(id)) {
            response = buildSuccessResponse();
        } else {
            response = buildNotAcceptableResponse(MessageConstant.GESTATION_DELETE_FAILED);
        }
        completed(methodName);
        return response;
    }
}
