package id.io.portal.resource.validator;

import id.io.portal.model.EmergencyRequest;

public class EmergencyValidator extends BaseValidator {

    public EmergencyValidator() {
    }

    public boolean validate(EmergencyRequest request) {
        return notNull(request) && validate(request.getUserId()) && validate(request.getLatitude())
                && validate(request.getLongitude());
    }
}
