/**
 * DO NOT ALTER OR REMOVE COPYRIGHT NOTICES OR THIS HEADER.
 * <p>
 * Copyright (c) 2020 IO-Teknologi Indonesia, and individual contributors
 * as indicated by the @author tags. All Rights Reserved
 * <p>
 * The contents of this file are subject to the terms of the
 * Common Development and Distribution License (the License).
 * <p>
 * Everyone is permitted to copy and distribute verbatim copies
 * of this license document, but changing it is not allowed.
 */
package id.io.portal.resource;

import id.io.portal.helper.ConvertionHelper;
import id.io.portal.helper.OTPGeneratorHelper;
import id.io.portal.model.OtpRequest;
import id.io.portal.model.User;
import id.io.portal.resource.util.constant.MessageConstant;
import id.io.portal.service.ConfigurationService;
import id.io.portal.service.OtpService;
import id.io.portal.service.UserService;
import id.io.portal.util.constant.ConstantHelper;

import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import java.util.HashMap;

/**
 * @author bmp
 */
@Path("otp")
public class OtpResource extends BaseResource {

    private UserService userService;
    private OtpService otpService;
    private ConfigurationService configurationService;

    public OtpResource() {
        log = getLogger(OtpResource.this.getClass());
        userService = new UserService();
        otpService = new OtpService();
        configurationService = new ConfigurationService();
    }

    @POST
    @Path("/send")
    @Produces(MediaType.APPLICATION_JSON)
    public Response sendOtp(OtpRequest request) {
        final String methodName = "send/resend";
        log.debug(methodName, "POST /otp/send/");
        start(methodName);
        logRequest(methodName, request);
        Response response = buildBadRequestResponse();
        User user = userService.validateUser(request.getEmail());
        HashMap<String, String> otpConfiguration = configurationService.getOtpConfiguration();
        if (Boolean.parseBoolean(otpConfiguration.get(ConstantHelper.OTP_ENABLED))) {
            String subject = otpConfiguration.get(ConstantHelper.OTP_MAIL_SUBJECT);
            String template = otpConfiguration.get(ConstantHelper.OTP_MAIL_CONTENT);
            String otp = OTPGeneratorHelper.generateOtp(Integer.parseInt(otpConfiguration.get(ConstantHelper.OTP_LENGTH)));
            String validity = ConvertionHelper.convertSecondToMinute(otpConfiguration.get(ConstantHelper.OTP_VALIDITY));
            String content = generateOTPContent(template, otp, validity);

            otpService.sendOtp(user, subject, content, otp, validity, methodName);
            response = buildSuccessResponse(MessageConstant.OTP_SEND_SUCCESSFUL);
        } else {
            response = buildNotAcceptableResponse(MessageConstant.OTP_SEND_FAILED);
        }

        completed(methodName);
        return response;
    }

    @POST
    @Path("/validate/{token}")
    @Produces(MediaType.APPLICATION_JSON)
    public Response validateOtp(@PathParam("token") String token) {
        final String methodName = "validateOtp";
        log.debug(methodName, "POST /validate/" + token);
        start(methodName);
        Response response = buildBadRequestResponse();

        if (!token.isEmpty()) {
            if (otpService.checkOtp(token)) {
                String userId = otpService.getUserId(token);
                if (otpService.validateOTP(userId, token)) {
                    response = buildSuccessResponse();
                } else {
                    response = buildNotAcceptableResponse(MessageConstant.OTP_EXPIRED);
                }
            } else {
                response = buildNotAcceptableResponse(MessageConstant.OTP_INVALID);
            }
        } else {
            response = buildNotAcceptableResponse(MessageConstant.OTP_IS_MISSING);
        }

        completed(methodName);
        return response;
    }

}
