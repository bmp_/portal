package id.io.portal.resource.validator;

import id.io.portal.model.WebUserRequest;

public class WebUserValidator extends BaseValidator {

    public WebUserValidator() {
    }

    public boolean validate(WebUserRequest request) {
        return notNull(request) && validate(request.getFullName())
                && validate(request.getEmail()) && validate(request.getPhone()) && validate(request.getRole())
                && validate(request.getPassword());
    }
}
