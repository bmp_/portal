/**
 * DO NOT ALTER OR REMOVE COPYRIGHT NOTICES OR THIS HEADER.
 * <p>
 * Copyright (c) 2020 IO-Teknologi Indonesia, and individual contributors
 * as indicated by the @author tags. All Rights Reserved
 * <p>
 * The contents of this file are subject to the terms of the
 * Common Development and Distribution License (the License).
 * <p>
 * Everyone is permitted to copy and distribute verbatim copies
 * of this license document, but changing it is not allowed.
 */
package id.io.portal.resource;

import id.io.portal.model.LocationRequest;
import id.io.portal.model.LocationResponse;
import id.io.portal.resource.validator.LocationValidator;
import id.io.portal.service.LocationsService;

import javax.ws.rs.*;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import java.util.ArrayList;
import java.util.List;

@Path("locations")
@Produces(MediaType.APPLICATION_JSON)
public class LocationsResource extends BaseResource {

    private LocationsService locationsService;

    //validator
    private LocationValidator validator;

    public LocationsResource() {
        log = getLogger(LocationsResource.this.getClass());
        locationsService = new LocationsService();
        validator = new LocationValidator();
    }

    @GET
    public Response getLocations() {
        final String methodName = "getLocations";
        log.debug(methodName, "GET api/locations");
        start(methodName);
        Response response = buildBadRequestResponse();
        List<LocationResponse> result = locationsService.getLocations();
        response = buildSuccessResponse(result);
        completed(methodName);
        return response;
    }

    @GET
    @Path("/province")
    public Response getProvinces() {
        final String methodName = "getProvinceList";
        log.debug(methodName, "GET api/locations/province");
        start(methodName);
        Response response = buildBadRequestResponse();
        List<String>result = new ArrayList<>();

        List<LocationResponse> locationList = locationsService.getProvinces();
        for (LocationResponse x : locationList) {
            result.add(x.getProvince());
        }
        response = buildSuccessResponse(result);
        completed(methodName);
        return response;
    }

    @GET
    @Path("/regency")
    public Response getRegencies(@QueryParam("province") String filter) {
        final String methodName = "getRegencies";
        log.debug(methodName, "GET api/locations/regency?province=" + filter);
        start(methodName);
        Response response = buildBadRequestResponse();
        List<String>result = new ArrayList<>();
        List<LocationResponse> locationList = locationsService.getRegencies(filter);
        for (LocationResponse x : locationList) {
            result.add(x.getRegency());
        }

        response = buildSuccessResponse(result);
        completed(methodName);
        return response;
    }

    @GET
    @Path("/district")
    public Response getDistricts(@QueryParam("regency") String filter) {
        final String methodName = "getProvinceList";
        log.debug(methodName, "GET api/locations/district?regency=" + filter);
        start(methodName);
        Response response = buildBadRequestResponse();
        List<String>result = new ArrayList<>();
        List<LocationResponse> locationList = locationsService.getDistricts(filter);
        for (LocationResponse x : locationList) {
            result.add(x.getDistrict());
        }
        response = buildSuccessResponse(result);
        completed(methodName);
        return response;
    }

    @GET
    @Path("/village")
    public Response getVillages(@QueryParam("district") String filter) {
        final String methodName = "getVillages";
        log.debug(methodName, "GET api/locations/village?district=" + filter);
        start(methodName);
        Response response = buildBadRequestResponse();
        List<String>result = new ArrayList<>();
        List<LocationResponse> locationList = locationsService.getVillages(filter);
        for (LocationResponse x : locationList) {
            result.add(x.getVillage());
        }
        response = buildSuccessResponse(result);
        completed(methodName);
        return response;
    }

    @GET
    @Path("/postal-code")
    public Response getPostalCodes(@QueryParam("district") String filter) {
        final String methodName = "getPostalCodes";
        log.debug(methodName, "GET api/locations/postal-code?district=" + filter);
        start(methodName);
        Response response = buildBadRequestResponse();
        List<String>result = new ArrayList<>();
        List<LocationResponse> locationList = locationsService.getPostalCodes(filter);
        for (LocationResponse x : locationList) {
            result.add(x.getPostalCode());
        }
        response = buildSuccessResponse(result);
        completed(methodName);
        return response;
    }

    @POST
    @Consumes(MediaType.APPLICATION_JSON)
    public Response addLocation(LocationRequest request) {
        final String methodName = "addLocation";
        log.debug(methodName, "POST api/locations");
        start(methodName);
        logRequest(methodName, request);
        Response response = buildBadRequestResponse();
        if (validator.validateAdd(request) && locationsService.addLocation(request)) {
            response = buildSuccessResponse();
        }
        completed(methodName);
        return response;
    }

    @PUT
    @Consumes(MediaType.APPLICATION_JSON)
    public Response updateLocation(LocationRequest request) {
        final String methodName = "updateLocation";
        log.debug(methodName, "PUT api/locations");
        start(methodName);
        logRequest(methodName, request);
        Response response = buildBadRequestResponse();
        if (validator.validateUpdate(request) && locationsService.updateLocation(request)) {
            response = buildSuccessResponse();
        }
        completed(methodName);
        return response;
    }

    @DELETE
    @Path("/{id}")
    @Consumes(MediaType.APPLICATION_JSON)
    public Response deleteLocation(@PathParam("id") String id) {
        final String methodName = "deleteLocation";
        log.debug(methodName, "DELETE api/locations/" + id);
        start(methodName);
        Response response = buildBadRequestResponse();
        if (!id.isEmpty() && locationsService.deleteLocation(id)) {
            response = buildSuccessResponse();
        }
        completed(methodName);
        return response;
    }

}
