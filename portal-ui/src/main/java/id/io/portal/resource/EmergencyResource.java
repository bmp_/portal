/**
 * DO NOT ALTER OR REMOVE COPYRIGHT NOTICES OR THIS HEADER.
 * <p>
 * Copyright (c) 2020 IO-Teknologi Indonesia, and individual contributors
 * as indicated by the @author tags. All Rights Reserved
 * <p>
 * The contents of this file are subject to the terms of the
 * Common Development and Distribution License (the License).
 * <p>
 * Everyone is permitted to copy and distribute verbatim copies
 * of this license document, but changing it is not allowed.
 */
package id.io.portal.resource;

import id.io.portal.model.EmergencyRequest;
import id.io.portal.model.EmergencyResponse;
import id.io.portal.resource.util.constant.MessageConstant;
import id.io.portal.resource.validator.EmergencyValidator;
import id.io.portal.service.EmergencyService;
import id.io.portal.util.constant.ConstantHelper;
import org.json.JSONException;
import org.json.JSONObject;

import javax.ws.rs.*;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import java.util.List;

@Path("emergency")
@Produces(MediaType.APPLICATION_JSON)
@Consumes(MediaType.APPLICATION_JSON)
public class EmergencyResource extends BaseResource {

    private EmergencyService emergencyService;

    //validator
    private EmergencyValidator validator;

    public EmergencyResource() {
        log = getLogger(this.getClass());
        emergencyService = new EmergencyService();
        validator = new EmergencyValidator();
    }

    @GET
    public Response getEmergency() {
        final String methodName = "getEmergency";
        log.debug(methodName, "GET /emergency");
        start(methodName);
        JSONObject response = new JSONObject();
        try {
            response = emergencyService.getEmergencys();
            completed(methodName);
            return Response.ok().entity(response.get(ConstantHelper.HTTP_RESPONSE).toString()).build();
        } catch (JSONException ex) {
            log.error(methodName, response.getString(ConstantHelper.HTTP_REASON));
            return buildBadRequestResponse("error_get_emergency");
        }
    }

    @POST
    public Response addEmergency(EmergencyRequest request) {
        final String methodName = "addEmergency";
        log.debug(methodName, "POST api/emergency");
        start(methodName);
        Response response = buildBadRequestResponse();
        logRequest(methodName, request);
        if (validator.validate(request)) {
            if (emergencyService.addEmergency(request)) {
                response = buildSuccessResponse();
            } else {
                response = buildNotAcceptableResponse(MessageConstant.EMERGENCY_ADD_FAILED);
            }
        } else {
            response = buildNotAcceptableResponse(MessageConstant.INVALID_REQUEST);
        }
        completed(methodName);
        return response;
    }

    @GET
    @Path("/{userId}")
    public Response getUserEmergency(@PathParam("userId") String userId) {
        final String methodName = "getUserEmergency";
        log.debug(methodName, "GET /emergency/" + userId);
        start(methodName);
        Response response = buildBadRequestResponse();
        List<EmergencyResponse> responseList = emergencyService.getEmergencies(userId);
        response = buildSuccessResponse(responseList);
        completed(methodName);
        return response;
    }
}
