/**
 * DO NOT ALTER OR REMOVE COPYRIGHT NOTICES OR THIS HEADER.
 * <p>
 * Copyright (c) 2020 IO-Teknologi Indonesia, and individual contributors
 * as indicated by the @author tags. All Rights Reserved
 * <p>
 * The contents of this file are subject to the terms of the
 * Common Development and Distribution License (the License).
 * <p>
 * Everyone is permitted to copy and distribute verbatim copies
 * of this license document, but changing it is not allowed.
 */
package id.io.portal.resource.util.constant;

public class MessageConstant {

    //Content Message
    public static final String NO_CONTENT_FOUND = "no_content_found";
    public static final String INVALID_REQUEST = "invalid_request";

    //Email Message
    public static final String EMAIL_ALREADY_USED = "email_already_in_use";

    //User Messages
    public static final String USER_NOT_FOUND = "user_not_found";

    public static final String USER_CREATE_FAILED = "failed_create_user";
    public static final String USER_UPDATE_FAILED ="failed_update_user";
    public static final String USER_DELETE_FAILED = "failed_delete_user";

    //Location Messages
    public static final String LOCATION_UPDATE_FAILED ="failed_update_location";

    //Gestation Messages
    public static final String GESTATION_ADD_FAILED ="failed_add_gestation";
    public static final String GESTATION_UPDATE_FAILED ="failed_update_gestation";
    public static final String GESTATION_DELETE_FAILED = "failed_delete_gestation";

    //Gestation Messages
    public static final String SCHEDULE_ADD_FAILED ="failed_add_schedule";
    public static final String SCHEDULE_UPDATE_FAILED ="failed_update_schedule";
    public static final String SCHEDULE_DELETE_FAILED = "failed_delete_schedule";

    //OTP Messages
    public static final String OTP_SEND_SUCCESSFUL ="send_otp_successful";
    public static final String OTP_SEND_FAILED ="send_otp_failed";
    public static final String OTP_EXPIRED ="otp_expired";
    public static final String OTP_INVALID ="invalid_otp";
    public static final String OTP_IS_MISSING ="otp_is_missing";

    //Emergency Messages
    public static final String EMERGENCY_ADD_FAILED ="failed_add_emergency";

}
