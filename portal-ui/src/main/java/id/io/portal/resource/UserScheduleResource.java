/**
 * DO NOT ALTER OR REMOVE COPYRIGHT NOTICES OR THIS HEADER.
 * <p>
 * Copyright (c) 2020 IO-Teknologi Indonesia, and individual contributors
 * as indicated by the @author tags. All Rights Reserved
 * <p>
 * The contents of this file are subject to the terms of the
 * Common Development and Distribution License (the License).
 * <p>
 * Everyone is permitted to copy and distribute verbatim copies
 * of this license document, but changing it is not allowed.
 */
package id.io.portal.resource;

import id.io.portal.model.UserScheduleRequest;
import id.io.portal.model.UserScheduleResponse;
import id.io.portal.resource.util.constant.MessageConstant;
import id.io.portal.resource.validator.UserValidator;
import id.io.portal.service.UserSchedulesService;

import javax.ws.rs.*;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import java.util.List;

@Path("users/schedule")
@Produces(MediaType.APPLICATION_JSON)
public class UserScheduleResource extends BaseResource {

    private UserSchedulesService schedulesService;

    //validator
    private UserValidator validator;

    public UserScheduleResource() {
        log = getLogger(this.getClass());
        schedulesService = new UserSchedulesService();

        validator = new UserValidator();
    }

    @GET
    @Path("/{userid}")
    @Consumes(MediaType.APPLICATION_JSON)
    public Response getSchedules(@PathParam("userid") String userid) {
        final String methodName = "getSchedules";
        log.debug(methodName, "GET /api/users/schedule/" + userid);
        start(methodName);
        Response response = buildBadRequestResponse();
        if (schedulesService.validateUser(userid)) {
            List<UserScheduleResponse> responseList = schedulesService.getSchedules(userid);
            response = buildSuccessResponse(responseList);
        } else {
            response = buildNoContentResponse(MessageConstant.NO_CONTENT_FOUND);
        }
        completed(methodName);
        return response;
    }

    @GET
    @Path("/{userid}/{id}")
    @Consumes(MediaType.APPLICATION_JSON)
    public Response getSchedule(@PathParam("userid") String userid, @PathParam("id") String id) {
        final String methodName = "getSchedules";
        log.debug(methodName, "GET /api/users/schedule/" + userid + "/" + id);
        start(methodName);
        Response response = buildBadRequestResponse();
        if (schedulesService.validateUser(userid)) {
            UserScheduleResponse result = schedulesService.getSchedule(id);
            response = buildSuccessResponse(result);
        } else {
            response = buildNotAcceptableResponse(MessageConstant.NO_CONTENT_FOUND);
        }
        completed(methodName);
        return response;
    }

    @POST
    @Consumes(MediaType.APPLICATION_JSON)
    public Response createSchedule(UserScheduleRequest request) {
        final String methodName = "createSchedule";
        log.debug(methodName, "POST /api/users/schedule");
        start(methodName);
        Response response = buildBadRequestResponse();
        logRequest(methodName, request);
        if (validator.validate(request)) {
            if (schedulesService.createSchedule(request)) {
                response = buildSuccessResponse();
            } else {
                response = buildNotAcceptableResponse(MessageConstant.SCHEDULE_ADD_FAILED);
            }
        } else {
            response = buildNotAcceptableResponse(MessageConstant.INVALID_REQUEST);
        }
        completed(methodName);
        return response;
    }

    @PUT
    @Consumes(MediaType.APPLICATION_JSON)
    public Response updateSchedule(UserScheduleRequest request) {
        final String methodName = "updateSchedule";
        log.debug(methodName, "PUT /api/users/schedule");
        start(methodName);
        Response response = buildBadRequestResponse();
        logRequest(methodName, request);
        if (validator.validate(request)) {
            if (schedulesService.updateSchedule(request)) {
                response = buildSuccessResponse();
            } else {
                response = buildNotAcceptableResponse(MessageConstant.SCHEDULE_UPDATE_FAILED);
            }
        } else {
            response = buildNotAcceptableResponse(MessageConstant.INVALID_REQUEST);
        }
        completed(methodName);
        return response;
    }

    @DELETE
    @Path("/{id}")
    public Response deleteSchedule(@PathParam("id") String id) {
        final String methodName = "deleteSchedule";
        log.debug(methodName, "DELETE api/users/schedule/" + id);
        start(methodName);
        Response response = buildBadRequestResponse();

        if (schedulesService.deleteSchedule(id)) {
            response = buildSuccessResponse();
        } else {
            response = buildNotAcceptableResponse(MessageConstant.SCHEDULE_DELETE_FAILED);
        }

        completed(methodName);
        return response;
    }
}
