package id.io.portal.console;

import javax.mail.*;
import javax.mail.internet.InternetAddress;
import javax.mail.internet.MimeMessage;
import java.util.Properties;

public class Email {

    public static void main(String[] args) {
        String recipient = "bachtiar.madya.p@gmail.com";
        String subject = "Test email";
        String content = "This is sample email sended from JAVA.";

        sendMail(recipient, subject, content);
    }

    public static void sendMail(String recipient, String subject, String content) {
        final String methodName = "sendMail";
        log(methodName, "start");


        final String username = "support@i-bunda.dev-kms.my.id";
        final String password = "Secret123!";

        Properties prop = new Properties();
        prop.put("mail.smtp.host", "i-bunda.dev-kms.my.id");
        prop.put("mail.smtp.port", "465");
        prop.put("mail.smtp.auth", "true");
        prop.put("mail.smtp.starttls.enable", "true"); //TLS

        prop.put("mail.smtp.socketFactory.port", "465");
        prop.put("mail.smtp.socketFactory.class", "javax.net.ssl.SSLSocketFactory"); //TLS


        Session session = Session.getInstance(prop,
                new javax.mail.Authenticator() {
                    protected PasswordAuthentication getPasswordAuthentication() {
                        return new PasswordAuthentication(username, password);
                    }
                });

        try {

            Message message = new MimeMessage(session);
            message.setFrom(new InternetAddress(username));
            message.setRecipients(
                    Message.RecipientType.TO,
                    InternetAddress.parse(recipient)
            );
            message.setSubject(subject);
            message.setText(content);

            Transport.send(message);
            log(methodName, "Send mail to " + recipient + " successfully");


        } catch (MessagingException e) {
            log(methodName, e.getMessage());
        }
        log(methodName, "completed");
    }

    private static void log(String methodName, String message) {

        System.out.println("[" + methodName + "] : " + message);
    }
}
