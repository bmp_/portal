/**
 * DO NOT ALTER OR REMOVE COPYRIGHT NOTICES OR THIS HEADER.
 *
 * Copyright (c) 2020 IO-Teknologi Indonesia, and individual contributors
 * as indicated by the @author tags. All Rights Reserved
 *
 * The contents of this file are subject to the terms of the
 * Common Development and Distribution License (the License).
 *
 * Everyone is permitted to copy and distribute verbatim copies
 * of this license document, but changing it is not allowed.
 *
 */
package id.io.portal.console;

import id.io.portal.manager.EncryptionManager;

/**
 *
 * @author bmp
 */
public class Encryption {
    
    public static void main(String [] args)
    {
        EncryptionManager encryptionManager = new EncryptionManager();
        System.out.println(encryptionManager.encrypt("Secret123!"));
        System.out.println(encryptionManager.decrypt("AquMy/6jndK+9GlqHb9HVw=="));
    }
    
}
