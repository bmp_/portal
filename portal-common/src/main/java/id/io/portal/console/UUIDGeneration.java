/**
 * DO NOT ALTER OR REMOVE COPYRIGHT NOTICES OR THIS HEADER.
 *
 * Copyright (c) 2020 IO-Teknologi Indonesia, and individual contributors
 * as indicated by the @author tags. All Rights Reserved
 *
 * The contents of this file are subject to the terms of the
 * Common Development and Distribution License (the License).
 *
 * Everyone is permitted to copy and distribute verbatim copies
 * of this license document, but changing it is not allowed.
 *
 */
package id.io.portal.console;

import id.io.portal.helper.UUIDGeneratorHelper;

import java.util.UUID;

/**
 *
 * @author bmp
 */
public class UUIDGeneration {
    
    public static void main (String [] args)
    {
        System.out.println(UUIDGeneratorHelper.generateUUID("bmp@mail.com"));
        System.out.println(UUID.randomUUID());
        
        //05fac943-80a7-5241-b237-80e7aef62b19
        //05fac943-80a7-5241-b237-80e7aef62b19
        //b1e75db2-725d-5423-93c9-ecda2bc3e33c
        //f119559e-3a9b-511b-8b67-2d4bd9cf013b
        //f119559e-3a9b-511b-8b67-2d4bd9cf013b
    }
}
