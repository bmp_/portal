package id.io.portal.console;

import java.math.BigDecimal;

public class LongParsers {

    public static  void main(String [] args)
    {
        String latitude ="-6.925432345708487";
        String longitude = "107.72325310664195";

        float lat = Float.valueOf(latitude);
        float lng =  Float.valueOf(longitude);

        System.out.println(latitude +"  "+ longitude);
        System.out.println(" \n");
        System.out.println(lat +"  "+ lng);

    }
}
