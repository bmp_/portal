/**
 * DO NOT ALTER OR REMOVE COPYRIGHT NOTICES OR THIS HEADER.
 *
 * Copyright (c) 2020 IO-Teknologi Indonesia, and individual contributors
 * as indicated by the @author tags. All Rights Reserved
 *
 * The contents of this file are subject to the terms of the
 * Common Development and Distribution License (the License).
 *
 * Everyone is permitted to copy and distribute verbatim copies
 * of this license document, but changing it is not allowed.
 *
 */
package id.io.portal.console;

import java.util.concurrent.TimeUnit;

/**
 *
 * @author bmp
 */
public class Converstion {

    public static void main(String[] args) {
        String seconds = "7200";
        String minute = "120";
        String hour = "2";
//        int day = (int) TimeUnit.SECONDS.toDays(seconds);
//        long hours = TimeUnit.SECONDS.toHours(seconds) - (day * 24);
//        long minute = TimeUnit.SECONDS.toMinutes(seconds) - (TimeUnit.SECONDS.toHours(seconds) * 60);
//        long second = TimeUnit.SECONDS.toSeconds(seconds) - (TimeUnit.SECONDS.toMinutes(seconds) * 60);
//
//        System.out.println("Day " + day + " Hour " + hours + " Minute " + minute + " Seconds " + second);
        System.out.println(convertSecondToMinute(seconds));
        System.out.println(convertSecondToHour(seconds));
        System.out.println(convertMinuteToSecond(minute));
        System.out.println(convertMinuteToHour(minute));
        System.out.println(convertHourToSecond(hour));
        System.out.println(convertHourToMinute(hour));
    }

    public static String convertSecondToMinute(String value) {
        int unit = Integer.parseInt(value);
        return String.valueOf(TimeUnit.SECONDS.toMinutes(unit));
    }

    public static String convertSecondToHour(String value) {
        int unit = Integer.parseInt(value);
        return String.valueOf(TimeUnit.SECONDS.toHours(unit));
    }

    public static String convertMinuteToSecond(String value) {
        int unit = Integer.parseInt(value);
        return String.valueOf(TimeUnit.MINUTES.toSeconds(unit));
    }

    public static String convertMinuteToHour(String value) {
        long unit = Long.parseLong(value);
        return String.valueOf(TimeUnit.MINUTES.toHours(unit));
    }

    public static String convertHourToSecond(String value) {
        long unit = Long.parseLong(value);
        return String.valueOf(TimeUnit.HOURS.toSeconds(unit));
    }

    public static String convertHourToMinute(String value) {
        long unit = Long.parseLong(value);
        return String.valueOf(TimeUnit.HOURS.toMinutes(unit));
    }

}
