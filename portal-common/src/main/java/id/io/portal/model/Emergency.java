package id.io.portal.model;

public class Emergency {

    private int id;
    private String userid;
    private String lat;
    private String lng;
    private String status;
    private String createdt;

    public Emergency() {
    }

    public Emergency(int id, String userid, String lat, String lng, String status, String createdt) {
        this.id = id;
        this.userid = userid;
        this.lat = lat;
        this.lng = lng;
        this.status = status;
        this.createdt = createdt;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getUserid() {
        return userid;
    }

    public void setUserid(String userid) {
        this.userid = userid;
    }

    public String getLat() {
        return lat;
    }

    public void setLat(String lat) {
        this.lat = lat;
    }

    public String getLng() {
        return lng;
    }

    public void setLng(String lng) {
        this.lng = lng;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public String getCreatedt() {
        return createdt;
    }

    public void setCreatedt(String createdt) {
        this.createdt = createdt;
    }
}
