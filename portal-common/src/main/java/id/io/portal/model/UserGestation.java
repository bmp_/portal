/**
 * DO NOT ALTER OR REMOVE COPYRIGHT NOTICES OR THIS HEADER.
 * <p>
 * Copyright (c) 2020 IO-Teknologi Indonesia, and individual contributors
 * as indicated by the @author tags. All Rights Reserved
 * <p>
 * The contents of this file are subject to the terms of the
 * Common Development and Distribution License (the License).
 * <p>
 * Everyone is permitted to copy and distribute verbatim copies
 * of this license document, but changing it is not allowed.
 */
package id.io.portal.model;

public class UserGestation {

    private int id;
    private String userid;
    private String babygender;
    private String babyname;
    private String duedate;
    private String insemination;
    private String firstpregnancy;
    private String misbirth;
    private String beenborn;
    private String createdt;

    public UserGestation() {
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getUserid() {
        return userid;
    }

    public void setUserid(String userid) {
        this.userid = userid;
    }

    public String getBabygender() {
        return babygender;
    }

    public void setBabygender(String babygender) {
        this.babygender = babygender;
    }

    public String getBabyname() {
        return babyname;
    }

    public void setBabyname(String babyname) {
        this.babyname = babyname;
    }

    public String getDuedate() {
        return duedate;
    }

    public void setDuedate(String duedate) {
        this.duedate = duedate;
    }

    public String getInsemination() {
        return insemination;
    }

    public void setInsemination(String insemination) {
        this.insemination = insemination;
    }

    public String getFirstpregnancy() {
        return firstpregnancy;
    }

    public void setFirstpregnancy(String firstpregnancy) {
        this.firstpregnancy = firstpregnancy;
    }

    public String getMisbirth() {
        return misbirth;
    }

    public void setMisbirth(String misbirth) {
        this.misbirth = misbirth;
    }

    public String getBeenborn() {
        return beenborn;
    }

    public void setBeenborn(String beenborn) {
        this.beenborn = beenborn;
    }

    public String getCreatedt() {
        return createdt;
    }

    public void setCreatedt(String createdt) {
        this.createdt = createdt;
    }
}
