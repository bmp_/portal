package id.io.portal.model;

public class LocationProvince {

    private String province;

    public LocationProvince() {
    }

    public String getProvince() {
        return province;
    }

    public void setProvince(String province) {
        this.province = province;
    }
}
