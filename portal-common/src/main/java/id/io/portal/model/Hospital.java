/**
 * DO NOT ALTER OR REMOVE COPYRIGHT NOTICES OR THIS HEADER.
 *
 * Copyright (c) 2020 IO-Teknologi Indonesia, and individual contributors
 * as indicated by the @author tags. All Rights Reserved
 *
 * The contents of this file are subject to the terms of the
 * Common Development and Distribution License (the License).
 *
 * Everyone is permitted to copy and distribute verbatim copies
 * of this license document, but changing it is not allowed.
 *
 */
package id.io.portal.model;

public class Hospital {

    private String id;
    private String name;
    private String address;
    private String contact;
    private String lat;
    private String lng;
    private String districts;
    private String city;
    private String province;
    private String inpatient;
    private String outpatient;
    private String ponek;
    private int doctorobsgynstanby;
    private int doctorobsgynnonstanby;
    private int totaldoctor;
    private int bedsavailable;

    public Hospital() {
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getAddress() {
        return address;
    }

    public void setAddress(String address) {
        this.address = address;
    }

    public String getContact() {
        return contact;
    }

    public void setContact(String contact) {
        this.contact = contact;
    }

    public String getLat() {
        return lat;
    }

    public void setLat(String lat) {
        this.lat = lat;
    }

    public String getLng() {
        return lng;
    }

    public void setLng(String lng) {
        this.lng = lng;
    }

    public String getDistricts() {
        return districts;
    }

    public void setDistricts(String districts) {
        this.districts = districts;
    }

    public String getCity() {
        return city;
    }

    public void setCity(String city) {
        this.city = city;
    }

    public String getProvince() {
        return province;
    }

    public void setProvince(String province) {
        this.province = province;
    }

    public String getInpatient() {
        return inpatient;
    }

    public void setInpatient(String inpatient) {
        this.inpatient = inpatient;
    }

    public String getOutpatient() {
        return outpatient;
    }

    public void setOutpatient(String outpatient) {
        this.outpatient = outpatient;
    }

    public String getPonek() {
        return ponek;
    }

    public void setPonek(String ponek) {
        this.ponek = ponek;
    }

    public int getDoctorobsgynstanby() {
        return doctorobsgynstanby;
    }

    public void setDoctorobsgynstanby(int doctorobsgynstanby) {
        this.doctorobsgynstanby = doctorobsgynstanby;
    }

    public int getDoctorobsgynnonstanby() {
        return doctorobsgynnonstanby;
    }

    public void setDoctorobsgynnonstanby(int doctorobsgynnonstanby) {
        this.doctorobsgynnonstanby = doctorobsgynnonstanby;
    }

    public int getTotaldoctor() {
        return totaldoctor;
    }

    public void setTotaldoctor(int totaldoctor) {
        this.totaldoctor = totaldoctor;
    }

    public int getBedsavailable() {
        return bedsavailable;
    }

    public void setBedsavailable(int bedsavailable) {
        this.bedsavailable = bedsavailable;
    }
}
