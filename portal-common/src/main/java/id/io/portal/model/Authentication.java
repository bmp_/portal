package id.io.portal.model;

import com.fasterxml.jackson.annotation.JsonProperty;

public class Authentication {

    @JsonProperty("userid")
    private String userid;
    @JsonProperty("password")
    private String password;

    public Authentication() {
    }

    public Authentication(String userid, String password) {
        this.userid = userid;
        this.password = password;
    }

    public String getUserid() {
        return userid;
    }

    public void setUserid(String userid) {
        this.userid = userid;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }
}
