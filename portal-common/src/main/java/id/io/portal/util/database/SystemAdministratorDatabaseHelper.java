/**
 * DO NOT ALTER OR REMOVE COPYRIGHT NOTICES OR THIS HEADER.
 *
 * Copyright (c) 2020 IO-Teknologi Indonesia, and individual contributors
 * as indicated by the @author tags. All Rights Reserved
 *
 * The contents of this file are subject to the terms of the
 * Common Development and Distribution License (the License).
 *
 * Everyone is permitted to copy and distribute verbatim copies
 * of this license document, but changing it is not allowed.
 *
 */
package id.io.portal.util.database;

import id.io.portal.model.SystemAdministrator;
import java.sql.SQLException;


import org.jdbi.v3.core.Handle;

public class SystemAdministratorDatabaseHelper extends BaseDatabaseHelper {

    public SystemAdministratorDatabaseHelper() {
        log = getLogger(SystemAdministratorDatabaseHelper.this.getClass());
    }

    public boolean authenticate(String username, String password) {
        final String methodName = "authenticate";
        boolean authenticate = false;
        start(methodName);
        final String sql = "select COUNT(1) FROM systemadministrators where username = :username AND password = :password ;";

        int row = 0;

        try (Handle handle = getHandle()) {
            row = handle.createQuery(sql)
                    .bind("username", username)
                    .bind("password", password)
                    .mapTo(Integer.class).findOnly();
            if (row != 0) {
                authenticate = true;
            }

        } catch (SQLException ex) {
            log.error(methodName, " - errorAuthenticate : " + ex);
        }

        completed(methodName);
        return authenticate;
    }

    public SystemAdministrator getAdminDetails(String username) {
        final  String methodName = "getAdminDetails";
        SystemAdministrator admin = new SystemAdministrator();
        start(methodName);

        final String sql = "SELECT userid, username, givenname, `role`, isactive, createdt FROM systemadministrators WHERE username =:username;";
        try (Handle handle = getHandle()) {
            admin = handle.createQuery(sql)
                    .bind("username", username)
                    .mapToBean(SystemAdministrator.class).first();
        } catch (SQLException ex) {
            log.error(methodName, " - errorGetAdminDetails : " + ex);
        }

        completed(methodName);
        return admin;
    }

}
