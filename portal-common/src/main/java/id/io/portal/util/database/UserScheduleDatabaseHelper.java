/**
 * DO NOT ALTER OR REMOVE COPYRIGHT NOTICES OR THIS HEADER.
 * <p>
 * Copyright (c) 2020 IO-Teknologi Indonesia, and individual contributors
 * as indicated by the @author tags. All Rights Reserved
 * <p>
 * The contents of this file are subject to the terms of the
 * Common Development and Distribution License (the License).
 * <p>
 * Everyone is permitted to copy and distribute verbatim copies
 * of this license document, but changing it is not allowed.
 */
package id.io.portal.util.database;

import id.io.portal.model.UserSchedule;
import org.jdbi.v3.core.Handle;

import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

public class UserScheduleDatabaseHelper extends BaseDatabaseHelper {

    public UserScheduleDatabaseHelper() {
        log = getLogger(UserScheduleDatabaseHelper.this.getClass());
    }

    public boolean validateUser(String userId) {
        final String methodName = "validateUser";
        boolean result = false;
        start(methodName);
        final String sql = "SELECT count(1) FROM user_schedules WHERE userid = :userId;";
        try (Handle handle = getHandle()) {
            int row = handle.createQuery(sql).bind("userId", userId).mapTo(Integer.class).findOnly();
            if (row != 0) {
                result = true;
            }
        } catch (SQLException ex) {
            log.error(methodName, "errorValidateUser");
        }
        completed(methodName);
        return result;
    }

    public boolean createSchedules(UserSchedule userSchedules) {
        final String methodName = "createSchedules";
        boolean result = false;
        start(methodName);

        final String sql = "INSERT INTO user_schedules (userid, schedulename, hospitalid, doctorid, scheduledate, scheduletime, motherweight, bloodpressure, heartrate, notes, status) " +
                "VALUES( :userid, :schedulename, :hospitalid, :doctorid, :scheduledate, :scheduletime, :motherweight, :bloodpressure, :heartrate, :notes, 'in progress');";
        int row = 0;
        try (Handle handle = getHandle()) {
            row = handle.createUpdate(sql).bindBean(userSchedules).execute();
            if (row != 0) {
                result = true;
            }
        } catch (SQLException ex) {
            log.error(methodName, " - errorCreateSchedules");
        }
        completed(methodName);
        return result;
    }

    public List<UserSchedule> getUserSchedules(String userId) {
        final String methodName = "getUserSchedules";
        List<UserSchedule> list = new ArrayList<>();
        start(methodName);

        final String sql = "SELECT id, schedulename, scheduledate, scheduletime, notes, status, createdt " +
                "FROM user_schedules WHERE  userid = :userId;";

        try (Handle handle = getHandle()) {
            list = handle.createQuery(sql).bind("userId", userId).mapToBean(UserSchedule.class).list();
        } catch (SQLException ex) {
            log.error(methodName, "errorGetUserSchedules");
        }
        completed(methodName);
        return list;
    }

    public UserSchedule getUserSchedule(String id) {
        final String methodName = "getUserSchedule";
        UserSchedule userSchedules = new UserSchedule();
        start(methodName);
        final String sql = "SELECT id, schedulename, hospitalid, doctorid, scheduledate, scheduletime, motherweight, " +
                "bloodpressure, heartrate, notes, status, createdt FROM user_schedules WHERE id = :id;";

        try (Handle handle = getHandle()) {
            userSchedules = handle.createQuery(sql).bind("id", id).mapToBean(UserSchedule.class).first();
        } catch (SQLException ex) {
            log.error(methodName, "errorGetUserSchedule");
        }
        completed(methodName);
        return userSchedules;
    }

    public boolean deleteSchedule(String id) {
        final String methodName = "deleteSchedule";
        start(methodName);
        boolean result = false;
        final String deleteSchedules = "DELETE FROM user_schedules WHERE id= :id;";

        int row = 0;
        try (Handle handle = getHandle()) {
            row = handle.createUpdate(deleteSchedules).bind("id", id).execute();
            if (row != 0) {
                result = true;
            }
        } catch (SQLException ex) {
            log.error(methodName, " - errorDeleteSchedule : " + ex);
        }

        completed(methodName);
        return result;

    }

    public boolean deleteUserSchedule(String userId) {
        final String methodName = "deleteUserSchedule";
        start(methodName);
        boolean result = false;
        final String deleteSchedules = "DELETE FROM user_schedules WHERE userid= :userId;";

        try (Handle handle = getHandle()) {
            int row = handle.createUpdate(deleteSchedules).bind("userId", userId).execute();
            result = true;

        } catch (SQLException ex) {
            log.error(methodName, "errorDeleteUserSchedule");
        }
        completed(methodName);
        return result;

    }

    public boolean updateSchedules(UserSchedule userSchedules) {
        final String methodName = "updateSchedules";
        boolean result = false;
        start(methodName);

        final String sql = "UPDATE user_schedules " +
                "SET schedulename= :schedulename, hospitalid= :hospitalid, doctorid= :doctorid, scheduledate= :scheduledate, scheduletime= :scheduletime, motherweight= :motherweight, bloodpressure= :bloodpressure, heartrate= :heartrate, notes= :notes " +
                "WHERE id= :id;";
        int row = 0;
        try (Handle handle = getHandle()) {
            row = handle.createUpdate(sql).bindBean(userSchedules).execute();
            if (row != 0) {
                result = true;
            }
        } catch (SQLException ex) {
            log.error(methodName, " - errorUpdateSchedules");
        }
        completed(methodName);
        return result;
    }
}
