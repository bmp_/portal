package id.io.portal.util.database;

import id.io.portal.model.Doctor;
import org.jdbi.v3.core.Handle;

import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

public class DoctorDatabaseHelper extends BaseDatabaseHelper {

    public DoctorDatabaseHelper() {
        log = getLogger(DoctorDatabaseHelper.this.getClass());
    }

    public List<Doctor> getDoctors() {
        final String methodName = "getDoctors";
        start(methodName);

        List<Doctor> doctorsList = new ArrayList<>();

        final String sql = "SELECT id, `name`, contact FROM doctors;";

        try (Handle handle = getHandle()) {
            doctorsList = handle.createQuery(sql).mapToBean(Doctor.class).list();
        } catch (SQLException ex) {
            log.error(methodName, " - errorGetDoctors");
        }

        completed(methodName);
        return doctorsList;
    }

    public List<Doctor> getDoctorsOnHospital(String hospitalId) {
        final String methodName = "getDoctorsOnHospital";
        start(methodName);
        List<Doctor> doctorsList = new ArrayList<>();
        final String sql = "SELECT id, `name` , contact FROM doctors WHERE hospitalid = :hospitalId;";
        try (Handle handle = getHandle()) {
            doctorsList = handle.createQuery(sql).bind("hospitalId", hospitalId).mapToBean(Doctor.class).list();
        } catch (SQLException ex) {
            log.error(methodName, "errorGetDoctorsOnHospital");
        }
        completed(methodName);
        return doctorsList;
    }

    public Doctor getDoctorByName(String name) {
        final String methodName = "getDoctorByName";
        start(methodName);

        Doctor doctors = new Doctor();

        final String sql = "SELECT id, `name` , contact FROM doctors WHERE `name` = :name;";

        try (Handle handle = getHandle()) {
            doctors = handle.createQuery(sql).bind("name", name).mapToBean(Doctor.class).first();
        } catch (SQLException ex) {
            log.error(methodName, "errorGetDoctorByName");
        }

        completed(methodName);
        return doctors;
    }

    public Doctor getDoctor(String id) {
        final String methodName = "getDoctor";
        start(methodName);

        Doctor doctors = new Doctor();

        final String sql = "SELECT id, `name` , contact FROM doctors WHERE id = :id;";

        try (Handle handle = getHandle()) {
            doctors = handle.createQuery(sql).bind("id", id).mapToBean(Doctor.class).first();
        } catch (SQLException ex) {
            log.error(methodName, "errorGetDoctor");
        }

        completed(methodName);
        return doctors;
    }

    public int countDoctor(String hospitalId) {
        final String methodName = "countDoctor";
        start(methodName);
        final String sql = "SELECT count(*) FROM doctors WHERE hospitalid = :hospitalId;";
        int count = 0;
        try (Handle handle = getHandle()) {
            count = handle.createQuery(sql).bind("hospitalId", hospitalId).mapTo(Integer.class).findOnly();
        } catch (SQLException ex) {
            log.error(methodName, "errorCountDoctor");
        }
        completed(methodName);
        return count;
    }
}
