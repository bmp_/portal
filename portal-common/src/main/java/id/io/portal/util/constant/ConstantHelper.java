/*
 * /**
 *   * DO NOT ALTER OR REMOVE COPYRIGHT NOTICES OR THIS HEADER.
 *   *
 *   * Copyright (c) 2020 IO-Teknologi Indonesia, and individual contributors
 *   * as indicated by the @author tags. All Rights Reserved
 *   *
 *   * The contents of this file are subject to the terms of the
 *   * Common Development and Distribution License (the License).
 *   *
 *   * Everyone is permitted to copy and distribute verbatim copies
 *   * of this license document, but changing it is not allowed.
 *   *
 */
package id.io.portal.util.constant; 

public class ConstantHelper {
    
    public static final String CONFIG_FILE                  = "application.properties";
    
    // REST Endpoints
    public static final String SESSION_KEY                  = "portal-session-key";

    // REST Endpoints
    public static final String HTTP_STATUS_CODE             = "statusCode";
    public static final String HTTP_RESPONSE                = "response";
    public static final String HTTP_REASON                  = "reason";
    public static final String HTTP_CODE                    = "code";
    public static final String HTTP_MESSAGE                 = "message";
    public static final String HTTP_SUCCESSFUL              = "successful";

    //Global COnfig
    public static final String KEY_USERID                   = "user-id";
    public static final String KEY_ID                       = "id";
    public static final String KEY_LATITUDE                 = "latitude";
    public static final String KEY_LONGITUDE                = "longitude";
    public static final String KEY_STATUS                   = "status";
    public static final String KEY_CREATE_DT                = "create-dt";

    // User Configuration
    public static final String USER_ADMINISTRATOR           = "Administrator";
    public static final String KEY_USER_EMAIL               = "email";


    //Address Configuration
    public static final String KEY_USER_LOCATION            = "locations";
    public static final String KEY_LOCATION_ADDRESS         = "address";
    public static final String KEY_LOCATION_ZONE            = "zone";
    public static final String KEY_LOCATION_CITY            = "city";


    // Pregnant Configuration
    public static final String KEY_GESTATION_NAME           = "baby-name";
    public static final String KEY_GESTATION_GENDER         = "baby-gender";
    public static final String KEY_GESTATION_INSEMINATION   = "insemination";
    public static final String KEY_GESTATION_DUE_DATE       = "due-date";
    public static final String KEY_GESTATION_FIRST_PREGNANT = "first-pregnant";
    public static final String KEY_GESTATION_BEEN_BORN      = "been-born";
    public static final String KEY_GESTATION_MISS_BIRTH     = "miss-birth";

    // User Schedules Configuration
    public static final String KEY_SCHEDULES_ID                 = "schedule-id";
    public static final String KEY_SCHEDULES_NAME               = "schedule-name";
    public static final String KEY_SCHEDULES_HOSPITALID         = "hospital-id";
    public static final String KEY_SCHEDULES_DOCTORID           = "doctor-id";
    public static final String KEY_SCHEDULES_APPOINTMENT        = "appointments";
    public static final String KEY_SCHEDULES_APPOINTMENT_DATE   = "appointment-date";
    public static final String KEY_SCHEDULES_APPOINTMENT_TIME   = "appointment-time";
    public static final String KEY_SCHEDULES_MOTHER_WEIGHT      = "mother-weight";
    public static final String KEY_SCHEDULES_BLOOD_PRESSURE     = "blood-pressure";
    public static final String KEY_SCHEDULES_HEART_RATE         = "heart-rate";
    public static final String KEY_SCHEDULES_NOTES              = "notes";

    // Hopital Configuration
    public static final String KEY_HOSPITAL_ID              = "hospital-id";
    public static final String KEY_HOSPITAL_NAME            = "name";
    public static final String KEY_HOSPITAL_CONTACT         = "contact";
    public static final String KEY_HOSPITAL_ADDRESS         = "address";

    // Doctor Configuration
    public static final String KEY_DOCTOR_ID                = "doctor-id";
    public static final String KEY_DOCTOR_NAME              = "name";
    public static final String KEY_DOCTOR_CONTACT           = "contact";

    public static final String KEY_DOCTOR_PRACTICAL         = "practice";
    public static final String KEY_DOCTOR_PRACTICAL_DAYS    = "days";
    public static final String KEY_DOCTOR_TIME_CLOSE        = "time-start";
    public static final String KEY_DOCTOR_TIME_START        = "time-close";

     // Encvryption Configuration key
     public static final String ENCRYPTION_KEY              = "ENCRYPTION_KEY";
     
     // Email Configuration key
     public static final String MAIL_USERNAME               = "MAIL_USERNAME";
     public static final String MAIL_PASSWORD               = "MAIL_PASSWORD";
     public static final String MAIL_SERVER_FROM_ADDRESS    = "MAIL_SERVER_FROM_ADDRESS";
     public static final String MAIL_SERVER_PREFIX          = "MAIL_SERVER_PREFIX";
     public static final String MAIL_SECURED_SMTP           = "MAIL_SECURED_SMTP";
     public static final String MAIL_HOST_NAME              = "MAIL_HOST_NAME";
     public static final String MAIL_SMTP_PORT              = "MAIL_SMTP_PORT";
     public static final String MAIL_SMTP_TLS               = "MAIL_SMTP_TLS";
     public static final String MAIL_SMTP_SSL               = "MAIL_SMTP_SSL";
     public static final String MAIL_SMTP_SOCKETFACTORY_CLASS = "MAIL_SMTP_SOCKETFACTORY_CLASS";
     public static final String MAIL_SMTP_AUTH              = "MAIL_SMTP_AUTH";
     
     // Email Configuration key
     public static final String OTP_ENABLED                 = "OTP_ENABLED";
     public static final String OTP_VALIDITY                = "OTP_VALIDITY";
     public static final String OTP_LENGTH                  = "OTP_LENGTH";
     public static final String OTP_MAIL_SUBJECT            = "OTP_MAIL_SUBJECT";
     public static final String OTP_MAIL_CONTENT            = "OTP_MAIL_CONTENT";
    
    
}
