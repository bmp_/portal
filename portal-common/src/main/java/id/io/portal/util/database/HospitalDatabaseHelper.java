/**
 * DO NOT ALTER OR REMOVE COPYRIGHT NOTICES OR THIS HEADER.
 * <p>
 * Copyright (c) 2020 IO-Teknologi Indonesia, and individual contributors
 * as indicated by the @author tags. All Rights Reserved
 * <p>
 * The contents of this file are subject to the terms of the
 * Common Development and Distribution License (the License).
 * <p>
 * Everyone is permitted to copy and distribute verbatim copies
 * of this license document, but changing it is not allowed.
 */
package id.io.portal.util.database;

import id.io.portal.model.Hospital;
import org.jdbi.v3.core.Handle;

import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

public class HospitalDatabaseHelper extends BaseDatabaseHelper {

    public HospitalDatabaseHelper() {
        log = getLogger(this.getClass());
    }

    public List<Hospital> getHospitals() {
        final String methodName = "getHospitals";
        List<Hospital> list = new ArrayList<>();
        start(methodName);
        final String sql = "SELECT id, `name`, address, contact, totaldoctor, bedsavailable FROM hospitals;";
        try (Handle handle = getHandle()) {
            list = handle.createQuery(sql).mapToBean(Hospital.class).list();
        } catch (SQLException ex) {
            log.error(methodName, "errorGetHospitals");
        }
        completed(methodName);
        return list;
    }

    public List<Hospital> getHospitalByDistrict(String district) {
        final String methodName = "getHospitalByDistrict";
        List<Hospital> list = new ArrayList<>();
        start(methodName);
        final String sql = "SELECT id, `name`, address, contact, totaldoctor, bedsavailable FROM hospitals " +
                "WHERE district = :district;";
        try (Handle handle = getHandle()) {
            list = handle.createQuery(sql).bind("district", district)
                    .mapToBean(Hospital.class).list();
        } catch (SQLException ex) {
            log.error(methodName, "errorGetHospitalByDistrict");
        }
        completed(methodName);
        return list;
    }

    public Hospital getHospitalByName(String name) {
        final String methodName = "getHospitalByName";
        start(methodName);
        Hospital hospital = new Hospital();
        final String sql = "SELECT id, `name`, address, contact, regency, province, bedsavailable"
                + " FROM hospitals WHERE `name` = :name;";
        try (Handle handle = getHandle()) {
            hospital = handle.createQuery(sql).bind("name", name).mapToBean(Hospital.class).first();
        } catch (SQLException ex) {
            log.error(methodName, "errorGetHospitalByName");
        }
        completed(methodName);
        return hospital;
    }

    public Hospital getHospital(String id) {
        final String methodName = "getHospital";
        start(methodName);
        Hospital hospital = new Hospital();
        final String sql = "SELECT id, `name`, address, contact, regency, province, bedsavailable"
                + " FROM hospitals WHERE id = :id;";
        try (Handle handle = getHandle()) {
            hospital = handle.createQuery(sql).bind("id", id).mapToBean(Hospital.class).first();
        } catch (SQLException ex) {
            log.error(methodName, "errorGetHospital");
        }
        completed(methodName);
        return hospital;
    }

    public boolean addHospitals(Hospital hospital) {
        final String methodName = "addHospitals";
        boolean result = false;
        start(methodName);
        final String sql = "INSERT INTO hospitals (id, `name`, address, contact, lat, lng, districts, city, province, inpatient, outpatient, ponek, doctorobsgynstanby, doctorobsgynnonstanby, totaldoctor, bedsavailable) "
                + "VALUES(:id, :name, :address, :contact, :lat, :lng, :districs, :city, :province, :inpatient, :outpatient, :ponek, :drObsgynStainby, :drObsgynNonStainby, :totalDr, :bedsAvailable);";
        int row = 0;
        try (Handle handle = getHandle()) {

            row = handle.createUpdate(sql)
                    .bind("id", hospital.getId())
                    .bind("name", hospital.getName())
                    .bind("address", hospital.getAddress())
                    .bind("contact", hospital.getContact())
                    .bind("lat", hospital.getLat())
                    .bind("lng", hospital.getLng())
                    .bind("districs", hospital.getDistricts())
                    .bind("city", hospital.getCity())
                    .bind("province", hospital.getProvince())
                    .bind("inpatient", hospital.getInpatient())
                    .bind("outpatient", hospital.getOutpatient())
                    .bind("ponek", hospital.getPonek())
                    .bind("drObsgynStainby", hospital.getDoctorobsgynstanby())
                    .bind("drObsgynNonStainby", hospital.getDoctorobsgynnonstanby())
                    .bind("totalDr", hospital.getTotaldoctor())
                    .bind("bedsAvailable", hospital.getBedsavailable())
                    .execute();
            if (row != 0) {
                result = true;
            }
        } catch (SQLException ex) {
            log.error(methodName, " - errorAddHospitals");
        }

        completed(methodName);
        return result;
    }

    public boolean updateHospitals(Hospital hospital) {
        final String methodName = "updateHospitals";
        boolean result = false;
        start(methodName);
        final String sql = "UPDATE hospitals SET "
                + "`name`= :name, "
                + "address= :address, "
                + "contact= :contact, "
                + "lat= :lat, "
                + "lng= :lng, "
                + "districs= :districs, "
                + "city= :city, "
                + "province= :province, "
                + "inpatient= :inpatient, "
                + "outpatient= :outpatient, "
                + "ponek= :ponek, "
                + "drObsgynStainby= :drObsgynStainby, "
                + "drObsgynNonStainby= :drObsgynNonStainby, "
                + "totalDr= :totalDr, "
                + "bedsAvailable= :bedsAvailable "
                + "WHERE id= :id;";
        int row = 0;
        try (Handle handle = getHandle()) {

            row = handle.createUpdate(sql)
                    .bind("id", hospital.getId())
                    .bind("name", hospital.getName())
                    .bind("address", hospital.getAddress())
                    .bind("contact", hospital.getContact())
                    .bind("lat", hospital.getLat())
                    .bind("lng", hospital.getLng())
                    .bind("districs", hospital.getDistricts())
                    .bind("city", hospital.getCity())
                    .bind("province", hospital.getProvince())
                    .bind("inpatient", hospital.getInpatient())
                    .bind("outpatient", hospital.getOutpatient())
                    .bind("ponek", hospital.getPonek())
                    .bind("drObsgynStainby", hospital.getDoctorobsgynstanby())
                    .bind("drObsgynNonStainby", hospital.getDoctorobsgynnonstanby())
                    .bind("totalDr", hospital.getTotaldoctor())
                    .bind("bedsAvailable", hospital.getBedsavailable())
                    .execute();
            if (row != 0) {
                result = true;
            }
        } catch (SQLException ex) {
            log.error(methodName, " - errorUpdateHospitals");
        }

        completed(methodName);
        return result;
    }

    public boolean deleteHospitals(String id) {
        final String methodName = "deleteHospitals";
        boolean result = false;
        start(methodName);
        final String sql = "DELETE FROM hospitals WHERE id = :id;";
        int row = 0;
        try (Handle handle = getHandle()) {

            row = handle.createUpdate(sql)
                    .bind("id", id)
                    .execute();
            if (row != 0) {
                result = true;
            }

        } catch (SQLException ex) {
            log.error(methodName, " - errorDeleteHospitals");
        }

        completed(methodName);
        return result;
    }

}
