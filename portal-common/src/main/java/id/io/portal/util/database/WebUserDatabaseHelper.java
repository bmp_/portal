package id.io.portal.util.database;

import id.io.portal.model.User;
import id.io.portal.model.WebUser;
import org.jdbi.v3.core.Handle;

import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

public class WebUserDatabaseHelper extends BaseDatabaseHelper {

    public WebUserDatabaseHelper() {
        log = getLogger(this.getClass());
    }

    public boolean createUser(WebUser users) {
        final String methodName = "createUser";
        boolean result = false;
        start(methodName);
        final String userSql = "INSERT INTO web_users (userid, fullname, email, phone, `role`, isactive) " +
                "VALUES(:userid, :fullname, :email, :phone, :role, 'true');";

        try (Handle handle = getHandle()) {
            int row = handle.createUpdate(userSql).bindBean(users)
                    .execute();
            if (row != 0) {
                result = true;
            }
        } catch (SQLException ex) {
            log.error(methodName, "errorCreateUser");
        }
        completed(methodName);
        return result;
    }

    public List<WebUser> getUsers() {
        final String methodName = "getUsers";
        start(methodName);
        List<WebUser> userList = new ArrayList<>();
        final String sql = "SELECT userid, fullname, email, phone, `role`, isactive, createdt FROM web_users;";
        try (Handle handle = getHandle()) {
            userList = handle.createQuery(sql).mapToBean(WebUser.class).list();
        } catch (SQLException ex) {
            log.error(methodName, "errorGetUsers");
        }
        completed(methodName);
        return userList;
    }

}
