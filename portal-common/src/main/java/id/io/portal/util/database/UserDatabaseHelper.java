/*
 * DO NOT ALTER OR REMOVE COPYRIGHT NOTICES OR THIS HEADER.
 * <p>
 * Copyright (c) 2020 IO-Teknologi Indonesia, and individual contributors
 * as indicated by the @author tags. All Rights Reserved
 * <p>
 * The contents of this file are subject to the terms of the
 * Common Development and Distribution License (the License).
 * <p>
 * Everyone is permitted to copy and distribute verbatim copies
 * of this license document, but changing it is not allowed.
 */
package id.io.portal.util.database;

import id.io.portal.model.User;
import org.jdbi.v3.core.Handle;

import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

public class UserDatabaseHelper extends BaseDatabaseHelper {

    public UserDatabaseHelper() {
        log = getLogger(UserDatabaseHelper.this.getClass());
    }

    public List<User> getUsers() {
        final String methodName = "getUsers";
        start(methodName);
        List<User> userList = new ArrayList<>();
        final String sql = "SELECT userid,cardid, givenname, fullname, email, phone, gender, relation, age, avatar, " +
                "managers, isactive, createdt FROM users;";
        try (Handle handle = getHandle()) {
            userList = handle.createQuery(sql).mapToBean(User.class).list();
        } catch (SQLException ex) {
            log.error(methodName, "errorGetUsers");
        }
        completed(methodName);
        return userList;
    }

    public User getUser(String userId) {
        final String methodName = "getUser";
        start(methodName);
        User user = new User();
        final String sql = "SELECT userid,cardid, givenname, fullname, email, phone, gender, relation, age, avatar, " +
                "managers, isactive, createdt FROM users WHERE userid =:userid;";
        try (Handle handle = getHandle()) {
            user = handle.createQuery(sql)
                    .bind("userid", userId)
                    .mapToBean(User.class).first();
        } catch (SQLException ex) {
            log.error(methodName, "errorGetUser");
        }
        completed(methodName);
        return user;
    }

    public String getUserId(String email) {
        final String methodName = "getUserId";
        start(methodName);
        String result = null;
        final String sql = "SELECT userid from users where email = :email;";
        try (Handle handle = getHandle()) {
            result = handle.createQuery(sql).bind("email", email).mapTo(String.class).findOnly();
            if (!result.isEmpty()) {
                completed(methodName);
                return result;
            }
        } catch (SQLException ex) {
            log.error(methodName, "errorGetUserId");
        }
        return result;
    }

    public boolean createUser(User users) {
        final String methodName = "createUser";
        boolean result = false;
        start(methodName);
        final String userSql = "INSERT INTO users (userid, givenname, fullname, email, phone,managers, createdt) " +
                "VALUES(:userid, :givenname, :fullname, :email, :phone, :managers, current_timestamp());";

        try (Handle handle = getHandle()) {
            int row = handle.createUpdate(userSql).bindBean(users)
                    .execute();
            if (row != 0) {
                result = true;
            }
        } catch (SQLException ex) {
            log.error(methodName, "errorCreateUser");
        }
        completed(methodName);
        return result;
    }

    public boolean updateUser(User users) {
        final String methodName = "updateUser";
        boolean result = false;
        start(methodName);
        final String updateUsers = "UPDATE users SET cardid=:cardid, givenname=:givenname, fullname= :fullname, email= :email, " +
                "phone= :phone, avatar= :avatar, relation= :relation, age= :age " +
                "WHERE userid= :userid;";

        try (Handle handle = getHandle()) {
            int row = handle.createUpdate(updateUsers).bindBean(users)
                    .execute();
            if (row != 0) {
                result = true;
            }
        } catch (SQLException ex) {
            log.error(methodName, "errorUpdateUser");
        }
        completed(methodName);
        return result;
    }

    public boolean validateEmail(String email) {
        final String methodName = "validateEmail";
        boolean result = false;
        start(methodName);
        final String sql = "SELECT count(1) FROM users WHERE email = :email;";
        try (Handle handle = getHandle()) {
            int row = handle.createQuery(sql).bind("email", email).mapTo(Integer.class).findOnly();
            if (row != 0) {
                result = true;
            }
        } catch (SQLException ex) {
            log.error(methodName, "errorValidateEmail");
        }
        completed(methodName);
        return result;
    }

    public boolean validateUserId(String userId) {
        final String methodName = "validateUserId";
        boolean result = false;
        start(methodName);
        final String sql = "SELECT count(1) FROM users WHERE userid = :userId;";
        try (Handle handle = getHandle()) {
            int row = handle.createQuery(sql).bind("userId", userId).mapTo(Integer.class).findOnly();
            if (row != 0) {
                result = true;
            }
        } catch (SQLException ex) {
            log.error(methodName, "errorValidateUserId");
        }
        completed(methodName);
        return result;
    }

    public boolean validateUsername(String username) {
        final String methodName = "validateUsername";
        boolean result = false;
        start(methodName);
        final String sql = "SELECT count(1) FROM users WHERE username = :username;";
        try (Handle handle = getHandle()) {
            int row = handle.createQuery(sql).bind("username", username).mapTo(Integer.class).findOnly();
            if (row != 0) {
                result = true;
            }
        } catch (SQLException ex) {
            log.error(methodName, "errorValidateUsername");
        }
        completed(methodName);
        return result;
    }

    public boolean activateUser(String userId) {
        final String methodName = "activateUser";
        start(methodName);
        boolean result = false;
        final String sql = "UPDATE users SET isactive = true WHERE userid = :userId;";
        try (Handle handle = getHandle()) {
            int  row = handle.createUpdate(sql)
                    .bind("userId", userId)
                    .execute();
            if (row != 0) {
                result = true;
            }
        } catch (SQLException ex) {
            log.error(methodName, "errorActivateUser");
        }
        completed(methodName);
        return result;

    }

    public boolean deleteUser(String userId) {
        final String methodName = "deleteUser";
        start(methodName);
        boolean result = false;
        final String deleteUser = "DELETE FROM users WHERE userid= :userId;";
        try (Handle handle = getHandle()) {
            int row = handle.createUpdate(deleteUser).bind("userId", userId).execute();
            if (row != 0) {
                result = true;
            }
        } catch (SQLException ex) {
            log.error(methodName, "errorDeleteUser");
        }
        completed(methodName);
        return result;
    }

}
