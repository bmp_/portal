package id.io.portal.util.database;

import id.io.portal.model.Downline;
import id.io.portal.model.WebUser;
import org.jdbi.v3.core.Handle;

import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

public class DownlineDatabaseHelper extends BaseDatabaseHelper {

    public DownlineDatabaseHelper() {

        log = getLogger(this.getClass());
    }

    public List<Downline> getDownline() {
        final String methodName = "getDownline";
        start(methodName);
        List<Downline> list = new ArrayList<>();
        final String sql = "SELECT id, managedid, userid, createdt FROM downline;";
        try (Handle handle = getHandle()) {
            list = handle.createQuery(sql).mapToBean(Downline.class).list();
        } catch (SQLException ex) {
            log.error(methodName, "errorGetDownline");
        }
        completed(methodName);
        return list;
    }

    public boolean createDownline(Downline downline) {
        final String methodName = "createDownline";
        boolean result = false;
        start(methodName);
        final String userSql = "INSERT INTO downline (id, managedid, userid) VALUES(:id, :managedid, :userid);";

        try (Handle handle = getHandle()) {
            int row = handle.createUpdate(userSql).bindBean(downline)
                    .execute();
            if (row != 0) {
                result = true;
            }
        } catch (SQLException ex) {
            log.error(methodName, "errorCreateDownline");
        }
        completed(methodName);
        return result;
    }
}
