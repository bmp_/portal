/**
 * DO NOT ALTER OR REMOVE COPYRIGHT NOTICES OR THIS HEADER.
 * <p>
 * Copyright (c) 2020 IO-Teknologi Indonesia, and individual contributors
 * as indicated by the @author tags. All Rights Reserved
 * <p>
 * The contents of this file are subject to the terms of the
 * Common Development and Distribution License (the License).
 * <p>
 * Everyone is permitted to copy and distribute verbatim copies
 * of this license document, but changing it is not allowed.
 */
package id.io.portal.util.database;

import id.io.portal.util.helper.DateHelper;
import org.jdbi.v3.core.Handle;

import java.sql.SQLException;
import java.time.LocalDateTime;

public class OtpLogDatabaseHelper extends BaseDatabaseHelper {

    public OtpLogDatabaseHelper() {
        log = getLogger(OtpLogDatabaseHelper.this.getClass());
    }

    public boolean saveOtpLog(String userid, String otp, String info) {
        final String methodName = "saveOtpLog";
        start(methodName);

        boolean result = false;

        final String sql = "INSERT INTO otplog (userid, token, info) VALUES(:userid, :token, :info);";
        int row = 0;
        try (Handle handle = getHandle()) {

            row = handle.createUpdate(sql)
                    .bind("userid", userid)
                    .bind("token", otp)
                    .bind("info", info).execute();
            if (row != 0) {
                result = true;
            }
        } catch (SQLException ex) {
            log.error(methodName, "errorSaveOtpLog");
        }
        completed(methodName);
        return result;

    }

    public boolean checkOtp(String otp) {
        final String methodName = "checkOtp";
        start(methodName);

        boolean isValid = false;

        final String sql = "SELECT COUNT(1) FROM otplog WHERE token =:token;";

        try (Handle h = getHandle()) {

            int count = h.createQuery(sql).bind("token", otp).mapTo(Integer.class).findOnly();
            if (count > 0) {
                isValid = true;
            }
        } catch (SQLException ex) {
            log.error(methodName, "errorCheckOTP");
        }
        completed(methodName);
        return isValid;
    }

    public boolean validateOtp(String userId, String otp, int expiry) {
        final String methodName = "validateOtp";
        start(methodName);

        boolean isValid = false;

        final String sql = "SELECT COUNT(1) FROM otplog WHERE userid = :userid AND token = :token AND createdt > :createDt";

        try (Handle h = getHandle()) {
            LocalDateTime createDt = LocalDateTime.now().minusMinutes(expiry);
            int count = h.createQuery(sql)
                    .bind("userid", userId)
                    .bind("token", otp)
                    .bind("createDt", DateHelper.formatDateTime(createDt)).mapTo(Integer.class).findOnly();
            if (count != 0) {
                isValid = true;
            }
        } catch (SQLException ex) {
            log.error(methodName, "errorValidateOTP");
        }
        completed(methodName);
        return isValid;
    }

    public String getUserId(String otp) {
        final String methodName = "getUserId";
        start(methodName);
        final String sql = "SELECT userid FROM otplog WHERE token = :token";

        String result = null;
        try (Handle h = getHandle()) {
            result = h.createQuery(sql)
                    .bind("token", otp).mapTo(String.class).findOnly();
            log.debug(methodName, result);
        } catch (SQLException ex) {
            log.error(methodName, " - errorGetUserId");
        }
        completed(methodName);
        return result;
    }

    public boolean revokeOTP(String userId, String otp) {
        final String methodName = "revokeOTP";
        start(methodName);

        final String sql = "DELETE FROM otplog WHERE userid = :userid AND token = :token";

        try (Handle h = getHandle()) {
            int count = h.createUpdate(sql)
                    .bind("userid", userId)
                    .bind("token", otp).execute();
            if (count != 0) {
                completed(methodName);
                return true;
            }
        } catch (SQLException ex) {
            log.error(methodName, " - errorRevokeOTP " + ex);
        }
        return false;
    }
}
