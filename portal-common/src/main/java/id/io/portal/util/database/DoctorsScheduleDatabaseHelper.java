package id.io.portal.util.database;

import id.io.portal.model.DoctorsSchedule;
import org.jdbi.v3.core.Handle;

import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

public class DoctorsScheduleDatabaseHelper extends BaseDatabaseHelper {

    public DoctorsScheduleDatabaseHelper() {
        log = getLogger(DoctorsScheduleDatabaseHelper.this.getClass());
    }

    public List<DoctorsSchedule> getDoctorsSchedule(String id) {
        final String methodName = "getDoctorsSchedule/" + id;
        start(methodName);

        List<DoctorsSchedule> schedule = new ArrayList<>();

        final String sql = "SELECT `day`, time_start, time_end FROM doctors_schedule WHERE doctor_id = :id;";

        try (Handle handle = getHandle()) {
            schedule = handle.createQuery(sql).bind("id", id).mapToBean(DoctorsSchedule.class).list();
        } catch (SQLException ex) {
            log.error(methodName, " - errorGetDoctorsSchedule");
        }

        completed(methodName);
        return schedule;
    }
}
