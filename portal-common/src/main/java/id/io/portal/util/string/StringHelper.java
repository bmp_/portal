/**
  * DO NOT ALTER OR REMOVE COPYRIGHT NOTICES OR THIS HEADER.
  *
  * Copyright (c) 2019 IO-Teknologi Indonesia, and individual contributors
  * as indicated by the @author tags. All Rights Reserved
  *
  * The contents of this file are subject to the terms of the
  * Common Development and Distribution License (the License).
  *
  * Everyone is permitted to copy and distribute verbatim copies
  * of this license document, but changing it is not allowed.
  *
  */
package id.io.portal.util.string;

import java.util.ArrayList;
import java.util.List;
import java.util.Random;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class StringHelper {

    public static boolean isNotBlank(String s) {
        return s != null && !s.trim().isEmpty();
    }

    public static boolean hasMandatoryChars(String s) {
        
        if (isNotBlank(s))
            return s.indexOf('{') >= 0 && 
                   s.indexOf('}') >= 0 && 
                   s.indexOf('[') >= 0 && 
                   s.indexOf(']') >= 0 &&
                   s.indexOf('<') >= 0 &&
                   s.indexOf('>') >= 0;
        
        return false;
    }

    // add escape character for double-quotes in supplied Reason textarea
    public static String escapeString(String s) {
        if (isNotBlank(s))
            s = s.replace("\"", "\\\"");

        return s;
    }

    public static String appendBreak(String s) {
        if (s.length() > 0) 
            s = s + "<br />";        
        
        return s;
    }    

    public static String getAlphaNumericString(int n) 
    { 
  
        // lower limit for LowerCase Letters 
        int lowerLimit = 97; 
  
        // lower limit for LowerCase Letters 
        int upperLimit = 122; 
  
        Random random = new Random(); 
  
        // Create a StringBuffer to store the result 
        StringBuilder r = new StringBuilder(n); 
  
        for (int i = 0; i < n; i++) { 
  
            // take a random value between 97 and 122 
            int nextRandomChar = lowerLimit 
                                 + (int)(random.nextFloat() 
                                         * (upperLimit - lowerLimit + 1)); 
  
            // append a character at the end of bs 
            r.append((char)nextRandomChar); 
        } 
  
        // return the resultant string 
        return r.toString(); 
    } 

    public static String removeExceptionMsgRegex(String str) {
        String regex = "[\\w-_\\.+]*[\\w-_\\:][ ]*\\([\\w_\\=+]+\\)[ ]*";
        return str.replaceAll(regex, "");
    }

    public static boolean checkEmail(String mail) {
        String regex = "^[\\w-_\\.+]*[\\w-_\\.]\\@([\\w]+\\.)+[\\w]+[\\w]$";
        return mail.matches(regex);
    }
    
    public static boolean checkPhNo(String mobile) {
        String regex = "^\\+?\\(?\\d+\\)?(\\-?\\d+)*$";
        return mobile.matches(regex);
    }
    
    public static String getApplicationNameFromRole(String input) {
        String regex = "\\[+[\\w_ ]+\\]*";
        Matcher matcher = Pattern.compile(regex).matcher(input);
        if(matcher.find()) {
            int start = matcher.start();
            int end = matcher.end();
            return input.substring(start + 1, end - 1);
        }
        return "";
    }
    
    public static String getCustomAttributes(String input) {
        String regex = "\\[+[\\w_\"_,_ ]+\\]";
        Matcher matcher = Pattern.compile(regex).matcher(input);
        if(matcher.find()) {
            int start = matcher.start();
            int end = matcher.end();
            return input.substring(start + 1, end - 1).replaceAll("\"", "");
        }
        return "";
    }

    public static String removeApplicationNameFromRole(String input) {
        String regex = "\\[+[\\w_ ]+\\][ ]*";
        return input.replaceAll(regex, "");
    }
    
    public static <T> String join(T[] array, String delimiter, String open, String close) {
        StringBuilder sb = new StringBuilder();
        boolean first = true;
        for (T t : array) {
            if (!first) {
                sb.append(delimiter);
            } else {
                first = false;
            }
            if (open != null) {
                sb.append(open);
            }
            sb.append(t);
            if (close != null) {
                sb.append(close);
            }
        }
        return sb.toString();
    }

    public static <T> String join(T[] array, String delimiter) {
        return join(array, delimiter, null, null);
    }

    public static <T> String join(List<T> list, String delimiter, String open, String close) {
        StringBuilder sb = new StringBuilder();
        boolean first = true;
        for (T t : list) {
            if (!first) {
                sb.append(delimiter);
            } else {
                first = false;
            }
            if (open != null) {
                sb.append(open);
            }
            sb.append(t);
            if (close != null) {
                sb.append(close);
            }
        }
        return sb.toString();
    }

    public static <T> String join(List<T> list, String delimiter) {
        return join(list, delimiter, null, null);
    }

    public static String repeat(String element, int multiplier, String delimiter) {
        List<String> list = new ArrayList<>();
        for (int i = 0; i < multiplier; i++) {
            list.add(element);
        }
        return join(list, ",");
    }
    
    public static String mapErrorMessage(String message) {
        message = message.replace("error_", "_");

        int index = message.indexOf("_");
        StringBuilder builder = new StringBuilder(message);
        while (index >= 0) {
            if(index < (message.length() - 1)) {
                char c = message.charAt(index + 1);
                builder.setCharAt(index + 1, Character.toUpperCase(c));
            }
            builder.setCharAt(index, ' ');
            index = message.indexOf("_", index + 1);
        }

        message = builder.substring(1);

        return message;
    }
}
