/**
 * DO NOT ALTER OR REMOVE COPYRIGHT NOTICES OR THIS HEADER.
 * <p>
 * Copyright (c) 2020 IO-Teknologi Indonesia, and individual contributors
 * as indicated by the @author tags. All Rights Reserved
 * <p>
 * The contents of this file are subject to the terms of the
 * Common Development and Distribution License (the License).
 * <p>
 * Everyone is permitted to copy and distribute verbatim copies
 * of this license document, but changing it is not allowed.
 */
package id.io.portal.util.database;

import id.io.portal.helper.JsonHelper;
import id.io.portal.model.UserGestation;
import org.jdbi.v3.core.Handle;

import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

public class UsersGestationDatabaseHelper extends BaseDatabaseHelper {

    public UsersGestationDatabaseHelper() {
        log = getLogger(UsersGestationDatabaseHelper.this.getClass());
    }


    public UserGestation getGestation(String id, String userid) {
        final String methodName = "getGestation";
        start(methodName);
        final String sql = "SELECT id, userid, babygender, babyname, insemination, duedate, firstpregnancy, " +
                "misbirth, beenborn, createdt FROM user_gestations WHERE id = :id AND userid = :userid;";
        UserGestation details = new UserGestation();
        try (Handle handle = getHandle()) {
            details = handle.createQuery(sql)
                    .bind("id", id)
                    .bind("userid", userid)
                    .mapToBean(UserGestation.class).first();
        } catch (SQLException ex) {
            log.error(methodName, "errorGetGestation");
        }
        completed(methodName);
        return details;
    }

    public List<UserGestation> getGestations(String userid) {
        final String methodName = "getGestations";
        List<UserGestation> details = new ArrayList<>();
        start(methodName);
        final String sql = "SELECT id, userid, babygender, babyname, insemination, duedate, firstpregnancy, " +
                "misbirth, beenborn, createdt FROM user_gestations WHERE userid = :userid;";
        try (Handle handle = getHandle()) {
            details = handle.createQuery(sql)
                    .bind("userid", userid)
                    .mapToBean(UserGestation.class).list();
        } catch (SQLException ex) {
            log.error(methodName, "errorGetGestations");
        }
        completed(methodName);
        return details;
    }

    public boolean addGestation(UserGestation userGestation) {
        boolean result = false;
        final String methodName = "addGestation";
        start(methodName);
        final String sql = "INSERT INTO user_gestations "
                + "(userid, babygender, babyname, insemination, duedate, firstpregnancy, misbirth, beenborn) "
                + "VALUES" +
                "(:userid, :babygender, :babyname, :insemination, :duedate, :firstpregnancy, :misbirth, :beenborn);";
        try (Handle handle = getHandle()) {
            int row = handle.createUpdate(sql).bindBean(userGestation)
                    .execute();
            if (row != 0) {
                result = true;
            }
        } catch (SQLException ex) {
            log.error(methodName, "errorAddGestation");
        }
        completed(methodName);
        return result;
    }

    public boolean updateGestation(UserGestation userGestation) {
        boolean result = false;
        final String methodName = "updateGestation";
        start(methodName);

        log.debug(methodName, JsonHelper.toJson(userGestation));

        final String sql = "UPDATE user_gestations " +
                "SET babyname= :babyname, babygender= :babygender, duedate= :duedate ,insemination = :insemination, " +
                "firstpregnancy= :firstpregnancy, beenborn= :beenborn, misbirth= :misbirth  " +
                "WHERE id= :id;";

        try (Handle handle = getHandle()) {
            int row = handle.createUpdate(sql).bindBean(userGestation)
                    .execute();
            if (row != 0) {
                result =  true;
            }
        } catch (SQLException ex) {
            log.error(methodName, "errorUpdateGestation");
        }
        completed(methodName);
        return result;
    }

    public UserGestation getTrackingGestation(String userid) {
        final String methodName = "getTrackingGestation";
        start(methodName);
        final String sql = "SELECT id, userid, babygender, babyname, insemination, duedate, firstpregnancy," +
                " misbirth, beenborn, createdt FROM user_gestations WHERE userid = :userid " +
                "AND misbirth = 'false' AND beenborn = 'false';";
        UserGestation result = new UserGestation();
        try (Handle handle = getHandle()) {
            result = handle.createQuery(sql)
                    .bind("userid", userid)
                    .mapToBean(UserGestation.class).first();
        } catch (SQLException ex) {
            log.error(methodName, "errorGetTrackingGestation");
        }
        completed(methodName);
        return result;
    }

    public boolean deleteGestation(String id) {
        final String methodName = "deleteGestation";
        start(methodName);
        boolean result = false;
        final String sql = "DELETE FROM user_gestations WHERE id= :id;";

        try (Handle handle = getHandle()) {
            int row = handle.createUpdate(sql).bind("id", id).execute();
            if (row != 0) {
                result =  true;
            }
        } catch (SQLException ex) {
            log.error(methodName, "errorDeleteGestation");
        }
        completed(methodName);
        return result;

    }

    public boolean validateUser(String userId) {
        final String methodName = "validateUser";
        boolean result = false;
        start(methodName);

        final String sql = "SELECT count(1) FROM user_gestations WHERE userid = :userId;";
        try (Handle handle = getHandle()) {
            int row = handle.createQuery(sql).bind("userId", userId).mapTo(Integer.class).findOnly();
            if (row != 0) {
                result = true;
            }
        } catch (SQLException ex) {
            log.error(methodName, "errorValidateUser");
        }
        completed(methodName);
        return result;
    }

    public boolean removeUserGestations(String userId) {
        final String methodName = "removeUserGestations";
        start(methodName);
        boolean result = false;
        final String sql = "DELETE FROM user_gestations WHERE userid= :userId;";

        try (Handle handle = getHandle()) {
            int row = handle.createUpdate(sql).bind("userId", userId).execute();
            result = true;
        } catch (SQLException ex) {
            log.error(methodName, "errorRemoveUserGestations");
        }
        completed(methodName);
        return result;

    }
}
