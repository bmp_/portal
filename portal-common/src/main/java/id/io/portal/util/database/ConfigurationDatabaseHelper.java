/**
 * DO NOT ALTER OR REMOVE COPYRIGHT NOTICES OR THIS HEADER.
 * <p>
 * Copyright (c) 2020 IO-Teknologi Indonesia, and individual contributors
 * as indicated by the @author tags. All Rights Reserved
 * <p>
 * The contents of this file are subject to the terms of the
 * Common Development and Distribution License (the License).
 * <p>
 * Everyone is permitted to copy and distribute verbatim copies
 * of this license document, but changing it is not allowed.
 */
package id.io.portal.util.database;

import id.io.portal.model.Configuration;
import org.jdbi.v3.core.Handle;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.concurrent.ConcurrentHashMap;

public class ConfigurationDatabaseHelper extends BaseDatabaseHelper {

    public ConfigurationDatabaseHelper() {
        log = getLogger(this.getClass());
    }

    public ConcurrentHashMap<String, String> getAllConfiguration() {
        final String methodName = "getAllConfiguration";
        start(methodName);

        ConcurrentHashMap<String, String> result = new ConcurrentHashMap<>();
        List<Configuration> configList = new ArrayList<>();
        final String sql = "SELECT config_key, config_value FROM configuration;";

        try (Handle h = getHandle()) {
            configList = h.createQuery(sql).mapToBean(Configuration.class).list();
            for (Configuration configuration : configList) {
                result.put(configuration.getConfig_key(), configuration.getConfig_value());
            }

        } catch (Exception ex) {
            log.error(methodName, "errorGetConfiguration");
        }

        completed(methodName);
        return result;

    }

    public String getConfigurationValue(String key) {
        final String methodName = "getConfigurationValue";
        start(methodName);

        String result = null;

        final String sql = "SELECT config_value FROM configuration WHERE config_key= :key;";

        try (Handle h = getHandle()) {
            result = h.createQuery(sql)
                    .bind("key", key)
                    .mapTo(String.class).first();

        } catch (Exception ex) {
            log.error(methodName, "errorGetConfiguration");
        }
        completed(methodName);
        return result;
    }

    public HashMap<String, String> getMailConfiguration() {
        final String methodName = "getMailConfiguration";
        start(methodName);

        HashMap<String, String> result = new HashMap<>();
        List<Configuration> configList = new ArrayList<>();

        final String sql = "SELECT config_key ,config_value from configuration where config_key like 'MAIL_%';";

        try (Handle h = getHandle()) {
            configList = h.createQuery(sql).mapToBean(Configuration.class).list();
            for (Configuration configuration : configList) {
                result.put(configuration.getConfig_key(), configuration.getConfig_value());
            }
        } catch (Exception ex) {
            log.error(methodName, "errorGetMailConfiguration");
        }
        completed(methodName);
        return result;
    }

    public HashMap<String, String> getOtpConfiguration() {
        final String methodName = "getOtpConfiguration";
        start(methodName);

        HashMap<String, String> result = new HashMap<>();
        List<Configuration> configList = new ArrayList<>();

        final String sql = "SELECT config_key ,config_value from configuration where config_key like 'OTP_%';";

        try (Handle h = getHandle()) {
            configList = h.createQuery(sql).mapToBean(Configuration.class).list();
            for (Configuration configuration : configList) {
                result.put(configuration.getConfig_key(), configuration.getConfig_value());
            }
        } catch (Exception ex) {
            log.error(methodName, "errorGetOtpConfiguration");
        }
        completed(methodName);
        return result;
    }

    public HashMap<String, String> getMobileConfiguration() {
        final String methodName = "getMobileConfiguration";
        start(methodName);

        HashMap<String, String> result = new HashMap<>();
        List<Configuration> configList = new ArrayList<>();

        final String sql = "SELECT config_key ,config_value from configuration where config_key like 'MOBILE_%';";

        try (Handle h = getHandle()) {
            configList = h.createQuery(sql).mapToBean(Configuration.class).list();
            for (Configuration configuration : configList) {
                result.put(configuration.getConfig_key(), configuration.getConfig_value());
            }
        } catch (Exception ex) {
            log.error(methodName, "getMobileConfiguration");
        }
        completed(methodName);
        return result;
    }

}
