package id.io.portal.util.database;

import id.io.portal.model.UserLocation;
import org.jdbi.v3.core.Handle;

import java.sql.SQLException;

public class UserLocationDatabaseHelper extends BaseDatabaseHelper {

    public UserLocationDatabaseHelper() {
        log = getLogger(this.getClass());
    }

    public boolean createUserLocation(UserLocation userLocation) {
        final String methodName = "createUserLocation";
        boolean result = false;
        start(methodName);
        final String sql = "INSERT INTO user_addresses (userid) VALUES (:userid)";
        try (Handle handle = getHandle()) {
            int row = handle.createUpdate(sql).bind("userid", userLocation.getUserid())
                    .execute();
            if (row != 0) {
                result = true;
            }
        } catch (SQLException ex) {
            log.error(methodName, "errorCreateUserLocation");
        }
        completed(methodName);
        return result;
    }

    public UserLocation getUserLocation(String userId) {
        final String methodName = "getUserLocation";
        UserLocation userLocation = new UserLocation();
        start(methodName);
        final String sql = "SELECT userid, village, postalcode, district, regency, province, address, lat, lng " +
                "FROM user_addresses WHERE userid = :userid;";
        try (Handle handle = getHandle()) {
            userLocation = handle.createQuery(sql)
                    .bind("userid", userId)
                    .mapToBean(UserLocation.class).first();
        } catch (SQLException ex) {
            log.error(methodName, "errorGetUserLocation");
        }
        completed(methodName);
        return userLocation;
    }

    public boolean updateUserLocation(UserLocation userLocation) {
        final String methodName = "updateUserLocation";
        start(methodName);
        boolean result = false;
        final String updateUserAddress = "UPDATE portal.user_addresses SET village= :village, postalcode= :postalcode, " +
                "district= :district, regency= :regency, province= :province, address= :address, lat= :lat, lng= :lng" +
                " WHERE userid= :userid;\n";
        try (Handle handle = getHandle()) {
            int row = handle.createUpdate(updateUserAddress).bindBean(userLocation)
                    .execute();
            if (row != 0) {
                result = true;
            }
        } catch (SQLException ex) {
            log.error(methodName, "errorUpdateUserLocation");
        }
        completed(methodName);
        return result;
    }

    public boolean validateUser(String userId) {
        final String methodName = "validateUser";
        boolean result = false;
        start(methodName);
        final String sql = "SELECT count(1) FROM user_addresses WHERE userid = :userId;";
        try (Handle handle = getHandle()) {
            int row = handle.createQuery(sql).bind("userId", userId).mapTo(Integer.class).findOnly();
            if (row != 0) {
                result = true;
            }
        } catch (SQLException ex) {
            log.error(methodName, "errorValidateUser");
        }
        completed(methodName);
        return result;
    }

    public boolean deleteUserLocation(String userId) {
        final String methodName = "deleteUserLocation";
        start(methodName);
        boolean result = false;
        final String sql = "DELETE FROM user_addresses WHERE userid = :userId;";
        try (Handle handle = getHandle()) {
            handle.createUpdate(sql).bind("userId", userId).execute();
            result = true;
        } catch (SQLException ex) {
            log.error(methodName, "errorDeleteUserLocation");
        }
        completed(methodName);
        return result;
    }

}
