/**
 * DO NOT ALTER OR REMOVE COPYRIGHT NOTICES OR THIS HEADER.
 * <p>
 * Copyright (c) 2020 IO-Teknologi Indonesia, and individual contributors
 * as indicated by the @author tags. All Rights Reserved
 * <p>
 * The contents of this file are subject to the terms of the
 * Common Development and Distribution License (the License).
 * <p>
 * Everyone is permitted to copy and distribute verbatim copies
 * of this license document, but changing it is not allowed.
 */
package id.io.portal.util.database;

import id.io.portal.model.Locations;
import org.jdbi.v3.core.Handle;

import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

public class LocationsDatabaseHelper extends BaseDatabaseHelper {

    public LocationsDatabaseHelper() {
        log = getLogger(LocationsDatabaseHelper.this.getClass());
    }

    public List<Locations> getLocations() {
        final String methodName = "getLocations";
        List<Locations> list = new ArrayList<>();
        start(methodName);

        final String sql =
                "select id,postalcode,village,district,regency,province from locations order by province asc;";

        try (Handle handle = getHandle()) {
            list = handle.createQuery(sql).mapToBean(Locations.class).list();
        } catch (SQLException ex) {
            log.error(methodName, "errorGetLocations");
        }

        completed(methodName);
        return list;
    }

    public List<Locations> getProvinces() {
        final String methodName = "getProvinces";
        List<Locations> list = new ArrayList<>();
        start(methodName);

        final String sql = "select province from locations group by province order by province asc;";

        try (Handle handle = getHandle()) {
            list = handle.createQuery(sql).mapToBean(Locations.class).list();
        } catch (SQLException ex) {
            log.error(methodName, "errorGetProvinces");
        }

        completed(methodName);
        return list;
    }

    public List<Locations> getRegencies(String filter) {
        final String methodName = "getRegencies";
        List<Locations> list = new ArrayList<>();
        start(methodName);

        final String sql =
                "select regency from locations where province =:filter group by regency order by regency asc;";

        try (Handle handle = getHandle()) {
            list = handle.createQuery(sql).bind("filter", filter).mapToBean(Locations.class).list();
        } catch (SQLException ex) {
            log.error(methodName, "errorGetRegencies");
        }

        completed(methodName);
        return list;
    }

    public List<Locations> getDistricts(String filter) {
        final String methodName = "getDistricts";
        List<Locations> list = new ArrayList<>();
        start(methodName);

        final String sql =
                "select district from locations where regency= :filter group by district order by district asc;";

        try (Handle handle = getHandle()) {
            list = handle.createQuery(sql).bind("filter", filter).mapToBean(Locations.class).list();
        } catch (SQLException ex) {
            log.error(methodName, "errorGetDistricts");
        }

        completed(methodName);
        return list;
    }

    public List<Locations> getVillages(String filter) {
        final String methodName = "getVillages";
        List<Locations> list = new ArrayList<>();
        start(methodName);

        final String sql =
                "select village from locations where district= :filter order by district asc;";

        try (Handle handle = getHandle()) {
            list = handle.createQuery(sql).bind("filter", filter).mapToBean(Locations.class).list();
        } catch (SQLException ex) {
            log.error(methodName, "errorGetVillages");
        }

        completed(methodName);
        return list;
    }

    public List<Locations> getPostalCodes(String filter) {
        final String methodName = "getPostalCodes";
        List<Locations> list = new ArrayList<>();
        start(methodName);

        final String sql =
                "SELECT postalcode FROM locations WHERE district= :filter  GROUP  BY postalcode ORDER BY postalcode ASC;";

        try (Handle handle = getHandle()) {
            list = handle.createQuery(sql).bind("filter", filter).mapToBean(Locations.class).list();
        } catch (SQLException ex) {
            log.error(methodName, "errorGetPostalCodes");
        }

        completed(methodName);
        return list;
    }

    public boolean addLocation(Locations locations) {
        final String methodName = "addLocation";
        boolean result = false;
        start(methodName);
        final String sql = "INSERT INTO locations (postalcode, village, district, regency, province) " +
                "VALUES(:postalcode, :village, :district, :regency, :province);";
        try (Handle handle = getHandle()) {
            int row = handle.createUpdate(sql).bindBean(locations)
                    .execute();
            if (row != 0) {
                result = true;
            }
        } catch (SQLException ex) {
            log.error(methodName, "errorAddLocation");
        }

        completed(methodName);
        return result;
    }

    public boolean updateLocations(Locations locations) {
        final String methodName = "updateLocations";
        boolean result = false;
        start(methodName);
        final String sql = "UPDATE locations SET postalcode= :postalcode, village= :village, district= :district, " +
                "regency= :regency, province= :province WHERE id= :id;";
        int row = 0;
        try (Handle handle = getHandle()) {

            row = handle.createUpdate(sql)
                    .bindBean(locations)
                    .execute();
            if (row != 0) {
                result = true;
            }
        } catch (SQLException ex) {
            log.error(methodName, " - errorUpdateLocations : " + ex);
        }

        completed(methodName);
        return result;
    }

    public boolean deleteLocation(String id) {
        final String methodName = "deleteLocation";
        boolean result = false;
        start(methodName);
        final String sql = "DELETE FROM locations WHERE id = :id;";
        try (Handle handle = getHandle()) {
            int row = handle.createUpdate(sql)
                    .bind("id", id)
                    .execute();
            if (row != 0) {
                result = true;
            }
        } catch (SQLException ex) {
            log.error(methodName, "errorDeleteLocation");
        }
        completed(methodName);
        return result;
    }

/*

    public List<Locations> getLocations() {
        final  String methodName = "getLocations";
        List<Locations> list = new ArrayList<>();
        start(methodName);

        final String sql = "SELECT id, districs1, districs2, city, province FROM locations ORDER BY province ASC;";

        try (Handle handle = getHandle()) {
            list = handle.createQuery(sql).mapToBean(Locations.class).list();
        } catch (SQLException ex) {
            log.error(methodName, " - errorGetLocatins : " + ex);
        }

        completed(methodName);
        return list;
    }

    public List<String> getDistrics1List() {
        final String methodName = "getDistrics1List";
        List<String> list = new ArrayList<>();
        start(methodName);

        final String sql = "SELECT districs1 from locations group by districs1;";

        try (Handle handle = getHandle()) {
            list = handle.createQuery(sql).mapTo(String.class).list();
        } catch (SQLException ex) {
            log.error(methodName, " - errorGetDistrics1List : " + ex);
        }

        completed(methodName);
        return list;
    }
    public List<String> getDistrics2List() {
        final  String methodName = "getDistrics2List";
        List<String> list = new ArrayList<>();
        start(methodName);

        final String sql = "SELECT districs2 from locations group by districs2;";

        try (Handle handle = getHandle()) {
            list = handle.createQuery(sql).mapTo(String.class).list();
        } catch (SQLException ex) {
            log.error(methodName, " - errorGetDistrics2List : " + ex);
        }

        completed(methodName);
        return list;
    }
    public List<String> getCityList() {
        final String methodName = "getCityList";
        List<String> list = new ArrayList<>();
        start(methodName);

        final String sql = "SELECT city from locations group by city ASC;";

        try (Handle handle = getHandle()) {
            list = handle.createQuery(sql).mapTo(String.class).list();
        } catch (SQLException ex) {
            log.error(methodName, " - errorGetCityList : " + ex);
        }

        completed(methodName);
        return list;
    }
    public List<Locations> getProvinceList() {
        final  String methodName = "getProvinceList";
        List<Locations> list = new ArrayList<>();
        start(methodName);

        final String sql = "select province from locations  GROUP BY province ORDER BY province ASC;";

        try (Handle handle = getHandle()) {
            list = handle.createQuery(sql).mapTo(Locations.class).list();
        } catch (SQLException ex) {
            log.error(methodName, " - errorGetProvinceList : " + ex);
        }

        completed(methodName);
        return list;
    }

    public List<Locations> getLocationsByProvince(String province) {
        final String methodName = "getLocationsByProvince";
        List<Locations> list = new ArrayList<>();
        start(methodName);

        final String sql = "SELECT id, districs1, districs2, city, province FROM locations WHERE province = :province;";

        try (Handle handle = getHandle()) {
            list = handle.createQuery(sql).bind("province", province).mapToBean(Locations.class).list();
        } catch (SQLException ex) {
            log.error(methodName, " - errorGetLocationsByProvince : " + ex);
        }

        completed(methodName);
        return list;
    }

    public List<Locations> getLocationsByDistrics(String districs) {
        final String methodName = "getLocationsByDistrics";
        List<Locations> list = new ArrayList<>();
        start(methodName);

        final String sql = "SELECT id, districs1, districs2, city, province FROM locations WHERE districs2 = :districs;";

        try (Handle handle = getHandle()) {
            list = handle.createQuery(sql).bind("districs", districs).mapToBean(Locations.class).list();
        } catch (SQLException ex) {
            log.error(methodName, " - errorGetLocationsByDistrics : " + ex);
        }

        completed(methodName);
        return list;
    }

    public List<Locations> getLocationsByCity(String city) {
        final String methodName = "getLocationsByCity";
        List<Locations> list = new ArrayList<>();
        start(methodName);

        final String sql = "SELECT id, districs1, districs2, city, province FROM locations WHERE city = :city;";

        try (Handle handle = getHandle()) {
            list = handle.createQuery(sql).bind("city", city).mapToBean(Locations.class).list();
        } catch (SQLException ex) {
            log.error(methodName, " - errorGetLocationsByCity : " + ex);
        }

        completed(methodName);
        return list;
    }

    public boolean create(Locations locations) {
        final String methodName = "addLocations";
        boolean result = false;
        start(methodName);
        final String sql = "INSERT INTO locations (districs1, districs2, city, province) VALUES( :districs1, :districs2, :city, :province);";
        int row = 0;
        try (Handle handle = getHandle()) {

            row = handle.createUpdate(sql)
                    .bind("districs1", locations.getDistrics1())
                    .bind("districs2", locations.getDistrics2())
                    .bind("city", locations.getCity())
                    .bind("province", locations.getProvince())
                    .execute();
            if (row != 0) {
                result = true;
            }
        } catch (SQLException ex) {
            log.error(methodName, " - errorAddNewLocations : " + ex);
        }

        completed(methodName);
        return result;
    }

    public boolean updateLocations(Locations locations) {
        final String methodName = "updateLocations";
        boolean result = false;
        start(methodName);
        final String sql = "UPDATE locations SET districs1 = :districs1, districs2 = :districs2, city= :city, province = :province WHERE id= :id;";
        int row = 0;
        try (Handle handle = getHandle()) {

            row = handle.createUpdate(sql)
                    .bind("id", locations.getId())
                    .bind("districs1", locations.getDistrics1())
                    .bind("districs2", locations.getDistrics2())
                    .bind("city", locations.getCity())
                    .bind("province", locations.getProvince())
                    .execute();
            if (row != 0) {
                result = true;
            }
        } catch (SQLException ex) {
            log.error(methodName," - errorUpdateLocations : " + ex);
        }

        completed(methodName);
        return result;
    }

    public boolean removeLocations(String id) {
        final String methodName = "removeLocations";
        boolean result = false;
        start(methodName);
        final String sql = "DELETE FROM locations WHERE id = :id;";
        int row = 0;
        try (Handle handle = getHandle()) {

            row = handle.createUpdate(sql)
                    .bind("id", id)
                    .execute();
            if (row != 0) {
                result = true;
            }

        } catch (SQLException ex) {
            log.error(methodName, " - errorRemoveLocations : " + ex);
        }

        completed(methodName);
        return result;
    }
*/

}
