/**
 * DO NOT ALTER OR REMOVE COPYRIGHT NOTICES OR THIS HEADER.
 * <p>
 * Copyright (c) 2020 IO-Teknologi Indonesia, and individual contributors
 * as indicated by the @author tags. All Rights Reserved
 * <p>
 * The contents of this file are subject to the terms of the
 * Common Development and Distribution License (the License).
 * <p>
 * Everyone is permitted to copy and distribute verbatim copies
 * of this license document, but changing it is not allowed.
 */
package id.io.portal.util.database;

import id.io.portal.model.UserLevel;
import org.jdbi.v3.core.Handle;

import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

public class UserLevelDatabaseHelper extends BaseDatabaseHelper {

    public UserLevelDatabaseHelper() {
        log = getLogger(UserLevelDatabaseHelper.this.getClass());
    }

    public UserLevel getLevel(String level) {
        final String methodName = "getUserLevel";
        UserLevel userLevel = new UserLevel();
        start(methodName);

        final String sql = "SELECT levelid, levelcode, levelname, description, isactive FROM userlevel WHERE levelname = :level ;";

        try (Handle handle = getHandle()) {
            userLevel = handle.createQuery(sql).bind("level", level).mapToBean(UserLevel.class).first();
        } catch (SQLException ex) {
            log.error(methodName, " - errorGetUserLevel : " + ex);
        }

        completed(methodName);
        return userLevel;
    }

    public List<UserLevel> getLevels() {
        final String methodName = "getLevels";
        List<UserLevel> list = new ArrayList<>();
        start(methodName);

        final String sql = "SELECT levelid, levelcode, levelname, description, isactive FROM userlevel;";

        try (Handle handle = getHandle()) {
            list = handle.createQuery(sql).mapToBean(UserLevel.class).list();
        } catch (SQLException ex) {
            log.error(methodName, " - errorGetLevels : " + ex);
        }

        completed(methodName);
        return list;
    }

    public boolean createLevel(UserLevel level) {
        final String methodName = "createUserLevel";
        boolean result = false;
        start(methodName);
        final String sql = "INSERT INTO userlevel(levelid, levelcode, levelname, description)"
                + "VALUES(:levelid, :levelcode, :levelname, :description);";
        int row = 0;
        try (Handle handle = getHandle()) {

            row = handle.createUpdate(sql)
                    .bind("levelid", level.getLevelid())
                    .bind("levelcode", level.getLevelcode())
                    .bind("levelname", level.getLevelname())
                    .bind("description", level.getDescription())
                    .execute();
            if (row != 0) {
                result = true;
            }
        } catch (SQLException ex) {
            log.error(methodName, " - errorCreateUserLevel : " + ex);
        }

        completed(methodName);
        return result;
    }

    public boolean updateLevel(UserLevel level) {
        final String methodName = "updateLevel";
        boolean result = false;
        start(methodName);
        final String sql = "UPDATE userlevel "
                + "SET levelcode= :levelcode, levelname= :levelname, description= :description WHERE levelid= :levelid;";
        int row = 0;
        try (Handle handle = getHandle()) {

            row = handle.createUpdate(sql)
                    .bind("levelid", level.getLevelid())
                    .bind("levelcode", level.getLevelcode())
                    .bind("levelname", level.getLevelname())
                    .bind("description", level.getDescription())
                    .execute();
            if (row != 0) {
                result = true;
            }
        } catch (SQLException ex) {
            log.error(methodName, " - errorUpdateLevel : " + ex);
        }

        completed(methodName);
        return result;
    }

    public boolean activateLevel(String level, int values) {
        final String methodName = "activateLevel";
        boolean result = false;
        start(methodName);
        final String sql = "UPDATE userlevel SET isactive= :isactive WHERE levelid= :levelid;";
        int row = 0;
        try (Handle handle = getHandle()) {

            row = handle.createUpdate(sql)
                    .bind("levelid", level)
                    .bind("isactive", values)
                    .execute();
            if (row != 0) {
                result = true;
            }
        } catch (SQLException ex) {
            log.error(methodName, " - errorActivateLevel : " + ex);
        }

        completed(methodName);
        return result;
    }

    public boolean removeLevel(String level) {
        final String methodName = "removeLevel";
        boolean result = false;
        start(methodName);
        final String sql = "DELETE FROM userlevel WHERE levelid= :levelid;";
        int row = 0;
        try (Handle handle = getHandle()) {

            row = handle.createUpdate(sql)
                    .bind("levelid", level)
                    .execute();
            if (row != 0) {
                result = true;
            }

        } catch (SQLException ex) {
            log.error(methodName, " - errorRemoveLevel : " + ex);
        }

        completed(methodName);
        return result;
    }

}
