/**
 * DO NOT ALTER OR REMOVE COPYRIGHT NOTICES OR THIS HEADER.
 * <p>
 * Copyright (c) 2020 IO-Teknologi Indonesia, and individual contributors
 * as indicated by the @author tags. All Rights Reserved
 * <p>
 * The contents of this file are subject to the terms of the
 * Common Development and Distribution License (the License).
 * <p>
 * Everyone is permitted to copy and distribute verbatim copies
 * of this license document, but changing it is not allowed.
 */
package id.io.portal.util.database;

import id.io.portal.model.Emergency;
import org.jdbi.v3.core.Handle;

import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

public class EmergencyDatabaseHelper extends BaseDatabaseHelper {

    public EmergencyDatabaseHelper() {
        log = getLogger(this.getClass());
    }

    public boolean addEmergency(Emergency emergency) {
        final String methodName = "addEmergency";
        start(methodName);
        String sql = "INSERT INTO emergency (userid, lat, lng) VALUES(:userid, :lat, :lng);";
        boolean result = false;
        try (Handle handle = getHandle()) {
            int row = handle.createUpdate(sql)
                    .bind("userid", emergency.getUserid())
                    .bind("lat", emergency.getLat())
                    .bind("lng", emergency.getLng())
                    .execute();
            if (row != 0) {
                result = true;
            }
        } catch (SQLException throwables) {
            log.error(methodName, throwables.getMessage());
        }
        completed(methodName);
        return result;
    }

    public List<Emergency> getEmergencyList() {
        final String methodName = "getEmergencyList";
        start(methodName);
        List<Emergency> emergencyList = new ArrayList<>();
        String sql = "SELECT id, userid, lat, lng, status, createdt FROM emergency;";
        try (Handle handle = getHandle()) {
            emergencyList = handle.createQuery(sql).mapToBean(Emergency.class).list();
        } catch (SQLException e) {
            log.error(methodName, e.getMessage());
        }
        completed(methodName);
        return emergencyList;
    }

    public Emergency getEmergency(String id) {
        final String methodName = "getEmergency";
        start(methodName);
        Emergency emergency = new Emergency();
        String sql = "SELECT id, userid, lat, lng, status, createdt FROM emergency WHERE id = :id;";
        try (Handle handle = getHandle()) {
            emergency = handle.createQuery(sql).bind("id", id).mapToBean(Emergency.class).first();
        } catch (SQLException e) {
            log.error(methodName, e.getMessage());
        }
        completed(methodName);
        return emergency;
    }

    public List<Emergency> getEmergencies(String userId) {
        final String methodName = "getEmergencies";
        start(methodName);
        List<Emergency> emergencyList = new ArrayList<>();
        String sql = "SELECT id, userid, lat, lng, status, createdt FROM emergency WHERE userid = :userid;";
        try (Handle handle = getHandle()) {
            emergencyList = handle.createQuery(sql).bind("userid", userId).mapToBean(Emergency.class).list();
        } catch (SQLException e) {
            log.error(methodName, e.getMessage());
        }
        completed(methodName);
        return emergencyList;
    }

    public boolean updateEmergency(Emergency emergency) {
        final String methodName = "updateEmergency";
        start(methodName);
        completed(methodName);
        return false;
    }

    public boolean deleteUser(String userId) {
        final String methodName = "deleteAllEmergency";
        start(methodName);
        completed(methodName);
        return false;
    }

    public boolean deleteEmergency(String id) {
        final String methodName = "deleteEmergency";
        start(methodName);
        completed(methodName);
        return false;
    }

}
