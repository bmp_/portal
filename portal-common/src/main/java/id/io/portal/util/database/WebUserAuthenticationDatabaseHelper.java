package id.io.portal.util.database;

import id.io.portal.model.Authentication;
import org.jdbi.v3.core.Handle;

import java.sql.SQLException;

public class WebUserAuthenticationDatabaseHelper extends BaseDatabaseHelper {

    public WebUserAuthenticationDatabaseHelper() {
        log = getLogger(this.getClass());
    }

    public boolean authenticate(String userid, String password) {
        final String methodName = "authenticate";
        start(methodName);
        boolean authenticate = false;

        final String sql = "select COUNT(1) FROM web_users_authentication where userid = :userid AND password = :password ;";

        int row = 0;

        try (Handle handle = getHandle()) {
            row = handle.createQuery(sql)
                    .bind("userid", userid)
                    .bind("password", password)
                    .mapTo(Integer.class).findOnly();
            if (row != 0) {
                authenticate = true;
            }

        } catch (SQLException ex) {
            log.error(methodName, "errorAuthenticate");
        }

        completed(methodName);
        return authenticate;
    }

    public boolean registerUser(Authentication users) {
        final String methodName = "createUserAuthentications";
        boolean created = false;
        start(methodName);
        final String authSql = "INSERT INTO web_users_authentication (userid, password) VALUES(:userid, :password);";
        int row = 0;

        try (Handle handle = getHandle()) {

            row = handle.createUpdate(authSql).bindBean(users)
                    .execute();
            if (row != 0) {
                created = true;
            }

        } catch (SQLException ex) {
            log.error(methodName, "errorCreateUser");
        }

        completed(methodName);
        return created;
    }

    public boolean deleteUserAuthentications(String userId) {
        final String methodName = "deleteUserAuthentications";
        start(methodName);

        boolean result = false;
        final String deleteAuthentication = "DELETE FROM web_users_authentication WHERE userid= :userId;";
        int row = 0;
        try (Handle handle = getHandle()) {
            row = handle.createUpdate(deleteAuthentication).bind("userId", userId).execute();
            if (row != 0) {
                result = true;
            }
        } catch (SQLException ex) {
            log.error(methodName, "errorDeleteUserAuthentications");
        }
        completed(methodName);
        return result;

    }
}
