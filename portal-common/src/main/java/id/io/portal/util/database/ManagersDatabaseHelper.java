package id.io.portal.util.database;

import id.io.portal.model.Managers;
import org.jdbi.v3.core.Handle;

import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

public class ManagersDatabaseHelper extends BaseDatabaseHelper {

    public ManagersDatabaseHelper() {
        log = getLogger(ManagersDatabaseHelper.this.getClass());
    }

    public List<Managers> getManagers() {
        final String methodName = "getManagers";
        List<Managers> managersList = new ArrayList<>();
        start(methodName);

        final String sql = "SELECT userid, fullname, email, phone FROM managers;";
        try (Handle handle = getHandle()) {
            managersList = handle.createQuery(sql).mapToBean(Managers.class).list();
        } catch (SQLException ex) {
            log.error(methodName, " - errorGetManagers : " + ex);
        }

        completed(methodName);
        return managersList;
    }

    public Managers getManager(String userid) {
        final String methodName = "getManagers";
        Managers managers = new Managers();
        start(methodName);

        final String sql = "SELECT userid, fullname, email, phone FROM managers WHERE userid = :userid;";
        try (Handle handle = getHandle()) {
            managers = handle.createQuery(sql).bind("userid", userid).mapToBean(Managers.class).first();
        } catch (SQLException ex) {
            log.error(methodName, " - errorGetManagers : " + ex);
        }

        completed(methodName);
        return managers;
    }
}
