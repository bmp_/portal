package id.io.portal.manager;

import id.io.portal.util.log.AppLogger;

public class BaseManager {

    protected AppLogger log;

    public BaseManager() {
        // Empty Constructor
    }

    protected AppLogger getLogger(Class<?> clazz) {
        return new AppLogger(clazz);
    }

    protected void start(String methodName) {
        log.info(methodName, "Start");
    }

    protected void completed(String methodName) {
        log.info(methodName, "Completed");
    }
}
