/*
 * /**
 *   * DO NOT ALTER OR REMOVE COPYRIGHT NOTICES OR THIS HEADER.
 *   *
 *   * Copyright (c) 2020 IO-Teknologi Indonesia, and individual contributors
 *   * as indicated by the @author tags. All Rights Reserved
 *   *
 *   * The contents of this file are subject to the terms of the
 *   * Common Development and Distribution License (the License).
 *   *
 *   * Everyone is permitted to copy and distribute verbatim copies
 *   * of this license document, but changing it is not allowed.
 *   *
 */
package id.io.portal.manager;

import com.zaxxer.hikari.HikariConfig;
import com.zaxxer.hikari.HikariDataSource;
import id.io.portal.util.property.Property;

import javax.inject.Singleton;
import javax.sql.DataSource;
import java.sql.Connection;
import java.sql.SQLException;

@Singleton
public class ConnectionManager extends BaseManager {

    private static ConnectionManager instance;

    private HikariDataSource dataSource;

    private ConnectionManager() {
        log = getLogger(this.getClass());

        HikariConfig config = new HikariConfig();

        log.info("Initiating Connection to : " + getProperty(Property.JDBC_URL));

        config.setUsername(getProperty(Property.JDBC_USERNAME));
        config.setPassword(EncryptionManager.getInstance().decrypt(getProperty(Property.JDBC_PASSWORD)));
        config.setJdbcUrl(getProperty(Property.JDBC_URL));
        config.setDriverClassName(getProperty(Property.JDBC_DRIVER));

        dataSource = new HikariDataSource(config);
    }

    public DataSource getDataSource() {
        return dataSource;
    }

    public Connection getConnection() throws SQLException {
        return dataSource.getConnection();
    }

    public void shutdown() {
        log.info("shutdown", "Stopping Data Source");
        dataSource.close();
        dataSource = null;
        log.info("shutdown", "Data Sources Stopped");
    }

    private String getProperty(String key) {
        return PropertyManager.getInstance().getProperty(key);
    }

    public static ConnectionManager getInstance() {
        if (instance == null) {
            instance = new ConnectionManager();
        }
        return instance;
    }

}