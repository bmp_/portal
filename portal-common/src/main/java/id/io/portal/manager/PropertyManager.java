package id.io.portal.manager;

import id.io.portal.util.constant.ConstantHelper;

import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Properties;

public class PropertyManager extends BaseManager {

    private static PropertyManager instance;
    private static Properties prop;

    public PropertyManager() {
        log = getLogger(this.getClass());

        start("Constructor");
        prop = new Properties();
        try {
            prop.load(PropertyManager.class.getClassLoader().getResourceAsStream(ConstantHelper.CONFIG_FILE));
        } catch (Exception ex) {
            log.error("PropertyManager", "Error Loading Properties File", ex);
        }
        completed("Constructor");
    }

    public void initExternalResources(final String filePath, final String filename) {
        final String methodName = "initExternalResources";
        start(methodName);

        try (InputStream input = new FileInputStream(filePath + filename)) {
            log.debug(methodName, "Loading [" + filename + "]...");

            (PropertyManager.prop = new Properties()).load(input);

        } catch (IOException ex) {
            log.error(methodName, "Error Loading Properties File", ex);
        }

        completed(methodName);    }

    public String getProperty(final String key) {
        return getProperty(key, "");
    }

    public int getIntProperty(final String key) {
        return getIntProperty(key, 0);
    }

    public int getIntProperty(final String key, final int defaultValue) {
        return Integer.parseInt(getProperty(key, String.valueOf(defaultValue)));
    }

    public long getLongProperty(final String key) {
        return getLongProperty(key, 0L);
    }

    public long getLongProperty(final String key, final long defaultValue) {
        return Long.parseLong(getProperty(key, String.valueOf(defaultValue)));
    }

    public double getDoubleProperty(final String key) {
        return getDoubleProperty(key, 0.0);
    }

    public double getDoubleProperty(final String key, final double defaultValue) {
        return Double.parseDouble(getProperty(key, String.valueOf(defaultValue)));
    }

    public boolean getBooleanProperty(final String key) {
        return Boolean.getBoolean(getProperty(key, "false"));
    }

    public Date getDateProperty(final String key, final String format) {
        final DateFormat df = new SimpleDateFormat(format);
        Date retDate = new Date();
        try {
            retDate = df.parse(getProperty(key));
        } catch (ParseException e) {
            retDate = new Date(System.currentTimeMillis());
        }
        return retDate;
    }

    private String getProperty(final String key, final String defaultValue) {
        if (PropertyManager.prop != null) {
            return PropertyManager.prop.getProperty(key, defaultValue);
        }
        return defaultValue;
    }

    public void destroy() {
        log.info("shutdown", "Start");
        PropertyManager.prop.clear();
        PropertyManager.prop = null;
        log.info("shutdown", "Completed");
    }
    public static PropertyManager getInstance() {
        if (instance == null) {
            instance = new PropertyManager();
        }
        return instance;
    }
}
