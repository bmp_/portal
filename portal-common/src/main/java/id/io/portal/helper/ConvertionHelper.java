/**
 * DO NOT ALTER OR REMOVE COPYRIGHT NOTICES OR THIS HEADER.
 *
 * Copyright (c) 2020 IO-Teknologi Indonesia, and individual contributors
 * as indicated by the @author tags. All Rights Reserved
 *
 * The contents of this file are subject to the terms of the
 * Common Development and Distribution License (the License).
 *
 * Everyone is permitted to copy and distribute verbatim copies
 * of this license document, but changing it is not allowed.
 *
 */
package id.io.portal.helper;

import java.util.concurrent.TimeUnit;

/**
 *
 * @author bmp
 */
public class ConvertionHelper {

    public static String convertSecondToMinute(String value) {
        int unit = Integer.parseInt(value);
        return String.valueOf(TimeUnit.SECONDS.toMinutes(unit));
    }

    public static String convertSecondToHour(String value) {
        int unit = Integer.parseInt(value);
        return String.valueOf(TimeUnit.SECONDS.toHours(unit));
    }

    public static String convertMinuteToSecond(String value) {
        int unit = Integer.parseInt(value);
        return String.valueOf(TimeUnit.MINUTES.toSeconds(unit));
    }

    public static String convertMinuteToHour(String value) {
        long unit = Long.parseLong(value);
        return String.valueOf(TimeUnit.MINUTES.toHours(unit));
    }

    public static String convertHourToSecond(String value) {
        long unit = Long.parseLong(value);
        return String.valueOf(TimeUnit.HOURS.toSeconds(unit));
    }

    public static String convertHourToMinute(String value) {
        long unit = Long.parseLong(value);
        return String.valueOf(TimeUnit.HOURS.toMinutes(unit));
    }
}
