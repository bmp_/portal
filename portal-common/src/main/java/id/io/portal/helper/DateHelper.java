package id.io.portal.helper;

import java.sql.Time;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;

public class DateHelper {

    public static String DATE_FORMAT = "yyyy-MM-dd";
    public static String TIME_FORMAT = "HH:mm:ss";

    private static SimpleDateFormat date_format = new SimpleDateFormat(DATE_FORMAT);
    private static SimpleDateFormat time_format = new SimpleDateFormat(TIME_FORMAT);

    private DateHelper() {
    }

    public static Date formatDate(String values) {
        Date date = null;
        try {
            date = date_format.parse(values);
        } catch (ParseException e) {
            e.printStackTrace();
        }
        return date;
    }

    public static Time formatTime(String values) {
        Time time = null;
        try {
            Date date = time_format.parse(values);
            time = new Time(date.getTime());
        } catch (ParseException e) {
            e.printStackTrace();
        }
        return time;
    }

    public static void main (String [] args)
    {
        String date = "2021-01-11";
        String time = "01:12:44";


    }

}
