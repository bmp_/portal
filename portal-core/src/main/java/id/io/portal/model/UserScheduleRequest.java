package id.io.portal.model;

import com.fasterxml.jackson.annotation.JsonProperty;

public class UserScheduleRequest {

    @JsonProperty("id")
    private int id;
    @JsonProperty("user-id")
    private String userId;
    @JsonProperty("name")
    private String scheduleName;
    @JsonProperty("hospital")
    private String hospital;
    @JsonProperty("doctor")
    private String doctor;
    @JsonProperty("date")
    private String scheduleDate;
    @JsonProperty("time")
    private String scheduleTime;
    @JsonProperty("weight")
    private double weight;
    @JsonProperty("blood-pressure")
    private String bloodPressure;
    @JsonProperty("heart-rate")
    private String heartRate;
    @JsonProperty("notes")
    private String notes;
    @JsonProperty("status")
    private String status;

    public UserScheduleRequest() {
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getUserId() {
        return userId;
    }

    public void setUserId(String userId) {
        this.userId = userId;
    }

    public String getScheduleName() {
        return scheduleName;
    }

    public void setScheduleName(String scheduleName) {
        this.scheduleName = scheduleName;
    }

    public String getHospital() {
        return hospital;
    }

    public void setHospital(String hospital) {
        this.hospital = hospital;
    }

    public String getDoctor() {
        return doctor;
    }

    public void setDoctor(String doctor) {
        this.doctor = doctor;
    }

    public String getScheduleDate() {
        return scheduleDate;
    }

    public void setScheduleDate(String scheduleDate) {
        this.scheduleDate = scheduleDate;
    }

    public String getScheduleTime() {
        return scheduleTime;
    }

    public void setScheduleTime(String scheduleTime) {
        this.scheduleTime = scheduleTime;
    }

    public double getWeight() {
        return weight;
    }

    public void setWeight(double weight) {
        this.weight = weight;
    }

    public String getBloodPressure() {
        return bloodPressure;
    }

    public void setBloodPressure(String bloodPressure) {
        this.bloodPressure = bloodPressure;
    }

    public String getHeartRate() {
        return heartRate;
    }

    public void setHeartRate(String heartRate) {
        this.heartRate = heartRate;
    }

    public String getNotes() {
        return notes;
    }

    public void setNotes(String notes) {
        this.notes = notes;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }
}
