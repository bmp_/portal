package id.io.portal.model;


import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;

@JsonPropertyOrder({
        "user-id", "full-name", "email", "phone", "role", "down-line", "active", "create-dt"
})
@JsonInclude(JsonInclude.Include.NON_NULL)
public class WebUserResponse {

    @JsonProperty("user-id")
    private String userId;
    @JsonProperty("full-name")
    private String fullName;
    @JsonProperty("email")
    private String email;
    @JsonProperty("phone")
    private String phone;
    @JsonProperty("role")
    private String role;
    @JsonProperty("down-line")
    private String downLine;
    @JsonProperty("active")
    private boolean isActive;
    @JsonProperty("create-dt")
    private String createDt;

    public WebUserResponse() {
    }

    public String getUserId() {
        return userId;
    }

    public void setUserId(String userId) {
        this.userId = userId;
    }

    public String getFullName() {
        return fullName;
    }

    public void setFullName(String fullName) {
        this.fullName = fullName;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getPhone() {
        return phone;
    }

    public void setPhone(String phone) {
        this.phone = phone;
    }

    public String getRole() {
        return role;
    }

    public void setRole(String role) {
        this.role = role;
    }

    public String getDownLine() {
        return downLine;
    }

    public void setDownLine(String downLine) {
        this.downLine = downLine;
    }

    public boolean isActive() {
        return isActive;
    }

    public void setActive(boolean active) {
        isActive = active;
    }

    public String getCreateDt() {
        return createDt;
    }

    public void setCreateDt(String createDt) {
        this.createDt = createDt;
    }
}
