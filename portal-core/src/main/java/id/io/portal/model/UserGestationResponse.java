package id.io.portal.model;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;

@JsonPropertyOrder({
        "id", "name", "gender", "insemination-dt", "hpl-dt", "first-gestation", "been-born", "mis-birth", "create-dt"
})
@JsonInclude(JsonInclude.Include.NON_NULL)
public class UserGestationResponse {

    @JsonProperty("id")
    private int id;
    @JsonProperty("name")
    private String name;
    @JsonProperty("gender")
    private String gender;
    @JsonProperty("hpl-dt")
    private String hplDt;
    @JsonProperty("insemination-dt")
    private String inseminationDt;
    @JsonProperty("first-gestation")
    private boolean firstGestation;
    @JsonProperty("mis-birth")
    private boolean misBirth;
    @JsonProperty("been-born")
    private boolean beenBorn;
    @JsonProperty("create-dt")
    private String createDt;

    public UserGestationResponse() {
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getGender() {
        return gender;
    }

    public void setGender(String gender) {
        this.gender = gender;
    }

    public String getHplDt() {
        return hplDt;
    }

    public void setHplDt(String hplDt) {
        this.hplDt = hplDt;
    }

    public String getInseminationDt() {
        return inseminationDt;
    }

    public void setInseminationDt(String inseminationDt) {
        this.inseminationDt = inseminationDt;
    }

    public boolean isFirstGestation() {
        return firstGestation;
    }

    public void setFirstGestation(boolean firstGestation) {
        this.firstGestation = firstGestation;
    }

    public boolean isMisBirth() {
        return misBirth;
    }

    public void setMisBirth(boolean misBirth) {
        this.misBirth = misBirth;
    }

    public boolean isBeenBorn() {
        return beenBorn;
    }

    public void setBeenBorn(boolean beenBorn) {
        this.beenBorn = beenBorn;
    }

    public String getCreateDt() {
        return createDt;
    }

    public void setCreateDt(String createDt) {
        this.createDt = createDt;
    }
}
