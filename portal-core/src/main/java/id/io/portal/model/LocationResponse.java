package id.io.portal.model;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;

@JsonPropertyOrder({
        "id", "postal-code", "village", "district", "regency", "province"
})
@JsonInclude(JsonInclude.Include.NON_NULL)
public class LocationResponse {

    @JsonProperty("id")
    private String id;
    @JsonProperty("postal-code")
    private String postalCode;
    @JsonProperty("village")
    private String village;
    @JsonProperty("district")
    private String district;
    @JsonProperty("regency")
    private String regency;
    @JsonProperty("province")
    private String province;

    public LocationResponse() {
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getPostalCode() {
        return postalCode;
    }

    public void setPostalCode(String postalCode) {
        this.postalCode = postalCode;
    }

    public String getVillage() {
        return village;
    }

    public void setVillage(String village) {
        this.village = village;
    }

    public String getDistrict() {
        return district;
    }

    public void setDistrict(String district) {
        this.district = district;
    }

    public String getRegency() {
        return regency;
    }

    public void setRegency(String regency) {
        this.regency = regency;
    }

    public String getProvince() {
        return province;
    }

    public void setProvince(String province) {
        this.province = province;
    }
}
