package id.io.portal.model;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;

@JsonPropertyOrder({
        "id", "name", "address", "phone", "latitude", "longitude", "district", "city", "province", "in-patient",
        "out-patient", "ponek", "dr-obsgyn-standby", "dr-obsgyn-non-standby","total-doctor","beds-available"
})
@JsonInclude(JsonInclude.Include.NON_NULL)
public class HospitalResponse {
    @JsonProperty("id")
    private String id;
    @JsonProperty("name")
    private String name;
    @JsonProperty("address")
    private String address;
    @JsonProperty("phone")
    private String phone;
    @JsonProperty("latitude")
    private String latitude;
    @JsonProperty("longitude")
    private String longitude;
    @JsonProperty("district")
    private String district;
    @JsonProperty("city")
    private String city;
    @JsonProperty("province")
    private String province;
    @JsonProperty("in-patient")
    private String inPatient;
    @JsonProperty("out-patient")
    private String outPatient;
    @JsonProperty("ponek")
    private String ponek;
    @JsonProperty("dr-obsgyn-standby")
    private int drObsgynStanby;
    @JsonProperty("dr-obsgyn-non-standby")
    private int drObsgynNonStanby;
    @JsonProperty("total-doctor")
    private int totalDoctor;
    @JsonProperty("beds-available")
    private int bedsAvailable;

    public HospitalResponse() {
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getAddress() {
        return address;
    }

    public void setAddress(String address) {
        this.address = address;
    }

    public String getPhone() {
        return phone;
    }

    public void setPhone(String phone) {
        this.phone = phone;
    }

    public String getLatitude() {
        return latitude;
    }

    public void setLatitude(String latitude) {
        this.latitude = latitude;
    }

    public String getLongitude() {
        return longitude;
    }

    public void setLongitude(String longitude) {
        this.longitude = longitude;
    }

    public String getDistrict() {
        return district;
    }

    public void setDistrict(String district) {
        this.district = district;
    }

    public String getCity() {
        return city;
    }

    public void setCity(String city) {
        this.city = city;
    }

    public String getProvince() {
        return province;
    }

    public void setProvince(String province) {
        this.province = province;
    }

    public String getInPatient() {
        return inPatient;
    }

    public void setInPatient(String inPatient) {
        this.inPatient = inPatient;
    }

    public String getOutPatient() {
        return outPatient;
    }

    public void setOutPatient(String outPatient) {
        this.outPatient = outPatient;
    }

    public String getPonek() {
        return ponek;
    }

    public void setPonek(String ponek) {
        this.ponek = ponek;
    }

    public int getDrObsgynStanby() {
        return drObsgynStanby;
    }

    public void setDrObsgynStanby(int drObsgynStanby) {
        this.drObsgynStanby = drObsgynStanby;
    }

    public int getDrObsgynNonStanby() {
        return drObsgynNonStanby;
    }

    public void setDrObsgynNonStanby(int drObsgynNonStanby) {
        this.drObsgynNonStanby = drObsgynNonStanby;
    }

    public int getTotalDoctor() {
        return totalDoctor;
    }

    public void setTotalDoctor(int totalDoctor) {
        this.totalDoctor = totalDoctor;
    }

    public int getBedsAvailable() {
        return bedsAvailable;
    }

    public void setBedsAvailable(int bedsAvailable) {
        this.bedsAvailable = bedsAvailable;
    }
}
