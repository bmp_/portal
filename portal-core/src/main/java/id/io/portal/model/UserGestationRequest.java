package id.io.portal.model;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;

@JsonInclude(JsonInclude.Include.NON_NULL)
public class UserGestationRequest {

    @JsonProperty("id")
    private int id;
    @JsonProperty("user-id")
    private String userId;
    @JsonProperty("name")
    private String name;
    @JsonProperty("gender")
    private String gender;
    @JsonProperty("insemination-dt")
    private String inseminationDt;
    @JsonProperty("hpl-dt")
    private String hplDt;
    @JsonProperty("first-gestation")
    private boolean firstGestation;
    @JsonProperty("been-born")
    private boolean beenBorn;
    @JsonProperty("mis-birth")
    private boolean misBirth;

    public UserGestationRequest() {
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getUserId() {
        return userId;
    }

    public void setUserId(String userId) {
        this.userId = userId;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getGender() {
        return gender;
    }

    public void setGender(String gender) {
        this.gender = gender;
    }

    public String getInseminationDt() {
        return inseminationDt;
    }

    public void setInseminationDt(String inseminationDt) {
        this.inseminationDt = inseminationDt;
    }

    public String getHplDt() {
        return hplDt;
    }

    public void setHplDt(String hplDt) {
        this.hplDt = hplDt;
    }

    public boolean isFirstGestation() {
        return firstGestation;
    }

    public void setFirstGestation(boolean firstGestation) {
        this.firstGestation = firstGestation;
    }

    public boolean isBeenBorn() {
        return beenBorn;
    }

    public void setBeenBorn(boolean beenBorn) {
        this.beenBorn = beenBorn;
    }

    public boolean isMisBirth() {
        return misBirth;
    }

    public void setMisBirth(boolean misBirth) {
        this.misBirth = misBirth;
    }
}
