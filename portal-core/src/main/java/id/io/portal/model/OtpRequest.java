package id.io.portal.model;

import com.fasterxml.jackson.annotation.JsonProperty;

public class OtpRequest {

    @JsonProperty("email")
    private String email;

    public OtpRequest() {
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }
}
