package id.io.portal.model;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;

@JsonPropertyOrder({
        "id", "user-id", "name", "hospital", "doctor", "date", "time", "weight", "blood-pressure", "heart-rate",
        "managers", "notes", "status", "create-dt"
})
@JsonInclude(JsonInclude.Include.NON_NULL)
public class UserScheduleResponse {

    @JsonProperty("id")
    private int id;
    @JsonProperty("user-id")
    private String userId;
    @JsonProperty("name")
    private String scheduleName;
    @JsonProperty("hospital")
    private HospitalResponse hospital;
    @JsonProperty("doctor")
    private DoctorResponse doctor;
    @JsonProperty("date")
    private String scheduleDate;
    @JsonProperty("time")
    private String scheduleTime;
    @JsonProperty("weight")
    private double weight;
    @JsonProperty("blood-pressure")
    private String bloodPressure;
    @JsonProperty("heart-rate")
    private String heartRate;
    @JsonProperty("notes")
    private String notes;
    @JsonProperty("status")
    private String status;
    @JsonProperty("create-dt")
    private String createDt;

    public UserScheduleResponse() {
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getUserId() {
        return userId;
    }

    public void setUserId(String userId) {
        this.userId = userId;
    }

    public String getScheduleName() {
        return scheduleName;
    }

    public void setScheduleName(String scheduleName) {
        this.scheduleName = scheduleName;
    }

    public HospitalResponse getHospital() {
        return hospital;
    }

    public void setHospital(HospitalResponse hospital) {
        this.hospital = hospital;
    }

    public DoctorResponse getDoctor() {
        return doctor;
    }

    public void setDoctor(DoctorResponse doctor) {
        this.doctor = doctor;
    }

    public String getScheduleDate() {
        return scheduleDate;
    }

    public void setScheduleDate(String scheduleDate) {
        this.scheduleDate = scheduleDate;
    }

    public String getScheduleTime() {
        return scheduleTime;
    }

    public void setScheduleTime(String scheduleTime) {
        this.scheduleTime = scheduleTime;
    }

    public double getWeight() {
        return weight;
    }

    public void setWeight(double weight) {
        this.weight = weight;
    }

    public String getBloodPressure() {
        return bloodPressure;
    }

    public void setBloodPressure(String bloodPressure) {
        this.bloodPressure = bloodPressure;
    }

    public String getHeartRate() {
        return heartRate;
    }

    public void setHeartRate(String heartRate) {
        this.heartRate = heartRate;
    }

    public String getNotes() {
        return notes;
    }

    public void setNotes(String notes) {
        this.notes = notes;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public String getCreateDt() {
        return createDt;
    }

    public void setCreateDt(String createDt) {
        this.createDt = createDt;
    }
}
