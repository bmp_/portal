/**
 * DO NOT ALTER OR REMOVE COPYRIGHT NOTICES OR THIS HEADER.
 * <p>
 * Copyright (c) 2020 IO-Teknologi Indonesia, and individual contributors
 * as indicated by the @author tags. All Rights Reserved
 * <p>
 * The contents of this file are subject to the terms of the
 * Common Development and Distribution License (the License).
 * <p>
 * Everyone is permitted to copy and distribute verbatim copies
 * of this license document, but changing it is not allowed.
 */
package id.io.portal.service;

import id.io.portal.model.*;
import id.io.portal.util.database.UserScheduleDatabaseHelper;

import java.util.ArrayList;
import java.util.List;

public class UserSchedulesService extends BaseService {

    private UserScheduleDatabaseHelper userSchedulesDatabaseHelper;
    private HospitalService hospitalService;
    private DoctorService doctorService;

    public UserSchedulesService() {
        log = getLogger(UserSchedulesService.this.getClass());
        userSchedulesDatabaseHelper = new UserScheduleDatabaseHelper();

        hospitalService = new HospitalService();
        doctorService = new DoctorService();
    }

    public boolean validateUser(String userId) {
        final String methodName = "validateUser";
        start(methodName);
        boolean result = userSchedulesDatabaseHelper.validateUser(userId);
        completed(methodName);
        return result;
    }

    public UserScheduleResponse getSchedule(String id) {
        final String methodName = "getSchedule";
        start(methodName);
        UserSchedule schedule = userSchedulesDatabaseHelper.getUserSchedule(id);
        UserScheduleResponse result = convertModelToResponse(schedule);
        HospitalResponse hospital = hospitalService.getHospital(schedule.getHospitalid());
        result.setHospital(hospital);
        DoctorResponse doctor = doctorService.getDoctor(schedule.getDoctorid());
        result.setDoctor(doctor);
        completed(methodName);
        return result;
    }

    public List<UserScheduleResponse> getSchedules(String userId) {
        final String methodName = "getSchedules";
        start(methodName);
        List<UserSchedule> scheduleList = userSchedulesDatabaseHelper.getUserSchedules(userId);
        List<UserScheduleResponse> result = new ArrayList<>();
        for (UserSchedule schedule : scheduleList) {
            UserScheduleResponse response = convertModelToResponse(schedule);
            result.add(response);
        }
        completed(methodName);
        return result;
    }

    public boolean createSchedule(UserScheduleRequest request) {
        final String methodName = "createSchedule";
        start(methodName);
        UserSchedule schedule = convertRequestToModel(request);
        boolean result = userSchedulesDatabaseHelper.createSchedules(schedule);
        completed(methodName);
        return result;
    }

    public boolean updateSchedule(UserScheduleRequest request) {
        final String methodName = "updateSchedule";
        start(methodName);
        UserSchedule schedule = convertRequestToModel(request);
        boolean result = userSchedulesDatabaseHelper.updateSchedules(schedule);
        completed(methodName);
        return result;
    }

    public boolean removeUser(String userId) {
        final String methodName = "removeUser";
        start(methodName);
        boolean result = userSchedulesDatabaseHelper.deleteUserSchedule(userId);
        completed(methodName);
        return result;
    }

    public boolean deleteSchedule(String id) {
        final String methodName = "deleteSchedule";
        start(methodName);
        boolean result = false;
        if (userSchedulesDatabaseHelper.deleteSchedule(id)) {
            result = true;
        }
        completed(methodName);
        return result;
    }

    public UserScheduleResponse convertModelToResponse(UserSchedule schedules) {
        UserScheduleResponse result = new UserScheduleResponse();
        result.setId(schedules.getId());
        result.setScheduleName(schedules.getSchedulename());
        result.setScheduleDate(schedules.getScheduledate());
        result.setScheduleTime(schedules.getScheduletime());
        result.setWeight(schedules.getMotherweight());
        result.setBloodPressure(schedules.getBloodpressure());
        result.setHeartRate(schedules.getHeartrate());
        result.setNotes(schedules.getNotes());
        result.setStatus(schedules.getStatus());
        result.setCreateDt(schedules.getCreatedt());
        return result;
    }

    public UserSchedule convertRequestToModel(UserScheduleRequest request) {
        UserSchedule result = new UserSchedule();
        result.setUserid(request.getUserId());
        if (request.getId() != 0) {
            result.setId(request.getId());
        }
        result.setSchedulename(request.getScheduleName());
        result.setScheduledate(request.getScheduleDate());
        result.setScheduletime(request.getScheduleTime());
        result.setHospitalid(request.getHospital());
        result.setDoctorid(request.getDoctor());
        result.setMotherweight(request.getWeight());
        result.setBloodpressure(request.getBloodPressure());
        result.setHeartrate(request.getHeartRate());
        result.setNotes(request.getNotes());
        result.setStatus(request.getStatus());
        return result;
    }
}
