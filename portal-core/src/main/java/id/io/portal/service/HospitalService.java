/**
 * DO NOT ALTER OR REMOVE COPYRIGHT NOTICES OR THIS HEADER.
 * <p>
 * Copyright (c) 2020 IO-Teknologi Indonesia, and individual contributors
 * as indicated by the @author tags. All Rights Reserved
 * <p>
 * The contents of this file are subject to the terms of the
 * Common Development and Distribution License (the License).
 * <p>
 * Everyone is permitted to copy and distribute verbatim copies
 * of this license document, but changing it is not allowed.
 */
package id.io.portal.service;


import id.io.portal.model.Hospital;
import id.io.portal.model.HospitalResponse;
import id.io.portal.util.constant.ConstantHelper;
import id.io.portal.util.database.HospitalDatabaseHelper;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.List;

public class HospitalService extends BaseService {

    private HospitalDatabaseHelper hospitalsDatabaseHelper;

    private DoctorService doctorService;

    public HospitalService() {
        log = getLogger(this.getClass());
        hospitalsDatabaseHelper = new HospitalDatabaseHelper();
        doctorService = new DoctorService();
    }

    public JSONObject getHospitals() {
        final String methodName = "getHospitals";
        start(methodName);

        List<Hospital> hospitalList = hospitalsDatabaseHelper.getHospitals();
        JSONObject response = new JSONObject();
        response.put(ConstantHelper.HTTP_RESPONSE, hospitalList);
        completed(methodName);
        return response;
    }

    public List<HospitalResponse> getHospitalByDistrict(String district) {
        final String methodName = "getHospitalByDistrict";
        start(methodName);
        List<Hospital> hospitalList = hospitalsDatabaseHelper.getHospitalByDistrict(district);
        List<HospitalResponse> responseList = new ArrayList<>();
        for (Hospital hospital : hospitalList) {
            HospitalResponse response = convertModelToResponse(hospital);
            responseList.add(response);
        }
        completed(methodName);
        return responseList;
    }

    public HospitalResponse getHospitalByName(String name) {
        final String methodName = "getHospitalByName " + name;
        start(methodName);
        Hospital hospital = hospitalsDatabaseHelper.getHospitalByName(name);

        HospitalResponse response = convertModelToResponse(hospital);
        completed(methodName);
        return response;
    }
/*

    public JSONObject getHospitals(String id) {
        final String methodName = "getHospitals " + id;
        start(methodName);

        Hospital hospital = hospitalsDatabaseHelper.getHospital(id);

        JSONObject hospitalObj = new JSONObject();
        JSONArray doctorsArr = new JSONArray();
        List<Doctor> doctorsList = doctorsDatabaseHelper.getDoctorsOnHospital(String.valueOf(hospital.getId()));
        Doctor doctors = new Doctor();
        List<DoctorsSchedule> scheduleList = new ArrayList<>();
        for (int i = 0; i < doctorsList.size(); i++) {
            doctors.setId(doctorsList.get(i).getId());
            doctors.setName(doctorsList.get(i).getName());
            doctors.setContact(doctorsList.get(i).getContact());

            scheduleList = doctorsScheduleDatabaseHelper.getDoctorsSchedule(doctors.getId());
            JSONObject doctorsObj = new JSONObject();
            doctorsObj.put(ConstantHelper.KEY_ID, doctors.getId());
            doctorsObj.put("name", doctors.getName());
            doctorsObj.put("contact", doctors.getContact());
            doctorsObj.put("schedules", scheduleList);
            doctorsArr.put(doctorsObj);
        }

        hospitalObj.put(ConstantHelper.KEY_ID, hospital.getId());
        hospitalObj.put("name", hospital.getName());
        hospitalObj.put("address", hospital.getAddress());
        hospitalObj.put("contact", hospital.getContact());
        hospitalObj.put(ConstantHelper.KEY_LATITUDE, hospital.getLat());
        hospitalObj.put(ConstantHelper.KEY_LONGITUDE, hospital.getLng());
        hospitalObj.put("districs", hospital.getDistricts());
        hospitalObj.put("city", hospital.getCity());
        hospitalObj.put("province", hospital.getProvince());
        hospitalObj.put("totalDr", doctorsList.size());
        hospitalObj.put("doctors", doctorsArr);
        hospitalObj.put("bedsAvailable", hospital.getBedsavailable());

        JSONObject response = new JSONObject();
        response.put(ConstantHelper.HTTP_RESPONSE, hospitalObj);

        completed(methodName);
        return response;
    }
*/

    public HospitalResponse getHospital(String id) {
        final String methodName = "getHospital";
        start(methodName);
        Hospital hospital = hospitalsDatabaseHelper.getHospital(id);
        HospitalResponse result = convertModelToResponse(hospital);
        result.setTotalDoctor(doctorService.countDoctor(hospital.getId()));
        completed(methodName);
        return result;
    }

    public HospitalResponse convertModelToResponse(Hospital hospital) {
        HospitalResponse result = new HospitalResponse();
        result.setId(hospital.getId());
        result.setName(hospital.getName());
        result.setAddress(hospital.getAddress());
        result.setPhone(hospital.getContact());
        result.setLatitude(hospital.getLat());
        result.setLongitude(hospital.getLng());
        result.setDistrict(hospital.getDistricts());
        result.setCity(hospital.getCity());
        result.setProvince(hospital.getProvince());
        result.setInPatient(hospital.getInpatient());
        result.setOutPatient(hospital.getOutpatient());
        result.setPonek(hospital.getPonek());
        result.setDrObsgynStanby(hospital.getDoctorobsgynstanby());
        result.setDrObsgynNonStanby(hospital.getDoctorobsgynnonstanby());
        result.setBedsAvailable(hospital.getBedsavailable());

        return result;
    }

}
