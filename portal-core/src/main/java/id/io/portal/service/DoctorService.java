/**
 * DO NOT ALTER OR REMOVE COPYRIGHT NOTICES OR THIS HEADER.
 * <p>
 * Copyright (c) 2020 IO-Teknologi Indonesia, and individual contributors
 * as indicated by the @author tags. All Rights Reserved
 * <p>
 * The contents of this file are subject to the terms of the
 * Common Development and Distribution License (the License).
 * <p>
 * Everyone is permitted to copy and distribute verbatim copies
 * of this license document, but changing it is not allowed.
 */
package id.io.portal.service;

import id.io.portal.model.Doctor;
import id.io.portal.model.DoctorResponse;
import id.io.portal.util.database.DoctorDatabaseHelper;

import java.util.ArrayList;
import java.util.List;

public class DoctorService extends BaseService {

    private DoctorDatabaseHelper doctorsDatabaseHelper;

    public DoctorService() {
        log = getLogger(this.getClass());
        doctorsDatabaseHelper = new DoctorDatabaseHelper();
    }

    public List<DoctorResponse> getDoctorsHospital(String id) {
        final String methodName = "getDoctorsHospital";
        start(methodName);
        List<Doctor> doctorsList = doctorsDatabaseHelper.getDoctorsOnHospital(id);
        List<DoctorResponse> responseList = new ArrayList<>();
        for (Doctor doctor : doctorsList) {
            DoctorResponse response = convertModelToResponse(doctor);
            responseList.add(response);
        }
        completed(methodName);
        return responseList;
    }

    public DoctorResponse getDoctorByName(String name) {
        final String methodName = "getDoctorByName";
        start(methodName);
        Doctor doctor = doctorsDatabaseHelper.getDoctorByName(name);
        DoctorResponse result = convertModelToResponse(doctor);
        completed(methodName);
        return result;
    }

    public DoctorResponse getDoctor(String id) {
        final String methodName = "getHospitals";
        start(methodName);
        Doctor doctor = doctorsDatabaseHelper.getDoctor(id);
        DoctorResponse result = convertModelToResponse(doctor);
        completed(methodName);
        return result;
    }

    public int countDoctor(String hospitalId) {
        final String methodName = "countDoctor";
        start(methodName);
        int result = doctorsDatabaseHelper.countDoctor(hospitalId);
        completed(methodName);
        return result;
    }

    public DoctorResponse convertModelToResponse(Doctor doctor) {
        DoctorResponse result = new DoctorResponse();
        result.setId(doctor.getId());
        result.setName(doctor.getName());
        result.setPhone(doctor.getContact());
        result.setCreateDt(doctor.getCreatedt());
        return result;
    }
}
