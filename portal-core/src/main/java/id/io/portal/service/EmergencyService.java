/**
 * DO NOT ALTER OR REMOVE COPYRIGHT NOTICES OR THIS HEADER.
 * <p>
 * Copyright (c) 2020 IO-Teknologi Indonesia, and individual contributors
 * as indicated by the @author tags. All Rights Reserved
 * <p>
 * The contents of this file are subject to the terms of the
 * Common Development and Distribution License (the License).
 * <p>
 * Everyone is permitted to copy and distribute verbatim copies
 * of this license document, but changing it is not allowed.
 */
package id.io.portal.service;

import id.io.portal.model.Emergency;
import id.io.portal.model.EmergencyRequest;
import id.io.portal.model.EmergencyResponse;
import id.io.portal.util.constant.ConstantHelper;
import id.io.portal.util.database.EmergencyDatabaseHelper;
import org.json.JSONArray;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.List;

public class EmergencyService extends BaseService {

    private EmergencyDatabaseHelper emergencyDatabaseHelper;

    public EmergencyService() {
        log = getLogger(this.getClass());
        emergencyDatabaseHelper = new EmergencyDatabaseHelper();
    }

    public JSONObject getEmergencys() {
        final String methodName = "getEmergencys";
        start(methodName);

        List<Emergency> emergencyList = emergencyDatabaseHelper.getEmergencyList();
        JSONArray emergencyArray = new JSONArray();
        for (Emergency emergency : emergencyList) {
            JSONObject object = new JSONObject();
            object.put(ConstantHelper.KEY_ID, emergency.getId());
            object.put(ConstantHelper.KEY_USERID, emergency.getUserid());
            object.put(ConstantHelper.KEY_LATITUDE, emergency.getLat());
            object.put(ConstantHelper.KEY_LONGITUDE, emergency.getLng());
            object.put(ConstantHelper.KEY_STATUS, emergency.getStatus());
            object.put(ConstantHelper.KEY_CREATE_DT, emergency.getCreatedt());
            emergencyArray.put(object);
        }
        JSONObject response = new JSONObject();
        response.put(ConstantHelper.HTTP_RESPONSE, emergencyArray);
        completed(methodName);
        return response;
    }

    public JSONObject getEmergency(String id) {
        final String methodName = "getEmergency";
        start(methodName);

        Emergency emergency = emergencyDatabaseHelper.getEmergency(id);

        JSONObject json = new JSONObject();
        json.put(ConstantHelper.KEY_ID, emergency.getId());
        json.put(ConstantHelper.KEY_USERID, emergency.getUserid());
        json.put(ConstantHelper.KEY_LATITUDE, emergency.getLat());
        json.put(ConstantHelper.KEY_LONGITUDE, emergency.getLng());
        json.put(ConstantHelper.KEY_STATUS, emergency.getStatus());
        json.put(ConstantHelper.KEY_CREATE_DT, emergency.getCreatedt());

        JSONObject response = new JSONObject();
        response.put(ConstantHelper.HTTP_RESPONSE, json);

        return response;
    }

    public List<EmergencyResponse> getEmergencies(String userId) {
        final String methodName = "getEmergencies";
        start(methodName);
        List<Emergency> emergencyList = emergencyDatabaseHelper.getEmergencies(userId);
        List<EmergencyResponse> result = new ArrayList<>();
        for (Emergency emergency : emergencyList) {
            EmergencyResponse response = convertModelToResponse(emergency);
            result.add(response);
        }

        return result;
    }

    public boolean addEmergency(EmergencyRequest request) {
        final String methodName = "addEmergency";
        start(methodName);
        Emergency emergency = convertRequestToModel(request);
        boolean result = emergencyDatabaseHelper.addEmergency(emergency);
        completed(methodName);
        return result;
    }

    public Emergency convertRequestToModel(EmergencyRequest request) {
        Emergency result = new Emergency();
        result.setUserid(request.getUserId());
        result.setLat(request.getLatitude());
        result.setLng(request.getLongitude());
        return result;
    }

    public EmergencyResponse convertModelToResponse(Emergency request) {
        EmergencyResponse result = new EmergencyResponse();
        result.setId(request.getId());
        result.setUserId(request.getUserid());
        result.setLatitude(request.getLat());
        result.setLongitude(request.getLng());
        result.setStatus(request.getStatus());
        result.setCreateDt(request.getCreatedt());
        return result;
    }
}
