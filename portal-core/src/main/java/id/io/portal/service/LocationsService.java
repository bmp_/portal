/**
 * DO NOT ALTER OR REMOVE COPYRIGHT NOTICES OR THIS HEADER.
 * <p>
 * Copyright (c) 2020 IO-Teknologi Indonesia, and individual contributors
 * as indicated by the @author tags. All Rights Reserved
 * <p>
 * The contents of this file are subject to the terms of the
 * Common Development and Distribution License (the License).
 * <p>
 * Everyone is permitted to copy and distribute verbatim copies
 * of this license document, but changing it is not allowed.
 */
package id.io.portal.service;


import id.io.portal.model.LocationRequest;
import id.io.portal.model.LocationResponse;
import id.io.portal.model.Locations;
import id.io.portal.util.database.LocationsDatabaseHelper;

import java.util.ArrayList;
import java.util.List;

public class LocationsService extends BaseService {
    private LocationsDatabaseHelper locationsDatabaseHelper;

    public LocationsService() {
        log = getLogger(this.getClass());
        locationsDatabaseHelper = new LocationsDatabaseHelper();
    }

    public List<LocationResponse> getProvinces() {
        final String methodName = "getProvinces";
        start(methodName);
        List<LocationResponse> responseList = new ArrayList<>();
        List<Locations> locationsList = locationsDatabaseHelper.getProvinces();
        for (Locations locations : locationsList) {
            LocationResponse response = new LocationResponse();
            response.setProvince(locations.getProvince());
            responseList.add(response);
        }
        completed(methodName);
        return responseList;
    }

    public List<LocationResponse> getRegencies(String filter) {
        final String methodName = "getRegencies";
        start(methodName);
        List<LocationResponse> responseList = new ArrayList<>();
        List<Locations> locationsList = locationsDatabaseHelper.getRegencies(filter);
        for (Locations locations : locationsList) {
            LocationResponse response = new LocationResponse();
            response.setRegency(locations.getRegency());
            responseList.add(response);
        }
        completed(methodName);
        return responseList;
    }

    public List<LocationResponse> getDistricts(String filter) {
        final String methodName = "getDistricts";
        start(methodName);
        List<LocationResponse> responseList = new ArrayList<>();
        List<Locations> locationsList = locationsDatabaseHelper.getDistricts(filter);
        for (Locations locations : locationsList) {
            LocationResponse response = new LocationResponse();
            response.setDistrict(locations.getDistrict());
            responseList.add(response);
        }
        completed(methodName);
        return responseList;
    }

    public List<LocationResponse> getVillages(String filter) {
        final String methodName = "getVillages";
        start(methodName);
        List<LocationResponse> responseList = new ArrayList<>();
        List<Locations> locationsList = locationsDatabaseHelper.getVillages(filter);
        for (Locations locations : locationsList) {
            LocationResponse response = new LocationResponse();
            response.setVillage(locations.getVillage());
            responseList.add(response);
        }
        completed(methodName);
        return responseList;
    }

    public List<LocationResponse> getPostalCodes(String filter) {
        final String methodName = "getPostalCodes";
        start(methodName);
        List<LocationResponse> responseList = new ArrayList<>();
        List<Locations> locationsList = locationsDatabaseHelper.getPostalCodes(filter);
        for (Locations locations : locationsList) {
            LocationResponse response = new LocationResponse();
            response.setPostalCode(locations.getPostalcode());
            response.setVillage(locations.getVillage());
            responseList.add(response);
        }
        completed(methodName);
        return responseList;
    }

    public List<LocationResponse> getLocations() {
        final String methodName = "getLocations";
        start(methodName);
        List<LocationResponse> responseList = new ArrayList<>();
        List<Locations> locationsList = locationsDatabaseHelper.getLocations();
        for (Locations locations : locationsList) {
            LocationResponse response = new LocationResponse();
            response.setId(String.valueOf(locations.getId()));
            response.setPostalCode(locations.getPostalcode());
            response.setVillage(locations.getVillage());
            response.setDistrict(locations.getDistrict());
            response.setRegency(locations.getRegency());
            response.setProvince(locations.getProvince());
            responseList.add(response);
        }
        completed(methodName);
        return responseList;
    }

    public boolean addLocation(LocationRequest request) {
        final String methodName = "addLocation";
        start(methodName);
        Locations locations = convertRequestToModel(request);
        boolean result = locationsDatabaseHelper.addLocation(locations);
        completed(methodName);
        return result;
    }

    public boolean updateLocation(LocationRequest request) {
        final String methodName = "updateLocation";
        start(methodName);
        Locations locations = convertRequestToModel(request);
        boolean result = locationsDatabaseHelper.updateLocations(locations);
        completed(methodName);
        return result;
    }

    public boolean deleteLocation(String id) {
        final String methodName = "deleteLocation";
        start(methodName);
        boolean result = locationsDatabaseHelper.deleteLocation(id);
        completed(methodName);
        return result;
    }

    private Locations convertRequestToModel(LocationRequest request) {
        Locations result = new Locations();
        if (request.getId() != null) {
            result.setId(Integer.parseInt(request.getId()));
        }
        result.setPostalcode(request.getPostalCode());
        result.setVillage(request.getVillage());
        result.setDistrict(request.getDistrict());
        result.setRegency(request.getRegency());
        result.setProvince(request.getProvince());

        return result;
    }
}
