/**
 * DO NOT ALTER OR REMOVE COPYRIGHT NOTICES OR THIS HEADER.
 * <p>
 * Copyright (c) 2020 IO-Teknologi Indonesia, and individual contributors
 * as indicated by the @author tags. All Rights Reserved
 * <p>
 * The contents of this file are subject to the terms of the
 * Common Development and Distribution License (the License).
 * <p>
 * Everyone is permitted to copy and distribute verbatim copies
 * of this license document, but changing it is not allowed.
 */
package id.io.portal.service;

import id.io.portal.helper.ConvertionHelper;
import id.io.portal.helper.MailHelper;
import id.io.portal.model.User;
import id.io.portal.util.constant.ConstantHelper;
import id.io.portal.util.database.ConfigurationDatabaseHelper;
import id.io.portal.util.database.OtpLogDatabaseHelper;
import id.io.portal.util.database.UserDatabaseHelper;

import javax.inject.Inject;
import java.util.HashMap;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;

public class OtpService extends BaseService {

    private ConfigurationDatabaseHelper configDatabaseHelper;
    private OtpLogDatabaseHelper otpLogDatabaseHelper;
    private UserDatabaseHelper userDatabaseHelper;

    @Inject
    private ExecutorService executor;

    public OtpService() {
        log = getLogger(this.getClass());
        configDatabaseHelper = new ConfigurationDatabaseHelper();
        otpLogDatabaseHelper = new OtpLogDatabaseHelper();
        userDatabaseHelper = new UserDatabaseHelper();
        executor = Executors.newFixedThreadPool(10);
    }
/*
    public boolean sendOtp(User user, String action) {

        final String methodName = "sendOtp";
        start(methodName);
        boolean result = false;

        if (Boolean.parseBoolean(otpConfig.get(ConstantHelper.OTP_ENABLED))) {

            String subject = otpConfig.get(ConstantHelper.OTP_MAIL_SUBJECT);
            String template = otpConfig.get(ConstantHelper.OTP_MAIL_CONTENT);
            String otp = OTPGeneratorHelper.generateOtp(Integer.parseInt(otpConfig.get(ConstantHelper.OTP_LENGTH)));
            String validity = ConvertionHelper.convertSecondToMinute(otpConfig.get(ConstantHelper.OTP_VALIDITY));
            String content = generateOTPContent(template, otp, validity);

            if (otpLogDatabaseHelper.saveOtpLog(user.getUserid(), otp, action)
                    || otpLogDatabaseHelper.saveOtpLog(user.getEmail(), otp, action)) {

                log.debug(methodName, user.getUserid() + " : " + otp);
                MailHelper mailHelper = new MailHelper();
                executor.execute(() ->
                        mailHelper.sendMail(user.getEmail(), subject, content)
                );
                executor.shutdown();
                result = true;
            }
        }
        completed(methodName);
        return result;
    }*/

    public boolean sendOtp(User user, String subject, String content, String otp, String validity, String action) {
        final String methodName = "sendOtp";
        start(methodName);
        boolean result = false;

        if (otpLogDatabaseHelper.saveOtpLog(user.getUserid(), otp, action)
                || otpLogDatabaseHelper.saveOtpLog(user.getEmail(), otp, action)) {

            log.debug(methodName, user.getUserid() + " : " + otp);
            MailHelper mailHelper = new MailHelper();
            executor.execute(() ->
                    mailHelper.sendMail(user.getEmail(), subject, content)
            );
            executor.shutdown();
            result = true;
        }

        completed(methodName);
        return result;
    }

    public String getUserId(String token) {
        final String methodName = "getUserId";
        start(methodName);
        String result = otpLogDatabaseHelper.getUserId(token);
        completed(methodName);
        return result;
    }

    public boolean checkOtp(String token) {
        final String methodName = "checkOtp";
        start(methodName);
        boolean result = otpLogDatabaseHelper.checkOtp(token);
        completed(methodName);
        return result;
    }

    public boolean validateOTP(String userId, String otpToken) {
        final String methodName = "validateOTP";
        start(methodName);
        boolean result = false;
        HashMap<String, String> otpConfig = configDatabaseHelper.getOtpConfiguration();
        if (otpLogDatabaseHelper.validateOtp(userId, otpToken,
                Integer.parseInt(ConvertionHelper.convertSecondToMinute(otpConfig.get(ConstantHelper.OTP_VALIDITY))))) {
            if (userDatabaseHelper.activateUser(userId)) {
                result = true;
            }
        }
        otpLogDatabaseHelper.revokeOTP(userId, otpToken);
        completed(methodName);
        return result;
    }
}
