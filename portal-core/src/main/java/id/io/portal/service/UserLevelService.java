/**
 * DO NOT ALTER OR REMOVE COPYRIGHT NOTICES OR THIS HEADER.
 * <p>
 * Copyright (c) 2020 IO-Teknologi Indonesia, and individual contributors
 * as indicated by the @author tags. All Rights Reserved
 * <p>
 * The contents of this file are subject to the terms of the
 * Common Development and Distribution License (the License).
 * <p>
 * Everyone is permitted to copy and distribute verbatim copies
 * of this license document, but changing it is not allowed.
 */
package id.io.portal.service;


import id.io.portal.model.UserLevel;
import id.io.portal.util.constant.ConstantHelper;
import id.io.portal.util.database.UserLevelDatabaseHelper;
import org.apache.http.HttpStatus;
import org.json.JSONObject;

import java.util.List;

public class UserLevelService extends BaseService {

    private UserLevelDatabaseHelper userLevelDatabaseHelper;

    public UserLevelService() {
        log = getLogger(this.getClass());
        userLevelDatabaseHelper = new UserLevelDatabaseHelper();
    }

    public UserLevel getLevel(String level) {
        final String methodName = "getLevel";
        start(methodName);

        UserLevel userLevel = userLevelDatabaseHelper.getLevel(level);

        completed(methodName);
        return userLevel;
    }

    public JSONObject getLevels() {
        final String methodName = "getLevels";
        start(methodName);
        List<UserLevel> list = userLevelDatabaseHelper.getLevels();

        JSONObject response = new JSONObject();
        response.put(ConstantHelper.HTTP_CODE, HttpStatus.SC_OK);
        response.put(ConstantHelper.HTTP_RESPONSE, list);

        completed(methodName);
        return response;
    }


}
