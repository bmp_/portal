package id.io.portal.service;


import id.io.portal.manager.EncryptionManager;
import id.io.portal.model.Authentication;
import id.io.portal.util.database.AuthenticationDatabaseHelper;
import id.io.portal.util.database.UserDatabaseHelper;

public class MobileService extends BaseService {

    private AuthenticationDatabaseHelper authenticationDatabaseHelper;
    private UserDatabaseHelper userDatabaseHelper;

    public MobileService() {
        log = getLogger(this.getClass());
        authenticationDatabaseHelper = new AuthenticationDatabaseHelper();
        userDatabaseHelper = new UserDatabaseHelper();
    }

    public boolean authenticate(Authentication authentication) {
        final String methodName = "authenticate";
        start(methodName);

        boolean result = authenticationDatabaseHelper.authenticate(authentication.getUserid(),
                EncryptionManager.getInstance().encrypt(authentication.getPassword()));

        completed(methodName);
        return result;
    }

    public boolean register(Authentication authentication) {
        final String methodName = "register";
        start(methodName);
        boolean result = authenticationDatabaseHelper.registerUser(authentication);
        completed(methodName);
        return result;
    }

    public boolean removeUser(String userId) {
        final String methodName = "removeUser";
        start(methodName);
        boolean result = authenticationDatabaseHelper.deleteUserAuthentications(userId);
        completed(methodName);
        return result;
    }
}
