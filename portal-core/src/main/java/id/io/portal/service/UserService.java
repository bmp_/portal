/**
 * DO NOT ALTER OR REMOVE COPYRIGHT NOTICES OR THIS HEADER.
 * <p>
 * Copyright (c) 2020 IO-Teknologi Indonesia, and individual contributors
 * as indicated by the @author tags. All Rights Reserved
 * <p>
 * The contents of this file are subject to the terms of the
 * Common Development and Distribution License (the License).
 * <p>
 * Everyone is permitted to copy and distribute verbatim copies
 * of this license document, but changing it is not allowed.
 */
package id.io.portal.service;

import id.io.portal.model.Managers;
import id.io.portal.model.User;
import id.io.portal.model.UserResponse;
import id.io.portal.util.database.*;

import javax.inject.Inject;
import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;

public class UserService extends BaseService {

    private UsersGestationDatabaseHelper usersGestationDatabaseHelper;
    private UserDatabaseHelper userDatabaseHelper;
    private ManagersDatabaseHelper managersDatabaseHelper;

    private UserScheduleDatabaseHelper userSchedulesDatabaseHelper;
    private ConfigurationDatabaseHelper configDatabaseHelper;
    private OtpLogDatabaseHelper otpLogDatabaseHelper;

    private AuthenticationDatabaseHelper authenticationDatabaseHelper;

    @Inject
    private ExecutorService executor;


    public UserService() {
        log = getLogger(this.getClass());
        usersGestationDatabaseHelper = new UsersGestationDatabaseHelper();
        userDatabaseHelper = new UserDatabaseHelper();
        managersDatabaseHelper = new ManagersDatabaseHelper();
        userSchedulesDatabaseHelper = new UserScheduleDatabaseHelper();
        configDatabaseHelper = new ConfigurationDatabaseHelper();
        otpLogDatabaseHelper = new OtpLogDatabaseHelper();
        authenticationDatabaseHelper = new AuthenticationDatabaseHelper();

        executor = Executors.newFixedThreadPool(10);

    }

    public List<UserResponse> getUsers() {
        final String methodName = "getUsers";
        start(methodName);
        List<UserResponse> userResponseList = new ArrayList<>();
        List<User> userList = userDatabaseHelper.getUsers();
        for (User user : userList) {
            UserResponse userResponse = convertResponseModel(user);
            userResponseList.add(userResponse);
        }
        completed(methodName);
        return userResponseList;
    }

    public String getUserId(String email) {
        final String methodName = "getUserId";
        start(methodName);
        String result = userDatabaseHelper.getUserId(email);
        completed(methodName);
        return result;
    }

    public UserResponse getUser(String userid) {
        final String methodName = "getUser";
        start(methodName);
        User user = userDatabaseHelper.getUser(userid);
        UserResponse userResponse = convertResponseModel(user);
        completed(methodName);
        return userResponse;
    }

    private UserResponse convertResponseModel(User user) {
        UserResponse userResponse = new UserResponse();

        userResponse.setUserId(user.getUserid());
        userResponse.setCardId(user.getCardid());
        userResponse.setGivenName(user.getGivenname());
        userResponse.setFullName(user.getFullname());
        userResponse.setEmail(user.getEmail());
        userResponse.setPhone(user.getPhone());
        userResponse.setRelation(user.getRelation());
        userResponse.setAge(user.getAge());
        userResponse.setAvatar(user.getAvatar());
        userResponse.setActive(user.isIsactive());
        userResponse.setCreateDt(user.getCreatedt());

        if (!user.getManagers().isEmpty()) {
            Managers managers = managersDatabaseHelper.getManager(user.getManagers());
            userResponse.setManagers(managers);
        }
        return userResponse;
    }


    public boolean registerUser(User user) {
        final String methodName = "registerUser";
        start(methodName);
        boolean result = userDatabaseHelper.createUser(user);
        completed(methodName);
        return result;

    }

    public User validateUser(String email) {
        String userId = userDatabaseHelper.getUserId(email);
        return userDatabaseHelper.getUser(userId);
    }

    public boolean validateUserId(String userId) {
        final String methodName = "validateUserId";
        start(methodName);
        boolean result = userDatabaseHelper.validateUserId(userId);
        completed(methodName);
        return result;

    }

    public boolean updateUser(User user) {
        final String methodName = "updateUser ";
        start(methodName);
        boolean result = userDatabaseHelper.updateUser(user);
        completed(methodName);
        return result;
    }

    public boolean validateEmail(String email) {
        final String methodName = "validateEmail";
        start(methodName);
        boolean result = false;
        if (userDatabaseHelper.validateEmail(email)) {
            result = true;
        }
        completed(methodName);
        return result;
    }

    public boolean removeUser(String userId) {
        final String methodName = "removeUser ";
        start(methodName);
        boolean result = userDatabaseHelper.deleteUser(userId);
        completed(methodName);
        return result;
    }
}
