/**
 * DO NOT ALTER OR REMOVE COPYRIGHT NOTICES OR THIS HEADER.
 *
 * Copyright (c) 2020 IO-Teknologi Indonesia, and individual contributors
 * as indicated by the @author tags. All Rights Reserved
 *
 * The contents of this file are subject to the terms of the
 * Common Development and Distribution License (the License).
 *
 * Everyone is permitted to copy and distribute verbatim copies
 * of this license document, but changing it is not allowed.
 *
 */
package id.io.portal.service;


import id.io.portal.manager.EncryptionManager;
import id.io.portal.model.SystemAdministrator;
import id.io.portal.util.constant.ConstantHelper;
import id.io.portal.util.database.SystemAdministratorDatabaseHelper;
import org.apache.http.HttpStatus;
import org.json.JSONObject;

public class SystemAdministratorService extends BaseService {

    private SystemAdministratorDatabaseHelper systemAdministratorDatabaseHelper;

    public SystemAdministratorService() {
        log = getLogger(this.getClass());
        systemAdministratorDatabaseHelper = new SystemAdministratorDatabaseHelper();
    }

    public JSONObject authenticate(JSONObject json) {
        final String methodName ="authenticate";
        start(methodName);
        JSONObject response = new JSONObject();

        boolean authenticate = systemAdministratorDatabaseHelper.authenticate(json.getString("username"), EncryptionManager.getInstance().encrypt(json.getString("password")));

        if (authenticate) {
            SystemAdministrator sysadmin = systemAdministratorDatabaseHelper.getAdminDetails(json.getString("username"));
            if (sysadmin.getIsactive() == 1) {
                response.put(ConstantHelper.HTTP_CODE, HttpStatus.SC_OK);
                response.put(ConstantHelper.HTTP_RESPONSE, sysadmin);
            } else {
                response.put(ConstantHelper.HTTP_CODE, HttpStatus.SC_FORBIDDEN);
                response.put(ConstantHelper.HTTP_MESSAGE, "user_not_active");
                response.put(ConstantHelper.HTTP_REASON, "User not active!!");
                response.put(ConstantHelper.KEY_USERID, sysadmin.getUserid());
                log.error(methodName, response.get(ConstantHelper.HTTP_REASON));
            }

        } else {
            response.put(ConstantHelper.HTTP_CODE, HttpStatus.SC_UNAUTHORIZED);
            response.put(ConstantHelper.HTTP_MESSAGE, "invalid_username_or_password");
            response.put(ConstantHelper.HTTP_REASON, "Invalid username and/or password!!");
            log.error(methodName, response.get(ConstantHelper.HTTP_REASON));

        }
        completed(methodName);
        return response;
    }
}
