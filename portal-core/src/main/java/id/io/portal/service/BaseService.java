package id.io.portal.service;

import id.io.portal.util.log.AppLogger;

import java.util.concurrent.ExecutorService;

public class BaseService {
    ExecutorService executor;

    protected AppLogger log;

    protected AppLogger getLogger(Class<?> clazz) {
        return new AppLogger(clazz);
    }

    protected void start(String methodName) {
        log.info(methodName, "start");
    }

    protected void completed(String methodName) {
        log.info(methodName, "completed");
    }


}
