/**
 * DO NOT ALTER OR REMOVE COPYRIGHT NOTICES OR THIS HEADER.
 * <p>
 * Copyright (c) 2020 IO-Teknologi Indonesia, and individual contributors
 * as indicated by the @author tags. All Rights Reserved
 * <p>
 * The contents of this file are subject to the terms of the
 * Common Development and Distribution License (the License).
 * <p>
 * Everyone is permitted to copy and distribute verbatim copies
 * of this license document, but changing it is not allowed.
 */
package id.io.portal.service;

import id.io.portal.helper.JsonHelper;
import id.io.portal.model.UserGestation;
import id.io.portal.model.UserGestationRequest;
import id.io.portal.model.UserGestationResponse;
import id.io.portal.util.database.UsersGestationDatabaseHelper;

import java.util.ArrayList;
import java.util.List;

public class UsersGestationService extends BaseService {

    private UsersGestationDatabaseHelper usersGestationDatabaseHelper;

    public UsersGestationService() {
        log = getLogger(this.getClass());
        usersGestationDatabaseHelper = new UsersGestationDatabaseHelper();
    }

    public List<UserGestationResponse> getGestations(String userId) {
        final String methodName = "getGestations";
        start(methodName);
        List<UserGestation> gestationList = usersGestationDatabaseHelper.getGestations(userId);
        List<UserGestationResponse> result = new ArrayList<>();
        for (UserGestation gestation : gestationList) {
            UserGestationResponse gestationResponse = convertModelToResponse(gestation);
            result.add(gestationResponse);
        }
        completed(methodName);
        return result;
    }

    public UserGestationResponse getGestation(String id, String userId) {
        final String methodName = "getGestation";
        start(methodName);
        UserGestation gestation = usersGestationDatabaseHelper.getGestation(id, userId);
        UserGestationResponse result = convertModelToResponse(gestation);
        completed(methodName);
        return result;
    }

    public UserGestationResponse getTrackingGestation(String userId) {
        final String methodName = "getTrackingGestation";
        start(methodName);
        UserGestation gestation = usersGestationDatabaseHelper.getTrackingGestation(userId);
        UserGestationResponse result = convertModelToResponse(gestation);
        completed(methodName);
        return result;
    }


    public boolean addGestation(UserGestationRequest request) {
        final String methodName = "addGestation";
        boolean result = false;
        start(methodName);

        UserGestation userGestation = convertRequestToModel(request);

        if (usersGestationDatabaseHelper.addGestation(userGestation)) {
            result = true;
        }
        completed(methodName);
        return result;
    }

    public boolean updateGestation(UserGestationRequest request) {
        final String methodName = "updatePregnantDetail";
        start(methodName);
        boolean result = false;
        UserGestation userGestation = convertRequestToModel(request);
        log.debug(methodName, JsonHelper.toJson(userGestation));
        if (usersGestationDatabaseHelper.updateGestation(userGestation)) {
            result = true;
        }
        completed(methodName);
        return result;
    }

    public boolean deleteGestation(String id) {
        final String methodName = "deleteGestation";
        start(methodName);
        boolean result = false;
        if (usersGestationDatabaseHelper.deleteGestation(id)) {
            result = true;
        }
        completed(methodName);
        return result;

    }

    public boolean validateUser(String userId) {
        final String methodName = "validateUser ";
        start(methodName);
        boolean result = usersGestationDatabaseHelper.validateUser(userId);
        completed(methodName);
        return result;
    }

    public boolean removeUser(String userId) {
        final String methodName = "removeUser";
        start(methodName);
        boolean result = usersGestationDatabaseHelper.removeUserGestations(userId);
        completed(methodName);
        return result;
    }

    private UserGestation convertRequestToModel(UserGestationRequest request) {

        UserGestation userGestation = new UserGestation();
        userGestation.setId(request.getId());
        userGestation.setUserid(request.getUserId());
        userGestation.setBabyname(request.getName());
        userGestation.setBabygender(request.getGender());
        userGestation.setInsemination(request.getInseminationDt());
        userGestation.setDuedate(request.getHplDt());
        userGestation.setFirstpregnancy(String.valueOf(request.isFirstGestation()));
        userGestation.setBeenborn(String.valueOf(request.isBeenBorn()));
        userGestation.setMisbirth(String.valueOf(request.isMisBirth()));

        return userGestation;
    }


    private UserGestationResponse convertModelToResponse(UserGestation userGestation) {
        UserGestationResponse result = new UserGestationResponse();
        result.setId(userGestation.getId());
        result.setName(userGestation.getBabyname());
        result.setGender(userGestation.getBabygender());
        result.setInseminationDt(userGestation.getInsemination());
        result.setHplDt(userGestation.getDuedate());
        result.setFirstGestation(Boolean.parseBoolean(userGestation.getFirstpregnancy()));
        result.setBeenBorn(Boolean.parseBoolean(userGestation.getBeenborn()));
        result.setMisBirth(Boolean.parseBoolean(userGestation.getMisbirth()));
        result.setCreateDt(userGestation.getCreatedt());

        return result;
    }

}
