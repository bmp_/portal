/**
 * DO NOT ALTER OR REMOVE COPYRIGHT NOTICES OR THIS HEADER.
 * <p>
 * Copyright (c) 2020 IO-Teknologi Indonesia, and individual contributors
 * as indicated by the @author tags. All Rights Reserved
 * <p>
 * The contents of this file are subject to the terms of the
 * Common Development and Distribution License (the License).
 * <p>
 * Everyone is permitted to copy and distribute verbatim copies
 * of this license document, but changing it is not allowed.
 */
package id.io.portal.service;

import id.io.portal.util.database.ConfigurationDatabaseHelper;
import org.json.JSONObject;

import java.util.HashMap;
import java.util.concurrent.ConcurrentHashMap;

/**
 * @author bmp
 */
public class ConfigurationService extends BaseService {

    private ConfigurationDatabaseHelper configDatabaseHelper;

    public ConfigurationService() {
        log = getLogger(this.getClass());
        configDatabaseHelper = new ConfigurationDatabaseHelper();
    }

    public JSONObject getListConfiguration() {

        final String methodName = "getListConfiguration";
        start(methodName);
        ConcurrentHashMap<String, String> configValues = configDatabaseHelper.getAllConfiguration();
        JSONObject response = new JSONObject(configValues);

        completed(methodName);
        return response;
    }

    public HashMap<String, String> getMailConfiguration() {
        final String methodName = "getMailConfiguration";
        start(methodName);
        HashMap<String, String> mailConfiguration = configDatabaseHelper.getMailConfiguration();
        completed(methodName);
        return mailConfiguration;
    }

    public HashMap<String, String> getOtpConfiguration() {
        final String methodName = "getOtpConfiguration";
        start(methodName);
        HashMap<String, String> mailConfiguration = configDatabaseHelper.getOtpConfiguration();
        completed(methodName);
        return mailConfiguration;
    }

    public HashMap<String, String> getMobileConfiguration() {
        final String methodName = "getMobileConfiguration";
        start(methodName);
        HashMap<String, String> mailConfiguration = configDatabaseHelper.getMobileConfiguration();
        completed(methodName);
        return mailConfiguration;
    }


}
