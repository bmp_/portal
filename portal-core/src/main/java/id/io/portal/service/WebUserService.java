package id.io.portal.service;

import id.io.portal.manager.EncryptionManager;
import id.io.portal.model.Authentication;
import id.io.portal.model.WebUser;
import id.io.portal.model.WebUserRequest;
import id.io.portal.model.WebUserResponse;
import id.io.portal.util.database.AuthenticationDatabaseHelper;
import id.io.portal.util.database.WebUserAuthenticationDatabaseHelper;
import id.io.portal.util.database.WebUserDatabaseHelper;

import java.util.ArrayList;
import java.util.List;
import java.util.UUID;

public class WebUserService extends BaseService {

    private WebUserDatabaseHelper userDatabaseHelper;
    private WebUserAuthenticationDatabaseHelper authenticationDatabaseHelper;

    public WebUserService() {
        log = getLogger(this.getClass());

        userDatabaseHelper = new WebUserDatabaseHelper();
        authenticationDatabaseHelper = new WebUserAuthenticationDatabaseHelper();
    }

    public List<WebUserResponse> getUsers() {
        final String methodName = "getUsers";
        start(methodName);
        List<WebUserResponse> responseList = new ArrayList<>();
        List<WebUser> userList = userDatabaseHelper.getUsers();
        for (WebUser user : userList) {
            WebUserResponse userResponse = convertModelToResponse(user);
            responseList.add(userResponse);
        }
        completed(methodName);
        return responseList;
    }

    public boolean addUser(WebUserRequest request) {
        final String methodName = "addUser";
        start(methodName);
        boolean result = false;
        WebUser user = convertRequestToModel(request);
        String password = EncryptionManager.getInstance().encrypt(request.getPassword());
        Authentication authentication = new Authentication();
        authentication.setUserid(user.getUserid());
        authentication.setPassword(password);
        if (userDatabaseHelper.createUser(user) && addAuthentication(authentication)) {
            result = true;
        }
        completed(methodName);
        return result;
    }

    public boolean addAuthentication(Authentication authentication) {
        final String methodName = "addAuthentication";
        start(methodName);
        boolean result = authenticationDatabaseHelper.registerUser(authentication);
        completed(methodName);
        return result;
    }

    public WebUser convertRequestToModel(WebUserRequest request) {
        WebUser user = new WebUser();
        user.setUserid(UUID.randomUUID().toString());
        user.setFullname(request.getFullName());
        user.setEmail(request.getEmail());
        user.setPhone(request.getPhone());
        user.setRole(request.getRole());
        return user;
    }

    public WebUserResponse convertModelToResponse(WebUser user) {
        WebUserResponse response = new WebUserResponse();
        response.setUserId(user.getUserid());
        response.setFullName(user.getFullname());
        response.setEmail(user.getEmail());
        response.setPhone(user.getPhone());
        response.setRole(user.getRole());
        response.setDownLine(user.getDownline());
        response.setActive(user.isIsactive());
        response.setCreateDt(user.getCreatedt());
        return response;
    }
}
