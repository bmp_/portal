package id.io.portal.service;

import id.io.portal.model.UserLocation;
import id.io.portal.model.UserLocationResponse;
import id.io.portal.util.database.UserLocationDatabaseHelper;

public class UserLocationService extends BaseService {

    private UserLocationDatabaseHelper userLocationDatabaseHelper;

    public UserLocationService() {
        log = getLogger(this.getClass());
        userLocationDatabaseHelper = new UserLocationDatabaseHelper();
    }

    public boolean registerUser(UserLocation user) {
        final String methodName = "registerUser ";
        start(methodName);
        boolean result = userLocationDatabaseHelper.createUserLocation(user);
        completed(methodName);
        return result;
    }

    public boolean updateUserLocation(UserLocation user) {
        final String methodName = "updateUserLocation ";
        start(methodName);
        boolean result = userLocationDatabaseHelper.updateUserLocation(user);
        completed(methodName);
        return result;
    }

    public boolean removeUser(String userId) {
        final String methodName = "removeUser ";
        start(methodName);
        boolean result = userLocationDatabaseHelper.deleteUserLocation(userId);
        completed(methodName);
        return result;
    }

    public boolean validateUser(String userId) {
        final String methodName = "validateUser ";
        start(methodName);
        boolean result = userLocationDatabaseHelper.validateUser(userId);
        completed(methodName);
        return result;
    }

    public UserLocationResponse getUserLocation(String userId) {
        final String methodName = "validateUser ";
        start(methodName);
        UserLocationResponse result = new UserLocationResponse();
        UserLocation userLocation = userLocationDatabaseHelper.getUserLocation(userId);
        result.setAddress(userLocation.getAddress());
        result.setVillage(userLocation.getVillage());
        result.setPostalCode(userLocation.getPostalcode());
        result.setDistrict(userLocation.getDistrict());
        result.setRegency(userLocation.getRegency());
        result.setProvince(userLocation.getProvince());
        result.setLatitude(userLocation.getLat());
        result.setLongitude(userLocation.getLng());

        completed(methodName);
        return result;
    }
}
